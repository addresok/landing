<?php

/**
 * @copyright	Copyright (C) 2012 Cedric KEIFLIN alias ced1870
 * http://www.joomlack.fr
 * Module Slideshow CK
 * @license		GNU/GPL
 * */
// no direct access
defined('_JEXEC') or die;
// JHtml::_('behavior.modal');
require_once dirname(__FILE__) . '/helper.php';




$items = modSlideHelper::getItems($params);




// display the module
require JModuleHelper::getLayoutPath('mod_slides', $params->get('layout', 'default'));
