<?php
/**
 * @copyright	Copyright (C) 2012 Cedric KEIFLIN alias ced1870
 * http://www.joomlack.fr
 * Module Slideshow CK
 * @license		GNU/GPL
 * */
// no direct access
defined('_JEXEC') or die('Restricted access');

?>



<section id="our-clients" class="cd-section">
    <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="edit"><?=$params->get('b11-title')?></h2>
                </div> 
                <div class="col-md-8 col-md-offset-2 our-clients-slide-waper">


 <div class="our-clients-slide owl-carousel">
<?php

foreach ($items as $i => $item) { ?>
 <img class="item" src="<?=$item->imgname?>" alt="">          
<?php  } ?>
</div>

                    
                </div> 
            </div> 
    </div> 
</section>   