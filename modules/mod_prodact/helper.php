<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_random_image
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_random_image
 *
 * @package     Joomla.Site
 * @subpackage  mod_random_image
 * @since       1.5
 */
class ModProdactHelper
{

    private $productList;
    function __construct($lang)
    {
        $database = JFactory::getDBO();
        $query  = 'SELECT * FROM bm_prodact WHERE lang = "' . $lang . '"';
        $database->setQuery($query);
        $this->productList = $database->loadAssocList();
    }

  public function getProdacts() {
  	return $this->productList;
  }

}
