<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_random_image
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<section id="rates" class="cd-section">
    <div class="container">
        <div class="row">
                <div class="col-md-12 header-pr">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="switch">
                                <span></span>
                                <a href="" id="licen" class="activ"><?=$params->get('tarif-name-1')?></a>
                                <a href="" id="aren"><?=$params->get('tarif-name-2')?></a>
                            </div>
                        </div> 
                    </div> 
                </div> 
         </div>


         
         <div id="licenT" class="row price-table activ">
        <div class="row">
                <div class="col-md-12 header-pr">
                    <h2><?=$params->get('title')?></h2>
                    <div class="text edit"><?=$params->get('textt')?></div>
                </div>
        </div> 

 <div class="row price-slide">

            <div class="col-md-3 r-no-p">
                <div class="roww header">
                    <h2><?=$params->get('tarif-name-1')?></h2>
                    <?=$params->get('sub-title')?> 
                </div>
    <?php 
    foreach($List1['name'] as $key=>$val ){ ?>
       <div class="roww<?=((count($List1['name'])-1) == $key) ? ' no-border' : ''?>">
     <?=str_replace('|', '<br>', $val)?>
      </div>
    <?php } ?>
            </div>
          
            <div class="col-md-3 price-opt l-no-p wow flipInY animated animated" data-wow-duration="1s" data-wow-delay="500ms">
                <div class="wraper">
                    <div class="roww header">
                        <h3><?=$params->get('polz')?></h3>
                        <p class="elips"><?=$params->get('polz-kl-t1')?></p>
                    </div>
                    
  <?php  foreach($List1['t1'] as $key=>$val ){ ?>
                    <div class="roww<?=((count($List1['name'])-1) == $key) ? ' no-border' : ''?>">
                    <?php  
                    if ($mandatory[$key]){ ?>
                    <span class="check"><u><?=$val?></u> <?=$params->get('valuta')?></span> 
                    <?php } else { ?>
                        <span><u><?=$val?></u> <?=$params->get('valuta')?></span> 
                       <div class="check-tr">
                           <p class="check"></p>
                       </div>                   
                    <?php } ?>
      </div>
    <?php } ?>
                    <div class="h-80 no-border">
                        <p class="final-calc"><u></u> <?=$params->get('valuta')?></p>
                        <a class="issue show-form" href="#"><?=$params->get('btn')?></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 price-opt wow flipInY animated animated" data-wow-duration="1s" data-wow-delay="800ms">
                <div class="wraper">
                    <div class="roww header">
                        <h3><?=$params->get('polz')?></h3>
                        <p class="elips"><?=$params->get('polz-kl-t2')?></p>
                    </div>
  <?php  foreach($List1['t2'] as $key=>$val ){ ?>
                    <div class="roww<?=((count($List1['name'])-1) == $key) ? ' no-border' : ''?>">
                    <?php  
                    if ($mandatory[$key]){ ?>
                    <span class="check"><u><?=$val?></u> <?=$params->get('valuta')?></span> 
                    <?php } else { ?>
                        <span><u><?=$val?></u> <?=$params->get('valuta')?></span> 
                       <div class="check-tr">
                           <p class="check"></p>
                       </div>                   
                    <?php } ?>
      </div>
    <?php } ?>
                    <div class="h-80 no-border">
                        <p class="final-calc"><u></u> <?=$params->get('valuta')?></p>
                        <a class="issue show-form" href="#"><?=$params->get('btn')?></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 price-opt wow flipInY animated animated" data-wow-duration="1s" data-wow-delay="1100ms">
                <div class="wraper">
                    <div class="roww header">
                        <h3><?=$params->get('polz')?></h3>
                        <p class="elips"><?=$params->get('polz-kl-t3')?></p>
                    </div>
  <?php  foreach($List1['t3'] as $key=>$val ){ ?>
                    <div class="roww<?=((count($List1['name'])-1) == $key) ? ' no-border' : ''?>">
                    <?php  
                    if ($mandatory[$key]){ ?>
                    <span class="check"><u><?=$val?></u> <?=$params->get('valuta')?></span> 
                    <?php } else { ?>
                        <span><u><?=$val?></u> <?=$params->get('valuta')?></span> 
                       <div class="check-tr">
                           <p class="check"></p>
                       </div>                   
                    <?php } ?>
      </div>
    <?php } ?>
                    <div class="h-80 no-border">
                        <p class="final-calc"><u></u> <?=$params->get('valuta')?></p>
                        <a class="issue show-form" href="#"><?=$params->get('btn')?></a>
                    </div>
                </div>
            </div>
            </div>
        </div>  








<div id="arenT" class="row price-table">
        <div class="row">
                <div class="col-md-12 header-pr">
                    <h2><?=$params->get('title2')?></h2>
                    <div class="text edit"><?=$params->get('textt2')?></div>
                </div>
        </div> 
     <div class="row price-slide">
            <div class="col-md-3 r-no-p">
                <div class="roww header">
                    <h2 class="tip2"><?=$params->get('tarif-name-2')?></h2>
                    <?=$params->get('sub-title2')?> 
                </div>
        <div class="roww">
            <?=$params->get('cost')?>
      </div>          
    <?php 
    foreach($List2_1['name'] as $key=>$val ){ ?>
       <div class="roww<?=((count($List2_1['name'])-1) == $key) ? ' no-border' : ''?>">
     <?=str_replace('|', '<br>', $val)?>
      </div>
    <?php } ?>
            </div>
          
        <div class="col-md-3 price-opt l-no-p wow flipInY animated animated" data-wow-duration="1s" data-wow-delay="500ms">
            <div class="wraper">
               
                <div class="roww header">
                    <h3><?=$params->get('tarif-2-1-name')?></h3>
                    <p class="elips"><?=$params->get('tarif-kl-2-1')?></p>
                </div>
                <div class="roww">
                        <span class="" style="text-align:center;padding: 0;">
                            <p class="final-calc">
                                <?=$params->get('tarif-2-1-с')?> 
                                <?=$params->get('valuta')?>
                            </p></span> 
                </div>

            <?php 
            foreach($List2_1['name'] as $key=>$val ){ ?>
            <div class="roww<?=((count($List2_1['name'])-1) == $key) ? ' no-border' : ''?>">
                    <span class="check check2"><?=$params->get('vkl')?></span> 
            </div>
            <?php } ?>
                    
                    <div class="h-25 no-border">
                        <a class="issue show-form" href="#"><?=$params->get('btn')?></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 price-opt wow flipInY animated animated" data-wow-duration="1s" data-wow-delay="800ms">
                <div class="wraper">
                <div class="roww header">
                    <h3><?=$params->get('tarif-2-1-name')?></h3>
                    <p class="elips"><?=$params->get('tarif-kl-2-2')?></p>
                </div>
                <div class="roww">
                        <span class="" style="text-align:center;padding: 0;">
                            <p class="final-calc">
                                <?=$params->get('tarif-2-2-с')?> 
                                <?=$params->get('valuta')?>
                            </p></span> 
                </div>
                <?php 
                foreach($List2_1['name'] as $key=>$val ){ ?>
                <div class="roww<?=((count($List2_1['name'])-1) == $key) ? ' no-border' : ''?>">
                        <span class="check check2"><?=$params->get('vkl')?></span> 
                </div>
                <?php } ?>



                    <div class="h-25 no-border">
                        <a class="issue show-form" href="#"><?=$params->get('btn')?></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 price-opt wow flipInY animated animated" data-wow-duration="1s" data-wow-delay="1100ms">
                <div class="wraper">
                <div class="roww header">
                    <h3><?=$params->get('tarif-2-1-name')?></h3>
                    <p class="elips"><?=$params->get('tarif-kl-2-3')?></p>
                </div>
                <div class="roww">
                        <span class="" style="text-align:center;padding: 0;">
                            <p class="final-calc">
                                <?=$params->get('tarif-2-3-с')?> 
                                <?=$params->get('valuta')?>
                            </p></span> 
                </div>
                <?php 
                foreach($List2_1['name'] as $key=>$val ){ ?>
                <div class="roww<?=((count($List2_1['name'])-1) == $key) ? ' no-border' : ''?>">
                        <span class="check check2"><?=$params->get('vkl')?></span> 
                </div>
                <?php } ?>

                    <div class="h-25 no-border">
                        <a class="issue show-form" href="#"><?=$params->get('btn')?></a>
                    </div>
                </div>
            </div>


    </div>









  <div class="row arenT2 price-slide">
   
     
    
            <div class="col-md-3 r-no-p">
                <div class="roww header">
                    <h2 class="tip2"><?=$params->get('tarif-name-2')?></h2>
                    <?=$params->get('sub-title2')?> 
                </div>
        <div class="roww">
            <?=$params->get('cost')?>
      </div>          
    <?php 
    foreach($List2_2['name'] as $key=>$val ){ ?>
       <div class="roww<?=((count($List2_2['name'])-1) == $key) ? ' no-border' : ''?>">
     <?=str_replace('|', '<br>', $val)?>
      </div>
    <?php } ?>
            </div>
          
        <div class="col-md-3 price-opt l-no-p wow flipInY animated animated" data-wow-duration="1s" data-wow-delay="500ms">
            <div class="wraper">
               
                <div class="roww header">
                    <h3><?=$params->get('tarif-2-2-name')?></h3>
                    <p class="elips"><?=$params->get('tarif-kl-2-2-1')?></p>
                </div>
                <div class="roww">
                        <span class="" style="text-align:center;padding: 0;">
                            <p class="final-calc">
                                <?=$params->get('tarif-2-2-1-с')?> 
                                <?=$params->get('valuta')?>
                            </p></span> 
                </div>

            <?php 
            foreach($List2_2['name'] as $key=>$val ){ ?>
            <div class="roww<?=((count($List2_2['name'])-1) == $key) ? ' no-border' : ''?>">
                    <span class="check check2"><?=$params->get('vkl')?></span> 
            </div>
            <?php } ?>
                    
                    <div class="h-25 no-border">
                        <a class="issue show-form" href="#"><?=$params->get('btn')?></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 price-opt wow flipInY animated animated" data-wow-duration="1s" data-wow-delay="800ms">
                <div class="wraper">
                <div class="roww header">
                    <h3><?=$params->get('tarif-2-2-name')?></h3>
                    <p class="elips"><?=$params->get('tarif-kl-2-2-2')?></p>
                </div>
                <div class="roww">
                        <span class="" style="text-align:center;padding: 0;">
                            <p class="final-calc">
                                <?=$params->get('tarif-2-2-2-с')?> 
                                <?=$params->get('valuta')?>
                            </p></span> 
                </div>
                <?php 
                foreach($List2_2['name'] as $key=>$val ){ ?>
                <div class="roww<?=((count($List2_2['name'])-1) == $key) ? ' no-border' : ''?>">
                        <span class="check check2"><?=$params->get('vkl')?></span> 
                </div>
                <?php } ?>



                    <div class="h-25 no-border">
                        <a class="issue show-form" href="#"><?=$params->get('btn')?></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 price-opt wow flipInY animated animated" data-wow-duration="1s" data-wow-delay="1100ms">
                <div class="wraper">
                <div class="roww header">
                    <h3><?=$params->get('tarif-2-2-name')?></h3>
                    <p class="elips"><?=$params->get('tarif-kl-2-3k')?></p>
                </div>
                <div class="roww">
                        <span class="" style="text-align:center;padding: 0;">
                            <p class="final-calc">
                                <?=$params->get('tarif-2-2-3-сc')?> 
                                <?=$params->get('valuta')?>
                            </p></span> 
                </div>
                <?php 
                foreach($List2_2['name'] as $key=>$val ){ ?>
                <div class="roww<?=((count($List2_2['name'])-1) == $key) ? ' no-border' : ''?>">
                        <span class="check check2"><?=$params->get('vkl')?></span> 
                </div>
                <?php } ?>

                    <div class="h-25 no-border">
                        <a class="issue show-form" href="#"><?=$params->get('btn')?></a>
                    </div>
                </div>
            </div>


   </div> 




        </div>  







    </div> 
</section>