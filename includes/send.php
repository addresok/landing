<?php



if (!defined('_JEXEC')) {
    define( '_JEXEC', 1 );
    define('JPATH_BASE', '../');    //  line 4
    require_once ( JPATH_BASE .'/includes/defines.php' );
    require_once ( JPATH_BASE .'/includes/framework.php' );
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);
}

$mailer = JFactory::getMailer();
$config = JFactory::getConfig();


$sender = array($config->get('mailfrom'), $config->get('fromname') );
$mailer->setSender($sender);
$mailer->addRecipient($config->get('mailfrom'));


$body   = "<div>";
 $body  .= (isset($_POST['prefix']) AND !empty($_POST['prefix'])) ? "<h1> Форма: " . $_POST['prefix'] . "</h1>" : '';
 $body  .= (isset($_POST['name']) AND !empty($_POST['name'])) ? "<p><b>Имя :</b>" . $_POST['name'] . "</p>" : '';
 $body  .= (isset($_POST['tel']) AND !empty($_POST['tel'])) ? "<p><b>Номер:</b>" . $_POST['tel'] . "</p>" : '';
 $body  .= (isset($_POST['email']) AND !empty($_POST['email'])) ? "<p><b>Mail :</b>" . $_POST['email'] . "</p>" : '';
$body  .= "</div>";

$mailer->setSubject('Нужна бесплатная консультация');
$mailer->IsHTML(true);
$mailer->setBody($body);


$send = $mailer->Send();

if ($send !== true) {
    echo 'error';
} else {
    echo 'done';
}
