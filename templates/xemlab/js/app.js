jQuery(document).ready(function($){
	var contentSections = $('.cd-section');
    contentSections.each(function(i,e){
        var ul = $('#cd-vertical-nav ul'),
            li;
            li = '<li><a href="#'+ $(e).attr('id') +'" data-number="' + (i+1) + '"><span class="cd-dot"></span></a></li>';
        ul.append(li);
    });
	var	navigationItems = $('#cd-vertical-nav a');
	updateNavigation();
	$(window).on('scroll', function(){
		updateNavigation();
        

	});
	navigationItems.on('click', function(event){
        event.preventDefault();
        smoothScroll($(this.hash));
    });
    $('.cd-scroll-down').on('click', function(event){
        event.preventDefault();
        smoothScroll($(this.hash));
    });

    $('.touch .cd-nav-trigger').on('click', function(){
    	$('.touch #cd-vertical-nav').toggleClass('open');
  
    });

    $('.touch #cd-vertical-nav a').on('click', function(){
    	$('.touch #cd-vertical-nav').removeClass('open');
    });

	function updateNavigation() {
		contentSections.each(function(){
			$this = $(this);
			var activeSection = $('#cd-vertical-nav a[href="#'+$this.attr('id')+'"]').data('number') - 1;
			if ( ( $this.offset().top - $(window).height()/2 < $(window).scrollTop() ) && ( $this.offset().top + $this.height() - $(window).height()/2 > $(window).scrollTop() ) ) {
				navigationItems.eq(activeSection).addClass('is-selected');
			}else {
				navigationItems.eq(activeSection).removeClass('is-selected');
			}
		});
	}

	function smoothScroll(target) {
        $('body,html').animate(
        	{'scrollTop':target.offset().top},
        	600
        );
	}
});


$(document).ready(function() {


var homeSection = $('#homeSection').height();
window.onscroll = function() {
  var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    if (scrolled <= homeSection) 
        $('#headerBar').addClass('home');
    else  $('#headerBar').removeClass('home');
}



var wow = new WOW(
  {
    boxClass:     'wow',      
    animateClass: 'animated', 
    offset:       0,          
    mobile:       true,       
    live:         true,       
    callback:     function(box) {
    },
    scrollContainer: null 
  }
);
wow.init();


$('.how-do-slide').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,


});


$('.our-clients-slide').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 1,
  prevArrow:'<div class="slick-prev"><i></i></div>',
  nextArrow:'<div class="slick-next"><i></i></div>',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});




    if (analitik){
        var dots       = $('#analitik .dots ul');
        var beforSlide = $('#analitik .befor-slide');
        var afforSlide = $('#analitik .affor-slide');
        var slide      = $('#analitik .slide');
        var text       = $('#analitik .text');
        var aLActiv    = $('#analitik .a-l');
        var aRActiv    = $('#analitik .a-r');

        var dotsItem;
    analitik.forEach(function(element, i) {
        dots.append('<li id="dot'+i+'" data-id="'+i+'"></li>');
    }, this);
    }

    dotsItem = $('#analitik .dots ul li');
    beforSlide.css('backgroundImage', 'url('+analitik[0].img+')');
    text.html(analitik[0].text);
     $('#analitik .dots ul li:first').addClass('activ');
    
    dotsItem.click(function(){
        var id = $(this).data('id');
        dotsItem.removeClass('activ');
        $(this).addClass('activ');
        text.html(analitik[id].text);
        slide.css('backgroundImage', 'url('+analitik[id].img+')');
    
        provArrow();
        
        if (analitik[id - 1])  beforSlide.css('backgroundImage', 'url('+analitik[id-1].img+')');
        else  beforSlide.css('backgroundImage', 'url('+analitik[id].img+')');

        if (analitik[id + 1])  afforSlide.css('backgroundImage', 'url('+analitik[id+1].img+')');
        else  afforSlide.css('backgroundImage', 'url('+analitik[id].img+')');
    });


var provArrow = function(id = -1){
    var aL       = $('#analitik .a-l');
    var aR       = $('#analitik .a-r');
    var activ    = $('#analitik .dots ul li.activ').data('id');

if (id >=  0) activ = id;
    if (activ == 0 ) aL.removeClass('activ');
        else aL.addClass('activ');
    if (activ == analitik.length-1 ) aR.removeClass('activ');
        else aR.addClass('activ');
        return false;
}

beforSlide.click(function(){
    var activ    = $('#analitik .dots ul li.activ').data('id');
    if (activ == 0) return false;
    activ--;
     beforSlide.css('backgroundImage', 'url('+analitik[(activ-1 < 1)? 0 : activ-1].img+')');
     afforSlide.css('backgroundImage', 'url('+analitik[(activ+1 < analitik.length-1) ? activ+1 : analitik.length-1].img+')');
     slide.css('backgroundImage', 'url('+analitik[activ].img+')');
     text.html(analitik[activ].text);
     $('#analitik .dots ul li').removeClass('activ');
      $('#analitik .dots ul li#dot'+activ).addClass('activ');
      provArrow();
});
afforSlide.click(function(){
    var activ    = $('#analitik .dots ul li.activ').data('id');
    if (activ == analitik.length-1) return false;
    activ++;
     beforSlide.css('backgroundImage', 'url('+analitik[(activ-1 < 1)? 0 : activ-1].img+')');
     afforSlide.css('backgroundImage', 'url('+analitik[(activ+1 < analitik.length-1) ? activ+1 : analitik.length-1].img+')');
     slide.css('backgroundImage', 'url('+analitik[activ].img+')');
     text.html(analitik[activ].text);
     $('#analitik .dots ul li').removeClass('activ');
      $('#analitik .dots ul li#dot'+activ).addClass('activ');
      provArrow();
});







var priceOpt = $('.price-opt');
priceOpt.each(function(i, val){
    var s = $(val).find('.check u');
    var f = $(val).find('.final-calc u');
    var sum = 0;
    s.each(function(j, v){
        sum += parseInt($(v).html().replace(' ', ''));
    });
    $(f).html(sum);
});



$('p.check').click(function(){
    var parents = $( this ).parents( ".price-opt" );
    var roww = $( this ).parents( ".roww" ).find('span u').html().replace(' ', '');
    var sum = $(parents).find('.final-calc u').html().replace(' ', '');
    var s = 0;

    if ($(this).hasClass('activ') ) {
        s = parseInt(sum) - parseInt(roww); 
    } else {
        s = parseInt(sum) + parseInt(roww); 
    }

    $(this).toggleClass('activ');

    $(parents).find('.final-calc u').html(s)
});



$('#licen').click(function(){
    var that = this;
    $('#rates .header-pr .switch span').animate({
    left: '8px',
  }, 500, function(){
    $('#rates .switch a').removeClass('activ')
    $('.price-table').removeClass('activ')
    $(that).addClass('activ');
    $('#licenT').addClass('activ');
  });

    return false;
});

$('#aren').click(function(){
    var swit = ($('#rates .header-pr .switch').width() / 2) - 2;
    var that = this;
$('#rates .header-pr .switch span').animate({
    left: swit,
  }, 500, function(){
    $('#rates .switch a').removeClass('activ')
    $('.price-table').removeClass('activ')
    $(that).addClass('activ');
    $('#arenT').addClass('activ');
  });

    return false;
});


// Cache selectors
var lastId,
    topMenu = $("#mainMenu"),
    topMenuHeight = topMenu.outerHeight()+15,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    }),
    noScrollAction = false;

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    noScrollAction = true;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    },{
        duration: 300,
        complete: function() {
            menuItems
                .parent().removeClass("active")
                .end().filter($('a[href="'+href+'"]')).parent().addClass("active");
            setTimeout(function(){ noScrollAction = false; }, 10);
        }
    });
    e.preventDefault();
});

// Bind to scroll
$(window).scroll(function(){
   if(!noScrollAction){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
       
       if (lastId !== id) {
           lastId = id;
           // Set/remove active class
           menuItems
             .parent().removeClass("active")
             .end().filter($('a[href="#'+id+'"]')).parent().addClass("active");
       }
   }    
});

$('.scrollDown').click(function(e){
        var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    noScrollAction = true;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    },{
        duration: 300,
        complete: function() {
            menuItems
                .parent().removeClass("active")
                .end().filter($('a[href="'+href+'"]')).parent().addClass("active");
            setTimeout(function(){ noScrollAction = false; }, 10);
        }
    });
    e.preventDefault();
});

$('.popup-form .close').click(function(){
    $('.popup-form').fadeOut();
});
$('.show-form').click(function(e){
    $('.popup-form').fadeIn();
    $('.popup-form #prefix').val($(this).html());
    $('.popup-form h3').html($(this).html());


    e.preventDefault();
});



$('form .subm').click(function(e){
    
    var form  = $(this).parents('.contact-form')[0];
    var data = $(form).serializeArray();
    var error = false;
    var errorBlock = $(form).find('.error-info');
    var compileBlock = $(form).find('.compile-info');


data.forEach(function(el){
var inp = $(form).find('input[name='+el.name+']');
if (el.value.length == 0) {
    $(inp).addClass('inpError');
    error = true;
}


});
if(error) {
    errorBlock.fadeIn();
    setTimeout(function(){
    errorBlock.fadeOut();
    $(form).find('input').removeClass('inpError');
    }, 4000)
return false;
}

        $.ajax({
            async: false,
            type: "POST",
            url: "/includes/send.php",
            data: data,
            success: function(data){
                compileBlock.fadeIn();
                $(this).attr('disabled',true);
            }
        });






    e.preventDefault();
})




reWidthMob();

$( window ).resize(function() {
  reWidthMob();
});

function reWidthMob() {
    var w = $(window).width(); 
    if (w <= 980) { 
        $('.price-table .price-slide').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 2,
        prevArrow:'',
        nextArrow:'',


        });
    }

}



});