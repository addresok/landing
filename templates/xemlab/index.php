<?php defined('_JEXEC') or die;
require_once(dirname(__FILE__) . '/lib/conf.php');
 ?>
<!DOCTYPE html>
<html xmlns:jdoc="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=$langTag?>" lang="<?=$langTag?>" dir="<?=$directionTag?>">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <jdoc:include type="head"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="<?=$template_url?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$template_url?>/css/slick.css" rel="stylesheet">
    <link href="<?=$template_url?>/css/slick-theme.css" rel="stylesheet">
    <link href="<?=$template_url?>/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <script src="<?=$template_url?>/js/modernizr.js"></script> <!-- Modernizr -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>
  <body>
<header id="headerBar" class="home">
    <div class="container homecontainer">
            <div class="row">
                <div class="col-lg-10">
                   <nav class="navbar">
                        <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">
                         <img src="<?=$this->params->get('logoFile')?>" alt="" class="img-responsive">    
                        </a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <?php if ($this->countModules('topmenu')) : ?>
                            <jdoc:include type="modules" name="topmenu" />
	                <?php endif; ?>

                        </div>
                    </nav>
                </div> 
                <div class="col-lg-2 top-contact">
                     <a class="tell"><?=$this->params->get('number')?></a>
                    <a href="#" class="btn call-me show-form"><?=$this->params->get('hedderCall')?></a>
	
	                <?php if ($this->countModules('lang')) : ?>
                            <jdoc:include type="modules" name="lang" />
	                <?php endif; ?>
                </div>  
                </div> 
            </div>       
    </div>       
</header>
	<section class="cd-section" id="homeSection">
        <div class="container">
            <div class="row">
                <div class="col-md-7 home-l">
                    <h1 class="edit"><?=$this->params->get('b1-title')?></h1>
                     <ul>
                    <li class="edit"><?=$this->params->get('b1-list-1')?></li>
                    <li class="edit"><?=$this->params->get('b1-list-2')?></li>
                    <li class="edit"><?=$this->params->get('b1-list-3')?></li>
                     </ul> 
                    <a href="<?=$this->params->get('b1-f-link')?>" class="btn black"><?=$this->params->get('b1-f-link-text')?></a>
                    <a href="#problems" class="btn trans scrollDown"><?=$this->params->get('b2-f-link-text')?></a>
                </div>
                <div class="col-md-5 home-r">
                    <img src="<?=$this->params->get('b1-img')?>" alt="" class="wow fadeInRight" data-wow-duration="1s" data-wow-delay="1200ms">
                    <p class="andr wow fadeInRight" data-wow-duration="500ms" data-wow-delay="2000ms">
                        Android
                    </p>
                    <p class="ios wow fadeInRight" data-wow-duration="500ms" data-wow-delay="2600ms">
                        iOS
                    </p>
                </div>
            </div>
        </div>
        <a id="scrollDown" class="scrollDown" href="#problems"></a>
    </section>
<section id="problems" class="cd-section">
    <div class="container">
            <div class="row">
                <h2 class="edit"><?=$this->params->get('b2-title')?></h2>
                <div class="col-md-4 block1">
                    <span class="icon-block"></span>
                  <h3 class="edit"><?=$this->params->get('b2b1-title')?></h3>
                  <div class="text edit"><?=$this->params->get('b2b1-text')?></div>
                </div> 
                <div class="col-md-4 block2">
                    <span class="icon-block"></span>
                   <h3 class="edit"><?=$this->params->get('b2b2-title')?></h3>
                  <div class="text edit"><?=$this->params->get('b2b2-text')?></div>
                </div> 
                <div class="col-md-4 block3">
                    <span class="icon-block"></span>
                   <h3 class="edit"><?=$this->params->get('b2b3-title')?></h3>
                  <div class="text edit"><?=$this->params->get('b2b3-text')?></div>
                </div> 
                <div class="col-md-4 block4 col-md-offset-2">
                    <span class="icon-block"></span>
                   <h3 class="edit"><?=$this->params->get('b2b4-title')?></h3>
                  <div class="text edit"><?=$this->params->get('b2b4-text')?></div>
                </div> 
                <div class="col-md-4 block5">
                    <span class="icon-block"></span>
                    <h3 class="edit"><?=$this->params->get('b2b5-title')?></h3>
                  <div class="text edit"><?=$this->params->get('b2b5-text')?></div>
                </div> 

            </div> 
    </div> 
</section>
<section id="for-whom" class="cd-section">

<?php

$b3s1list1 	 = json_decode( $this->params->get('b3-s1-list-1'),true);
$b3s1list2 	 = json_decode( $this->params->get('b3-s2'),true);
$b3s1list3 	 = json_decode( $this->params->get('b3-s3'),true);
$b3s1list4 	 = json_decode( $this->params->get('b3-s4'),true);
$b3s1list5 	 = json_decode( $this->params->get('b3-s5'),true);
$b3s1list6 	 = json_decode( $this->params->get('b3-s6'),true);
$b3s1list7 	 = json_decode( $this->params->get('b3-s7'),true);
$b5_list 	 = json_decode( $this->params->get('b5-list'),true);

?>

<div class="container">
            <div class="row">
                <h2 class="edit"><?=$this->params->get('b3-title')?></h2>
                <div class="col-md-4 col-xs-6 col-sm-4">
                   <div class="row block1">
                    <div class="col-md-3">
                        <span class="icon-block"></span>
                    </div> 
                    <div class="col-md-9">
                        <h3 class="edit"><?=$this->params->get('b3-s1-title')?></h3>
                        <ul>
                            <?php
                            foreach( $b3s1list1 as $list ) {
                                foreach( $list as $val ) { ?>
                                    <li><?=$val?></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div> 
                   </div>   
                   <div class="row block2">
                    <div class="col-md-3">
                        <span class="icon-block"></span>
                    </div> 
                    <div class="col-md-9">
                       <h3 class="edit"><?=$this->params->get('b3-s2-title')?></h3>
                        <ul>
                            <?php
                            foreach( $b3s1list2 as $list ) {
                                foreach( $list as $val ) { ?>
                                    <li><?=$val?></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div> 
                   </div>   
                   <div class="row block3">
                    <div class="col-md-3">
                        <span class="icon-block"></span>
                    </div> 
                    <div class="col-md-9">
                        <h3 class="edit"><?=$this->params->get('b3-s3-title')?></h3>
                        <ul>
                            <?php
                            foreach( $b3s1list3 as $list ) {
                                foreach( $list as $val ) { ?>
                                    <li><?=$val?></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div> 
                   </div>   
                </div> 
                <div class="col-md-4 col-xs-6 col-sm-4">
                   <div class="row block4">
                    <div class="col-md-3">
                        <span class="icon-block"></span>
                    </div> 
                    <div class="col-md-9">
                        <h3 class="edit"><?=$this->params->get('b3-s4-title')?></h3>
                        <ul>
                            <?php
                            foreach( $b3s1list4 as $list ) {
                                foreach( $list as $val ) { ?>
                                    <li><?=$val?></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div> 
                   </div>   
                </div> 
                <div class="col-md-4 col-xs-6 col-sm-4">
                   <div class="row block5">
                    <div class="col-md-3">
                        <span class="icon-block"></span>
                    </div> 
                    <div class="col-md-9">
                         <h3 class="edit"><?=$this->params->get('b3-s5-title')?></h3>
                        <ul>
                            <?php
                            foreach( $b3s1list5 as $list ) {
                                foreach( $list as $val ) { ?>
                                    <li><?=$val?></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div> 
                   </div>   
                   <div class="row block6">
                    <div class="col-md-3">
                        <span class="icon-block"></span>
                    </div> 
                    <div class="col-md-9">
                        <h3 class="edit"><?=$this->params->get('b3-s6-title')?></h3>
                        <ul>
                            <?php
                            foreach( $b3s1list6 as $list ) {
                                foreach( $list as $val ) { ?>
                                    <li><?=$val?></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div> 
                   </div>   
                   <div class="row block7">
                    <div class="col-md-3">
                        <span class="icon-block"></span>
                    </div> 
                    <div class="col-md-9">
                        <h3 class="edit"><?=$this->params->get('b3-s7-title')?></h3>
                        <ul>
                            <?php
                            foreach( $b3s1list7 as $list ) {
                                foreach( $list as $val ) { ?>
                                    <li><?=$val?></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div> 
                   </div>   
                </div> 
            </div> 
            <img src="images/bg/iphone-block-3.png" alt="" class="iphone-bg">
            <img src="images/bg/iphone-block-3-icon.png" alt="" class="iphone-bg-icon wow zoomIn" 
            data-wow-duration="1s" data-wow-delay="500ms">
        </div> 
</section>

<section id="system" class="cd-section">
    <div class="container">
            <div class="row">
                <h2 class="edit"><?=$this->params->get('b4-title')?></h2>
                <div class="col-md-12 shem">
                    <img class="img-responsive" src="<?=$this->params->get('shemFile')?>" alt="">
                </div>
                <div class="col-md-4 block1">
                    <span class="icon-block"></span>
                  <h3 class="edit"><?=$this->params->get('b4b1-title')?></h3>
                  <div class="text"><?=$this->params->get('b4b1-text')?></div>
                </div> 
                <div class="col-md-4 block2">
                    <span class="icon-block"></span>
                  <h3 class="edit"><?=$this->params->get('b4b2-title')?></h3>
                  <div class="text"><?=$this->params->get('b4b2-text')?></div>
                </div> 

                <div class="col-md-4 block3">
                    <span class="icon-block"></span>
                  <h3 class="edit"><?=$this->params->get('b4b3-title')?></h3>
                  <div class="text"><?=$this->params->get('b4b3-text')?></div>
                </div> 
            </div> 
</section>   

<section id="opportunities" class="cd-section">
    <div class="container">
            <div class="row">
            <h2 class="edit"><?=$this->params->get('b5-title')?></h2>
       
                <div class="col-md-5 block1">
                    <img class="iphone-6 screen-2 wow fadeInDown" 
                         data-wow-duration="1s" 
                         data-wow-delay="1200ms" 
                         src="<?=$this->params->get('screen-3')?>" 
                         alt="">
                    <img class="iphone-6 screen-1 wow fadeInDown" 
                         data-wow-duration="1s" 
                         data-wow-delay="800ms" 
                         src="<?=$this->params->get('screen-2')?>" 
                         alt="">
                    <img class="iphone-6 wow fadeInLeft" 
                         data-wow-duration="1s" 
                         data-wow-delay="300ms" 
                         src="<?=$this->params->get('screen-1')?>" 
                         alt="">
                </div> 
                <div class="col-md-7 block2">
                    <ul>
                            <?php
                            foreach( $b5_list as $list ) {
                                foreach( $list as $val ) { ?>
                                    <li><?=$val?></li>
                                    <?php
                                }
                            }
                            ?>
                  </ul>  
                    <a class="btn show-form uznB" href="<?=$this->params->get('uznBl')?>"><?=$this->params->get('uznB')?></a>      
                </div> 
            </div> 
</section>   


<section id="analitik" class="cd-section">
    <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="edit"><?=$this->params->get('b6-title')?></h2>
                </div> 
            </div> 
             <div class="row slider">
                 <script>
                 var analitik = [
                   {
                        img: '/<?=$this->params->get("b6-list-screen-1")?>',
                        text: '<?=$this->params->get("b6-list-text-1")?>'
                    },
                   {
                        img: '/<?=$this->params->get("b6-list-screen-2")?>',
                        text: '<?=$this->params->get("b6-list-text-2")?>'
                    },
                   {
                        img: '/<?=$this->params->get("b6-list-screen-3")?>',
                        text: '<?=$this->params->get("b6-list-text-3")?>'
                    },
                   {
                        img: '/<?=$this->params->get("b6-list-screen-4")?>',
                        text: '<?=$this->params->get("b6-list-text-4")?>'
                    },
                   {
                        img: '/<?=$this->params->get("b6-list-screen-5")?>',
                        text: '<?=$this->params->get("b6-list-text-5")?>'
                    },
                   
                 ];
                 </script>
                <div class="col-md-12">
                    <div id="device">
                        <span class="arrow a-l"></span>
                        <span class="arrow a-r activ"></span>
                        <div class="slide">
                        </div>    
                    </div>
                    <div class="befor-slide">
                    </div> 
                    <div class="affor-slide">
                    </div> 
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dots">
                    <ul>
                    </ul>                
                </div> 

            </div> 
    </div> 
</section>  


<section id="how-do" class="cd-section">
    <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="edit"><?=$this->params->get('b7-title')?></h2>
                    <div class="text edit"><?=$this->params->get('b7-text')?></div>
                   <?php


$b7b1l1_list	 = json_decode( $this->params->get('b7b1l1-list'),true);
$b7b1l2_list	 = json_decode( $this->params->get('b7b1l2-list'),true);
$b7b2l1_list	 = json_decode( $this->params->get('b7b2l1-list'),true);
$b7_list_text	 = json_decode( $this->params->get('b7-list-text'),true);


$b11_listt	 = json_decode( $this->params->get('b11-list'),true);

?>
                    <div class="row">
                        <div class="col-md-12 bodyy">
                            <div class="row">
                                <div class="col-md-2 col-xs-6 col-sm-6 left">
                                    <h4 class="edit"><?=$this->params->get('b7b1-title')?></h4>
                                    
                                    <div class="city">
                                        <h5><?=$this->params->get('b7b1l1-title')?></h5>
                                        <ul>
                                          <?php
                                            foreach( $b7b1l1_list as $list ) {
                                                foreach( $list as $val ) { ?>
                                                    <li><?=$val?></li>
                                                    <?php
                                                }
                                            }
                                         ?>
                                        </ul>
                                    </div>
                                    <div class="city">
                                        <h5><?=$this->params->get('b7b1l2-title')?></h5>
                                        <ul>
                                           <?php
                                            foreach( $b7b1l2_list as $list ) {
                                                foreach( $list as $val ) { ?>
                                                    <li><?=$val?></li>
                                                    <?php
                                                }
                                            }
                                         ?>
                                        </ul>
                                    </div>

                                </div> 
                                <div class="col-md-8 col-xs-12 col-sm-12 center">
                                     <h4 class="edit"><?=$this->params->get('b7b3-title')?></h4>
                                    <img class="img-responsive" src="<?=$this->params->get('planFile')?>" alt="">
                                </div> 
                                <div class="col-md-2 col-xs-6 col-sm-6 right">
                                    <h4 class="edit"><?=$this->params->get('b7b2-title')?></h4>
                                    <ul>
                                        <?php
                                            foreach( $b7b2l1_list as $list ) {
                                                foreach( $list as $val ) { ?>
                                                    <li><?=$val?></li>
                                                    <?php
                                                }
                                            }
                                         ?>
                                    </ul>

                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div> 
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="how-do-slide">
                                          <?php
                                            foreach( $b7_list_text as $list ) {
                                                foreach( $list as $val ) { ?>
                                                    <p><?=$val?></p>
                                                    <?php
                                                }
                                            }
                                         ?>
                    </div>
                </div>                 
            </div> 
        </div> 
</section>


                    <?php if ($this->countModules('rates')) : ?>
                            <jdoc:include type="modules" name="rates" />
	                <?php endif; ?>


<section id="choices-to-make" class="cd-section">
    <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="edit"><?=$this->params->get('b8-title')?></h2>
                    <p class="text edit"><?=$this->params->get('b8-text')?></p>
                    <div class="row">
                    <div class="col-md-8 col-md-offset-2 conracts-form">
                        <form action="" class="contact-form">
                        <div class="error-info"><?=$this->params->get('c-f-error')?></div>
                        <div class="compile-info"><?=$this->params->get('c-f-compile')?></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-4">           
                                            <input class="form-control name" name="name" type="text" placeholder="<?php echo JText::_('TPL_XEMLAB_FORMS_FIELDS_INPUT_NAME'); ?>">
                                        </div>
                                        <div class="form-group col-md-4">           
                                            <input class="form-control tel" name="tel" type="text" placeholder="<?php echo JText::_('TPL_XEMLAB_FORMS_FIELDS_INPUT_TELL'); ?>">
                                        </div>
                                        <div class="form-group col-md-4">           
                                            <button class="subm"><?=$this->params->get('b8-button')?></button>
                                        </div>
                                    </div>
                                </div>        
                            </div>        
                        </form>
                    </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
</section>   


<section id="wherefore" class="cd-section">
    <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="edit"><?=$this->params->get('b9-title')?></h2>
                    <div class="row">
                        <div class="col-md-4 blocks block1">
                            <div class="icon">
                                <img src="<?=$this->params->get('b9-img-1')?>" alt="">
                            </div>
                            <div class="text"><?=$this->params->get('b9-text1')?></div> 
                        </div> 
                        <div class="col-md-4 blocks block2">
                            <div class="icon">
                                <img src="<?=$this->params->get('b9-img-2')?>" alt="">
                            </div>
                            <div class="text"><?=$this->params->get('b9-text2')?></div>
                        </div> 
                        <div class="col-md-4 blocks block3">
                            <div class="icon">
                                <img src="<?=$this->params->get('b9-img-3')?>" alt="">
                            </div>
                            <div class="text"><?=$this->params->get('b9-text3')?></div>
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
</section>   
<?php if ($this->params->get('b10-show')) { ?>
<section id="our-download" class="cd-section">
    <div class="container">
            <div class="row">
                <div class="col-md-7">
                   <h2 class="edit"><?=$this->params->get('b10-title')?></h2>
                    <div class="text edit"><?=$this->params->get('b10-text')?></div>   
                    <a class="btn a-store" href="<?=$this->params->get('b10-button-1-url')?>"><?=$this->params->get('b10-button-1')?></a>  
                    <a class="btn g-store" href="<?=$this->params->get('b10-button-2-url')?>"><?=$this->params->get('b10-button-2')?></a>  
                </div> 
                <div class="col-md-5 iphone-6">
               <img class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="300ms" src="<?=$this->params->get('b10-img')?>" alt="">
                    
                </div> 
            </div> 
        </div> 
</section>  
<?php } ?>
<?php if ($this->countModules('clients')) : ?>
      <jdoc:include type="modules" name="clients" />
<?php endif; ?>

<section id="about" class="cd-section">
    <div class="container">
            <div class="row">
                <div class="col-md-6 block1">
                    <img class="img-responsive" src="<?=$this->params->get('b11-img')?>" alt="">
                </div> 
                <div class="col-md-6 block2">
                    <h2 class="edit"><?=$this->params->get('b11-title')?></h2>
                    <div class="text edit">
                        <?=$this->params->get('b11-text')?>
                        <div class="row top-50">
                            <div class="col-md-3 sub-img">
                                <img class="img-responsive" src="<?=$this->params->get('b11-img-1c')?>" alt="">
                            </div> 
                            <div class="col-md-9 sub-text edit"><?=$this->params->get('b11-text-sub')?> </div> 
                        </div> 
                    </div>
                    <a href="#" class="consul btn show-form"><?=$this->params->get('b11-button')?></a>
                </div> 
            </div> 
    </div> 
</section>   
<section id="conracts" class="cd-section">
    <div class="container">
            <div class="row rowcont">
                <div class="col-md-4 blocks block1">
                    <div class="icon">
                        <img src="images/icon/tel.png" alt="">
                    </div>
                    <div class="text">
                        <?=$this->params->get('c-num')?>
                    </div> 
                </div> 
                <div class="col-md-4 blocks block2">
                    <div class="icon">
                        <img src="images/icon/pointer.png" alt="">
                    </div>
                    <div class="text edit">
                        <?=$this->params->get('c-addres')?>
                    </div>
                </div> 
                <div class="col-md-4 blocks block3">
                    <div class="icon">
                        <img src="images/icon/envelope.png" alt="">
                    </div>
                    <div class="text">
                        <?=$this->params->get('c-mail')?>
                    </div> 
                </div> 
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 conracts-form wow fadeInUp" data-wow-duration="500ms" data-wow-delay="200ms">
                        <h2><?=$this->params->get('c-f-title')?></h2>
                        <div class="text edit"><?=$this->params->get('c-f-text')?></div>
                        <form class="contact-form">
                        <div class="error-info"><?=$this->params->get('c-f-error')?></div>
                        <div class="compile-info"><?=$this->params->get('c-f-compile')?></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-4">           
                                            <input class="form-control name" name="name" type="text" placeholder="<?php echo JText::_('TPL_XEMLAB_FORMS_FIELDS_INPUT_NAME'); ?>">
                                        </div>
                                        <div class="form-group col-md-4">           
                                            <input class="form-control tel" name="tel" type="text" placeholder="<?php echo JText::_('TPL_XEMLAB_FORMS_FIELDS_INPUT_TELL'); ?>">
                                        </div>
                                        <div class="form-group col-md-4">           
                                            <input class="form-control email" name="email" type="text" placeholder="<?php echo JText::_('TPL_XEMLAB_FORMS_FIELDS_INPUT_EMAIL'); ?>">
                                        </div>
                                        <input name="prefix" type="hidden" value="<?=$this->params->get('c-f-title')?>" >
                                        <div class="form-group col-md-12">   
                                            <button class="subm"><?=$this->params->get('c-f-btn')?></button>
                                        </div>
                                    </div>
                                </div>        
                            </div>        
                        </form>
                    </div>    
                </div>    
            </div> 
    </div> 
</section>   


<footer>
       <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-3 edit">
                <?=$this->params->get('f-copy')?>
                </div> 
                <div class="col-lg-8 col-md-6 text-center">
                   <a href="<?=$this->params->get('f-link')?>" class="f-link"> <?=$this->params->get('f-link-text')?></a> 
                </div> 
                <div class="col-lg-2 col-md-3">
                </div> 
            </div> 
        </div> 
</footer>
<div class="popup-form">   
                        <form action="" class="contact-form">
                        <div class="error-info"><?=$this->params->get('c-f-error')?></div>
                        <div class="compile-info"><?=$this->params->get('c-f-compile')?></div>
                            <i class="close"></i>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Заказать звонок </h3>
                                    
                                    <div class="row">
                                        <div class="form-group col-md-12 name">           
                                            <input class="form-control " name="name" type="text" placeholder="<?php echo JText::_('TPL_XEMLAB_FORMS_FIELDS_INPUT_NAME'); ?>">
                                        </div>
                                        <div class="form-group col-md-12 tel">           
                                            <input class="form-control" name="tel" type="text" placeholder="<?php echo JText::_('TPL_XEMLAB_FORMS_FIELDS_INPUT_TELL'); ?>">
                                        </div>
                                    <input id="prefix" name="prefix" type="hidden">
                                        <div class="form-group col-md-12">           
                                            <button class="subm">Заказать</button>
                                        </div>
                                        <div class="col-md-12">           
                                            <p class="forminfo">Ваши данные в сохранности</p>
                                        </div>
                                    </div>
                                </div>        
                            </div>        
                        </form>

 </div>
	<nav id="cd-vertical-nav">
		<ul>
		</ul>
	</nav>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?=$template_url?>/js/bootstrap.min.js"></script>
    <script src="<?=$template_url?>/js/slick.min.js"></script>
    <script src="<?=$template_url?>/js/wow.min.js"></script>
    <script src="<?=$template_url?>/js/app.js"></script>
  </body>
</html>
