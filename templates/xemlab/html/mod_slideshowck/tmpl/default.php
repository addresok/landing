<?php
/**
 * @copyright	Copyright (C) 2012 Cedric KEIFLIN alias ced1870
 * http://www.joomlack.fr
 * Module Slideshow CK
 * @license		GNU/GPL
 * */
// no direct access
defined('_JEXEC') or die('Restricted access');

// définit la largeur du slideshow
$width = ($params->get('width') AND $params->get('width') != 'auto') ? ' style="width:' . $params->get('width') . 'px;"' : '';
$needJModal = false;
?>
<!-- debut Slideshow CK -->

 <div class="our-clients-slide owl-carousel">
<?php

foreach ($items as $i => $item) { ?>
 <img class="item" src="<?=$item->imgname?>" alt="">          
<?php  } ?>
</div>


