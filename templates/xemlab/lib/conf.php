<?php defined('_JEXEC') or die;
require_once ('xemlab.php');

$template_url = $this->baseurl . '/templates/' . $this->template;

$xem = new \xem\xemlab();
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$menu = $app->getMenu();
$lang = JFactory::getLanguage();

$is_home_page = $menu->getActive() == $menu->getDefault($lang->getTag());
$headlink = $this->getHeadData();
//unset($this->_generator);
$this->_generator = 'UTP';
unset(
	$headlink['scripts']['/media/jui/js/jquery.min.js'],
	$headlink['scripts']['/media/jui/js/jquery-noconflict.js'],
	$headlink['scripts']['/media/system/js/caption.js'],
	$headlink['script']['text/javascript'],
	$headlink['scripts']['/media/jui/js/jquery-migrate.min.js'],
	$headlink['scripts']['/media/jui/js/bootstrap.min.js'],
	$doc->_scripts['/media/jui/js/jquery.min.js'],
	$doc->_scripts['/media/jui/js/jquery-noconflict.js'],
	$doc->_scripts['/media/system/js/caption.js'],
	$doc->_scripts['/media/jui/js/bootstrap.min.js'],
	$doc->_script['text/javascript'],
	$doc->_scripts['/media/jui/js/jquery-migrate.min.js']
);

$this->setHeadData($headlink);

$langTag = $doc->getLanguage();
$directionTag = $doc->getLanguage();

$template_url = $this->baseurl . '/templates/' . $this->template;

//$doc->addStyleSheet($template_url . '/css/style.css');
