var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync');


    // gulp.task('mytask', function() {
    //     console.log('Привет, я таск!');
    // });

    gulp.task('browser-sync', function() { 
        browserSync({ 
            server: { 
                baseDir: './' 
            },
            notify: false 
        });
    });

gulp.task('scss', function(){ 
    return gulp.src('css/scss/**/*.scss') 
        .pipe(sass({
            includePaths: require('node-bourbon').includePaths
        })) 
        .pipe(gulp.dest('css')) 
        //.pipe(browserSync.reload({stream: true}))
});
    gulp.task('watch', ['scss'],function() {
        gulp.watch('css/scss/**/*.scss', ['scss']); 
        //gulp.watch('./*.html', browserSync.reload);
        //gulp.watch('./js/**/*.js', browserSync.reload);
    });
