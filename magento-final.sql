-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 20 2017 г., 04:21
-- Версия сервера: 5.7.18-0ubuntu0.16.04.1
-- Версия PHP: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `magento`
--

-- --------------------------------------------------------

--
-- Структура таблицы `lend_assets`
--

CREATE TABLE `lend_assets` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) UNSIGNED NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_assets`
--

INSERT INTO `lend_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 0, 137, 0, 'root.1', 'Root Asset', '{"core.login.site":{"6":1,"2":1},"core.login.admin":{"6":1},"core.login.offline":{"6":1},"core.admin":{"8":1},"core.manage":{"7":1},"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(8, 1, 17, 20, 1, 'com_content', 'com_content', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(9, 1, 21, 22, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 23, 24, 1, 'com_installer', 'com_installer', '{"core.manage":{"7":0},"core.delete":{"7":0},"core.edit.state":{"7":0}}'),
(11, 1, 25, 26, 1, 'com_languages', 'com_languages', '{"core.admin":{"7":1}}'),
(12, 1, 27, 28, 1, 'com_login', 'com_login', '{}'),
(13, 1, 29, 30, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 31, 32, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 33, 34, 1, 'com_media', 'com_media', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":{"5":1}}'),
(16, 1, 35, 44, 1, 'com_menus', 'com_menus', '{"core.admin":{"7":1}}'),
(17, 1, 45, 46, 1, 'com_messages', 'com_messages', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(18, 1, 47, 100, 1, 'com_modules', 'com_modules', '{"core.admin":{"7":1}}'),
(19, 1, 101, 104, 1, 'com_newsfeeds', 'com_newsfeeds', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(20, 1, 105, 106, 1, 'com_plugins', 'com_plugins', '{"core.admin":{"7":1}}'),
(21, 1, 107, 108, 1, 'com_redirect', 'com_redirect', '{"core.admin":{"7":1}}'),
(22, 1, 109, 110, 1, 'com_search', 'com_search', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(23, 1, 111, 112, 1, 'com_templates', 'com_templates', '{"core.admin":{"7":1}}'),
(24, 1, 113, 116, 1, 'com_users', 'com_users', '{"core.admin":{"7":1}}'),
(26, 1, 117, 118, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 19, 2, 'com_content.category.2', 'Uncategorised', '{}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{}'),
(30, 19, 102, 103, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{}'),
(32, 24, 114, 115, 2, 'com_users.category.7', 'Uncategorised', '{}'),
(33, 1, 119, 120, 1, 'com_finder', 'com_finder', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(34, 1, 121, 122, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{}'),
(35, 1, 123, 124, 1, 'com_tags', 'com_tags', '{}'),
(36, 1, 125, 126, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(37, 1, 127, 128, 1, 'com_ajax', 'com_ajax', '{}'),
(38, 1, 129, 130, 1, 'com_postinstall', 'com_postinstall', '{}'),
(39, 18, 48, 49, 2, 'com_modules.module.1', 'Main Menu rus', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(40, 18, 50, 51, 2, 'com_modules.module.2', 'Login', '{}'),
(41, 18, 52, 53, 2, 'com_modules.module.3', 'Popular Articles', '{}'),
(42, 18, 54, 55, 2, 'com_modules.module.4', 'Recently Added Articles', '{}'),
(43, 18, 56, 57, 2, 'com_modules.module.8', 'Toolbar', '{}'),
(44, 18, 58, 59, 2, 'com_modules.module.9', 'Quick Icons', '{}'),
(45, 18, 60, 61, 2, 'com_modules.module.10', 'Logged-in Users', '{}'),
(46, 18, 62, 63, 2, 'com_modules.module.12', 'Admin Menu', '{}'),
(47, 18, 64, 65, 2, 'com_modules.module.13', 'Admin Submenu', '{}'),
(48, 18, 66, 67, 2, 'com_modules.module.14', 'User Status', '{}'),
(49, 18, 68, 69, 2, 'com_modules.module.15', 'Title', '{}'),
(50, 18, 70, 71, 2, 'com_modules.module.16', 'Login Form', '{}'),
(51, 18, 72, 73, 2, 'com_modules.module.17', 'Breadcrumbs', '{}'),
(52, 18, 74, 75, 2, 'com_modules.module.79', 'Multilanguage status', '{}'),
(53, 18, 76, 77, 2, 'com_modules.module.86', 'Joomla Version', '{}'),
(54, 16, 36, 37, 2, 'com_menus.menu.1', 'Main Menu', '{}'),
(55, 1, 131, 132, 1, '#__languages.2', '#__languages.2', '{}'),
(56, 1, 133, 134, 1, '#__languages.3', '#__languages.3', '{}'),
(57, 16, 38, 39, 2, 'com_menus.menu.2', 'Рус ', '{}'),
(58, 16, 40, 41, 2, 'com_menus.menu.3', 'Eng', '{}'),
(59, 16, 42, 43, 2, 'com_menus.menu.4', 'DE', '{}'),
(60, 18, 78, 79, 2, 'com_modules.module.87', 'Переключение языков ', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(61, 1, 135, 136, 1, '#__languages.1', '#__languages.1', '{}'),
(62, 18, 80, 81, 2, 'com_modules.module.88', 'Flexheader3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(63, 18, 82, 83, 2, 'com_modules.module.89', 'Slideshow CK', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(64, 18, 84, 85, 2, 'com_modules.module.90', 'клиенты en', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(65, 18, 86, 87, 2, 'com_modules.module.91', 'rates en', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(66, 18, 88, 89, 2, 'com_modules.module.92', 'Main Menu en', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(67, 18, 90, 91, 2, 'com_modules.module.93', 'Main Menu de', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(68, 18, 92, 93, 2, 'com_modules.module.94', 'rates rus', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(69, 18, 94, 95, 2, 'com_modules.module.95', 'rates de', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(70, 18, 96, 97, 2, 'com_modules.module.96', 'клиенты rus', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(71, 18, 98, 99, 2, 'com_modules.module.97', 'клиенты de', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_associations`
--

CREATE TABLE `lend_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_associations`
--

INSERT INTO `lend_associations` (`id`, `context`, `key`) VALUES
(102, 'com_menus.item', '506d0f16884602f0dc51102317bd1f29'),
(103, 'com_menus.item', '506d0f16884602f0dc51102317bd1f29'),
(104, 'com_menus.item', '506d0f16884602f0dc51102317bd1f29');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_banners`
--

CREATE TABLE `lend_banners` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custombannercode` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sticky` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_banner_clients`
--

CREATE TABLE `lend_banner_clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extrainfo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_banner_tracks`
--

CREATE TABLE `lend_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) UNSIGNED NOT NULL,
  `banner_id` int(10) UNSIGNED NOT NULL,
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_categories`
--

CREATE TABLE `lend_categories` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_categories`
--

INSERT INTO `lend_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 11, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '{}', 535, '2017-06-14 17:54:32', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 535, '2017-06-14 17:54:32', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 535, '2017-06-14 17:54:32', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 535, '2017-06-14 17:54:32', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 535, '2017-06-14 17:54:32', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 9, 10, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 535, '2017-06-14 17:54:32', 0, '0000-00-00 00:00:00', 0, '*', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_contact_details`
--

CREATE TABLE `lend_contact_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `con_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `suburb` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `misc` mediumtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_con` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `webpage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if contact is featured.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_content`
--

CREATE TABLE `lend_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `introtext` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fulltext` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribs` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_contentitem_tag_map`
--

CREATE TABLE `lend_contentitem_tag_map` (
  `type_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_content_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Структура таблицы `lend_content_frontpage`
--

CREATE TABLE `lend_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_content_rating`
--

CREATE TABLE `lend_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_content_types`
--

CREATE TABLE `lend_content_types` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `type_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `rules` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_mappings` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `router` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'JSON string for com_contenthistory options'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_content_types`
--

INSERT INTO `lend_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{"special":{"dbtable":"#__content","key":"id","type":"Content","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"state","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"introtext", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"attribs", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"asset_id"}, "special":{"fulltext":"fulltext"}}', 'ContentHelperRoute::getArticleRoute', '{"formFile":"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml", "hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(2, 'Contact', 'com_contact.contact', '{"special":{"dbtable":"#__contact_details","key":"id","type":"Contact","prefix":"ContactTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"address", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"image", "core_urls":"webpage", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"con_position":"con_position","suburb":"suburb","state":"state","country":"country","postcode":"postcode","telephone":"telephone","fax":"fax","misc":"misc","email_to":"email_to","default_con":"default_con","user_id":"user_id","mobile":"mobile","sortname1":"sortname1","sortname2":"sortname2","sortname3":"sortname3"}}', 'ContactHelperRoute::getContactRoute', '{"formFile":"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml","hideFields":["default_con","checked_out","checked_out_time","version","xreference"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"], "displayLookup":[ {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ] }'),
(3, 'Newsfeed', 'com_newsfeeds.newsfeed', '{"special":{"dbtable":"#__newsfeeds","key":"id","type":"Newsfeed","prefix":"NewsfeedsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"numarticles":"numarticles","cache_time":"cache_time","rtl":"rtl"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{"formFile":"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml","hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(4, 'User', 'com_users.user', '{"special":{"dbtable":"#__users","key":"id","type":"User","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"null","core_alias":"username","core_created_time":"registerdate","core_modified_time":"lastvisitDate","core_body":"null", "core_hits":"null","core_publish_up":"null","core_publish_down":"null","access":"null", "core_params":"params", "core_featured":"null", "core_metadata":"null", "core_language":"null", "core_images":"null", "core_urls":"null", "core_version":"null", "core_ordering":"null", "core_metakey":"null", "core_metadesc":"null", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{}}', 'UsersHelperRoute::getUserRoute', ''),
(5, 'Article Category', 'com_content.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContentHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(6, 'Contact Category', 'com_contact.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContactHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(7, 'Newsfeeds Category', 'com_newsfeeds.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(8, 'Tag', 'com_tags.tag', '{"special":{"dbtable":"#__tags","key":"tag_id","type":"Tag","prefix":"TagsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path"}}', 'TagsHelperRoute::getTagRoute', '{"formFile":"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml", "hideFields":["checked_out","checked_out_time","version", "lft", "rgt", "level", "path", "urls", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(9, 'Banner', 'com_banners.banner', '{"special":{"dbtable":"#__banners","key":"id","type":"Banner","prefix":"BannersTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"null","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"null", "asset_id":"null"}, "special":{"imptotal":"imptotal", "impmade":"impmade", "clicks":"clicks", "clickurl":"clickurl", "custombannercode":"custombannercode", "cid":"cid", "purchase_type":"purchase_type", "track_impressions":"track_impressions", "track_clicks":"track_clicks"}}', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml", "hideFields":["checked_out","checked_out_time","version", "reset"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "imptotal", "impmade", "reset"], "convertToInt":["publish_up", "publish_down", "ordering"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"cid","targetTable":"#__banner_clients","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(10, 'Banners Category', 'com_banners.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(11, 'Banner Client', 'com_banners.client', '{"special":{"dbtable":"#__banner_clients","key":"id","type":"Client","prefix":"BannersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml", "hideFields":["checked_out","checked_out_time"], "ignoreChanges":["checked_out", "checked_out_time"], "convertToInt":[], "displayLookup":[]}'),
(12, 'User Notes', 'com_users.note', '{"special":{"dbtable":"#__user_notes","key":"id","type":"Note","prefix":"UsersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml", "hideFields":["checked_out","checked_out_time", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(13, 'User Notes Category', 'com_users.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_core_log_searches`
--

CREATE TABLE `lend_core_log_searches` (
  `search_term` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_extensions`
--

CREATE TABLE `lend_extensions` (
  `extension_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Parent package ID for extensions installed as a package.',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `system_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_extensions`
--

INSERT INTO `lend_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 0, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{"name":"com_mailto","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MAILTO_XML_DESCRIPTION","group":"","filename":"mailto"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 0, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{"name":"com_wrapper","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_WRAPPER_XML_DESCRIPTION","group":"","filename":"wrapper"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 0, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{"name":"com_admin","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_ADMIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 0, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{"name":"com_banners","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_BANNERS_XML_DESCRIPTION","group":"","filename":"banners"}', '{"purchase_type":"3","track_impressions":"0","track_clicks":"0","metakey_prefix":"","save_history":"1","history_limit":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 0, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{"name":"com_cache","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CACHE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 0, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{"name":"com_categories","type":"component","creationDate":"December 2007","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 0, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{"name":"com_checkin","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CHECKIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 0, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{"name":"com_contact","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTACT_XML_DESCRIPTION","group":"","filename":"contact"}', '{"contact_layout":"_:default","show_contact_category":"hide","save_history":"1","history_limit":10,"show_contact_list":"0","presentation_style":"sliders","show_tags":"1","show_info":"1","show_name":"1","show_position":"1","show_email":"0","show_street_address":"1","show_suburb":"1","show_state":"1","show_postcode":"1","show_country":"1","show_telephone":"1","show_mobile":"1","show_fax":"1","show_webpage":"1","show_image":"1","show_misc":"1","image":"","allow_vcard":"0","show_articles":"0","articles_display_num":"10","show_profile":"0","show_user_custom_fields":["-1"],"show_links":"0","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","contact_icons":"0","icon_address":"","icon_email":"","icon_telephone":"","icon_mobile":"","icon_fax":"","icon_misc":"","category_layout":"_:default","show_category_title":"1","show_description":"1","show_description_image":"0","maxLevel":"-1","show_subcat_desc":"1","show_empty_categories":"0","show_cat_items":"1","show_cat_tags":"1","show_base_description":"1","maxLevelcat":"-1","show_subcat_desc_cat":"1","show_empty_categories_cat":"0","show_cat_items_cat":"1","filter_field":"0","show_pagination_limit":"0","show_headings":"1","show_image_heading":"0","show_position_headings":"1","show_email_headings":"0","show_telephone_headings":"1","show_mobile_headings":"0","show_fax_headings":"0","show_suburb_headings":"1","show_state_headings":"1","show_country_headings":"1","show_pagination":"2","show_pagination_results":"1","initial_sort":"ordering","captcha":"","show_email_form":"1","show_email_copy":"1","banned_email":"","banned_subject":"","banned_text":"","validate_session":"1","custom_reply":"0","redirect":"","show_feed_link":"1","sef_advanced":0,"sef_ids":0,"custom_fields_enable":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 0, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{"name":"com_cpanel","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CPANEL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 0, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{"name":"com_installer","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_INSTALLER_XML_DESCRIPTION","group":""}', '{"show_jed_info":"1","cachetimeout":"6","minimum_stability":"4"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 0, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{"name":"com_languages","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LANGUAGES_XML_DESCRIPTION","group":""}', '{"administrator":"ru-RU","site":"en-GB"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 0, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{"name":"com_login","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 0, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{"name":"com_media","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MEDIA_XML_DESCRIPTION","group":"","filename":"media"}', '{"upload_extensions":"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS","upload_maxsize":"10","file_path":"images","image_path":"images","restrict_uploads":"1","allowed_media_usergroup":"3","check_mime":"1","image_extensions":"bmp,gif,jpg,png","ignore_extensions":"","upload_mime":"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip","upload_mime_illegal":"text\\/html"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 0, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{"name":"com_menus","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MENUS_XML_DESCRIPTION","group":""}', '{"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 0, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{"name":"com_messages","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MESSAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 0, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{"name":"com_modules","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MODULES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 0, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{"name":"com_newsfeeds","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '{"newsfeed_layout":"_:default","save_history":"1","history_limit":5,"show_feed_image":"1","show_feed_description":"1","show_item_description":"1","feed_character_count":"0","feed_display_order":"des","float_first":"right","float_second":"right","show_tags":"1","category_layout":"_:default","show_category_title":"1","show_description":"1","show_description_image":"1","maxLevel":"-1","show_empty_categories":"0","show_subcat_desc":"1","show_cat_items":"1","show_cat_tags":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_items_cat":"1","filter_field":"1","show_pagination_limit":"1","show_headings":"1","show_articles":"0","show_link":"1","show_pagination":"1","show_pagination_results":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 0, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{"name":"com_plugins","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_PLUGINS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 0, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{"name":"com_search","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_SEARCH_XML_DESCRIPTION","group":"","filename":"search"}', '{"enabled":"0","search_phrases":"1","search_areas":"1","show_date":"1","opensearch_name":"","opensearch_description":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 0, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{"name":"com_templates","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_TEMPLATES_XML_DESCRIPTION","group":""}', '{"template_positions_display":"0","upload_limit":"10","image_formats":"gif,bmp,jpg,jpeg,png","source_formats":"txt,less,ini,xml,js,php,css,scss,sass","font_formats":"woff,ttf,otf","compressed_formats":"zip"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 0, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{"name":"com_content","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '{"article_layout":"_:default","show_title":"1","link_titles":"1","show_intro":"1","show_category":"1","link_category":"1","show_parent_category":"0","link_parent_category":"0","show_author":"1","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"1","show_item_navigation":"1","show_vote":"0","show_readmore":"1","show_readmore_title":"1","readmore_limit":"100","show_icons":"1","show_print_icon":"1","show_email_icon":"1","show_hits":"1","show_noauth":"0","show_publishing_options":"1","show_article_options":"1","save_history":"1","history_limit":10,"show_urls_images_frontend":"0","show_urls_images_backend":"1","targeta":0,"targetb":0,"targetc":0,"float_intro":"left","float_fulltext":"left","category_layout":"_:blog","show_category_title":"0","show_description":"0","show_description_image":"0","maxLevel":"1","show_empty_categories":"0","show_no_articles":"1","show_subcat_desc":"1","show_cat_num_articles":"0","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_num_articles_cat":"1","num_leading_articles":"1","num_intro_articles":"4","num_columns":"2","num_links":"4","multi_column_order":"0","show_subcategory_content":"0","show_pagination_limit":"1","filter_field":"hide","show_headings":"1","list_show_date":"0","date_format":"","list_show_hits":"1","list_show_author":"1","orderby_pri":"order","orderby_sec":"rdate","order_date":"published","show_pagination":"2","show_pagination_results":"1","show_feed_link":"1","feed_summary":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 0, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{"name":"com_config","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONFIG_XML_DESCRIPTION","group":""}', '{"filters":{"1":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"9":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"6":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"7":{"filter_type":"NONE","filter_tags":"","filter_attributes":""},"2":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"3":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"4":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"5":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"8":{"filter_type":"NONE","filter_tags":"","filter_attributes":""}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 0, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{"name":"com_redirect","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_REDIRECT_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 0, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{"name":"com_users","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_USERS_XML_DESCRIPTION","group":"","filename":"users"}', '{"allowUserRegistration":"0","new_usertype":"2","guest_usergroup":"9","sendpassword":"1","useractivation":"2","mail_to_admin":"1","captcha":"","frontend_userparams":"1","site_language":"0","change_login_name":"0","reset_count":"10","reset_time":"1","minimum_length":"4","minimum_integers":"0","minimum_symbols":"0","minimum_uppercase":"0","save_history":"1","history_limit":5,"mailSubjectPrefix":"","mailBodySuffix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 0, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{"name":"com_finder","type":"component","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_FINDER_XML_DESCRIPTION","group":"","filename":"finder"}', '{"show_description":"1","description_length":255,"allow_empty_query":"0","show_url":"1","show_advanced":"1","expand_advanced":"0","show_date_filters":"0","highlight_terms":"1","opensearch_name":"","opensearch_description":"","batch_size":"50","memory_table_limit":30000,"title_multiplier":"1.7","text_multiplier":"0.7","meta_multiplier":"1.2","path_multiplier":"2.0","misc_multiplier":"0.3","stemmer":"snowball"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 0, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{"name":"com_joomlaupdate","type":"component","creationDate":"February 2012","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.6.2","description":"COM_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '{"updatesource":"default","customurl":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 0, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{"name":"com_tags","type":"component","creationDate":"December 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"COM_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '{"tag_layout":"_:default","save_history":"1","history_limit":5,"show_tag_title":"0","tag_list_show_tag_image":"0","tag_list_show_tag_description":"0","tag_list_image":"","tag_list_orderby":"title","tag_list_orderby_direction":"ASC","show_headings":"0","tag_list_show_date":"0","tag_list_show_item_image":"0","tag_list_show_item_description":"0","tag_list_item_maximum_characters":0,"return_any_or_all":"1","include_children":"0","maximum":200,"tag_list_language_filter":"all","tags_layout":"_:default","all_tags_orderby":"title","all_tags_orderby_direction":"ASC","all_tags_show_tag_image":"0","all_tags_show_tag_descripion":"0","all_tags_tag_maximum_characters":20,"all_tags_show_tag_hits":"0","filter_field":"1","show_pagination_limit":"1","show_pagination":"2","show_pagination_results":"1","tag_field_ajax_mode":"1","show_feed_link":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 0, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{"name":"com_contenthistory","type":"component","creationDate":"May 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_CONTENTHISTORY_XML_DESCRIPTION","group":"","filename":"contenthistory"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 0, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 1, '{"name":"com_ajax","type":"component","creationDate":"August 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_AJAX_XML_DESCRIPTION","group":"","filename":"ajax"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 0, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{"name":"com_postinstall","type":"component","creationDate":"September 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_POSTINSTALL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(33, 0, 'com_fields', 'component', 'com_fields', '', 1, 1, 1, 0, '{"name":"com_fields","type":"component","creationDate":"March 2016","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"COM_FIELDS_XML_DESCRIPTION","group":"","filename":"fields"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(34, 0, 'com_associations', 'component', 'com_associations', '', 1, 1, 1, 0, '{"name":"com_associations","type":"component","creationDate":"Januar 2017","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"COM_ASSOCIATIONS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 0, 'LIB_PHPUTF8', 'library', 'phputf8', '', 0, 1, 1, 1, '{"name":"LIB_PHPUTF8","type":"library","creationDate":"2006","author":"Harry Fuecks","copyright":"Copyright various authors","authorEmail":"hfuecks@gmail.com","authorUrl":"http:\\/\\/sourceforge.net\\/projects\\/phputf8","version":"0.5","description":"LIB_PHPUTF8_XML_DESCRIPTION","group":"","filename":"phputf8"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 0, 'LIB_JOOMLA', 'library', 'joomla', '', 0, 1, 1, 1, '{"name":"LIB_JOOMLA","type":"library","creationDate":"2008","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"https:\\/\\/www.joomla.org","version":"13.1","description":"LIB_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '{"mediaversion":"5525947f0aff0ab5f626eb58c21bbd5e"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 0, 'LIB_IDNA', 'library', 'idna_convert', '', 0, 1, 1, 1, '{"name":"LIB_IDNA","type":"library","creationDate":"2004","author":"phlyLabs","copyright":"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de","authorEmail":"phlymail@phlylabs.de","authorUrl":"http:\\/\\/phlylabs.de","version":"0.8.0","description":"LIB_IDNA_XML_DESCRIPTION","group":"","filename":"idna_convert"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 0, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{"name":"FOF","type":"library","creationDate":"2015-04-22 13:15:32","author":"Nicholas K. Dionysopoulos \\/ Akeeba Ltd","copyright":"(C)2011-2015 Nicholas K. Dionysopoulos","authorEmail":"nicholas@akeebabackup.com","authorUrl":"https:\\/\\/www.akeebabackup.com","version":"2.4.3","description":"LIB_FOF_XML_DESCRIPTION","group":"","filename":"fof"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 0, 'LIB_PHPASS', 'library', 'phpass', '', 0, 1, 1, 1, '{"name":"LIB_PHPASS","type":"library","creationDate":"2004-2006","author":"Solar Designer","copyright":"","authorEmail":"solar@openwall.com","authorUrl":"http:\\/\\/www.openwall.com\\/phpass\\/","version":"0.3","description":"LIB_PHPASS_XML_DESCRIPTION","group":"","filename":"phpass"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 0, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{"name":"mod_articles_archive","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION","group":"","filename":"mod_articles_archive"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 0, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{"name":"mod_articles_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_NEWS_XML_DESCRIPTION","group":"","filename":"mod_articles_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 0, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{"name":"mod_articles_popular","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_articles_popular"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 0, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{"name":"mod_banners","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BANNERS_XML_DESCRIPTION","group":"","filename":"mod_banners"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 0, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{"name":"mod_breadcrumbs","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BREADCRUMBS_XML_DESCRIPTION","group":"","filename":"mod_breadcrumbs"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 0, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":"","filename":"mod_custom"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 0, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":"","filename":"mod_feed"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 0, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{"name":"mod_footer","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FOOTER_XML_DESCRIPTION","group":"","filename":"mod_footer"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 0, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":"","filename":"mod_login"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 0, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{"name":"mod_menu","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":"","filename":"mod_menu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 0, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{"name":"mod_articles_news","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_NEWS_XML_DESCRIPTION","group":"","filename":"mod_articles_news"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 0, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{"name":"mod_random_image","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RANDOM_IMAGE_XML_DESCRIPTION","group":"","filename":"mod_random_image"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 0, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{"name":"mod_related_items","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RELATED_XML_DESCRIPTION","group":"","filename":"mod_related_items"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 0, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{"name":"mod_search","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SEARCH_XML_DESCRIPTION","group":"","filename":"mod_search"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 0, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{"name":"mod_stats","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":"","filename":"mod_stats"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 0, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{"name":"mod_syndicate","type":"module","creationDate":"May 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SYNDICATE_XML_DESCRIPTION","group":"","filename":"mod_syndicate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 0, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{"name":"mod_users_latest","type":"module","creationDate":"December 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_USERS_LATEST_XML_DESCRIPTION","group":"","filename":"mod_users_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 0, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{"name":"mod_whosonline","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WHOSONLINE_XML_DESCRIPTION","group":"","filename":"mod_whosonline"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 0, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{"name":"mod_wrapper","type":"module","creationDate":"October 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WRAPPER_XML_DESCRIPTION","group":"","filename":"mod_wrapper"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 0, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{"name":"mod_articles_category","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION","group":"","filename":"mod_articles_category"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 0, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{"name":"mod_articles_categories","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION","group":"","filename":"mod_articles_categories"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 0, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{"name":"mod_languages","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.5.0","description":"MOD_LANGUAGES_XML_DESCRIPTION","group":"","filename":"mod_languages"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 0, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{"name":"mod_finder","type":"module","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FINDER_XML_DESCRIPTION","group":"","filename":"mod_finder"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 0, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":"","filename":"mod_custom"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 0, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":"","filename":"mod_feed"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 0, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{"name":"mod_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_XML_DESCRIPTION","group":"","filename":"mod_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 0, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{"name":"mod_logged","type":"module","creationDate":"January 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGGED_XML_DESCRIPTION","group":"","filename":"mod_logged"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 0, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"March 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":"","filename":"mod_login"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 0, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{"name":"mod_menu","type":"module","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":"","filename":"mod_menu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 0, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{"name":"mod_popular","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_popular"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 0, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{"name":"mod_quickicon","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_QUICKICON_XML_DESCRIPTION","group":"","filename":"mod_quickicon"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 0, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{"name":"mod_status","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATUS_XML_DESCRIPTION","group":"","filename":"mod_status"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 0, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{"name":"mod_submenu","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SUBMENU_XML_DESCRIPTION","group":"","filename":"mod_submenu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 0, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{"name":"mod_title","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TITLE_XML_DESCRIPTION","group":"","filename":"mod_title"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 0, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{"name":"mod_toolbar","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TOOLBAR_XML_DESCRIPTION","group":"","filename":"mod_toolbar"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 0, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{"name":"mod_multilangstatus","type":"module","creationDate":"September 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MULTILANGSTATUS_XML_DESCRIPTION","group":"","filename":"mod_multilangstatus"}', '{"cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 0, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{"name":"mod_version","type":"module","creationDate":"January 2012","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_VERSION_XML_DESCRIPTION","group":"","filename":"mod_version"}', '{"format":"short","product":"1","cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 0, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{"name":"mod_stats_admin","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":"","filename":"mod_stats_admin"}', '{"serverinfo":"0","siteinfo":"0","counter":"0","increase":"0","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 0, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{"name":"mod_tags_popular","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_tags_popular"}', '{"maximum":"5","timeframe":"alltime","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 0, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{"name":"mod_tags_similar","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_SIMILAR_XML_DESCRIPTION","group":"","filename":"mod_tags_similar"}', '{"maximum":"5","matchtype":"any","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 0, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_gmail","type":"plugin","creationDate":"February 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_GMAIL_XML_DESCRIPTION","group":"","filename":"gmail"}', '{"applysuffix":"0","suffix":"","verifypeer":"1","user_blacklist":""}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 0, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{"name":"plg_authentication_joomla","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 0, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_ldap","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LDAP_XML_DESCRIPTION","group":"","filename":"ldap"}', '{"host":"","port":"389","use_ldapV3":"0","negotiate_tls":"0","no_referrals":"0","auth_method":"bind","base_dn":"","search_string":"","users_dn":"","username":"admin","password":"bobby7","ldap_fullname":"fullName","ldap_email":"mail","ldap_uid":"uid"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(403, 0, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{"name":"plg_content_contact","type":"plugin","creationDate":"January 2014","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.2","description":"PLG_CONTENT_CONTACT_XML_DESCRIPTION","group":"","filename":"contact"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 0, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{"name":"plg_content_emailcloak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION","group":"","filename":"emailcloak"}', '{"mode":"1"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 0, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{"name":"plg_content_loadmodule","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOADMODULE_XML_DESCRIPTION","group":"","filename":"loadmodule"}', '{"style":"xhtml"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 0, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagebreak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION","group":"","filename":"pagebreak"}', '{"title":"1","multipage_toc":"1","showall":"1"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 0, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagenavigation","type":"plugin","creationDate":"January 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_PAGENAVIGATION_XML_DESCRIPTION","group":"","filename":"pagenavigation"}', '{"position":"1"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 0, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 0, '{"name":"plg_content_vote","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_VOTE_XML_DESCRIPTION","group":"","filename":"vote"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 0, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_codemirror","type":"plugin","creationDate":"28 March 2011","author":"Marijn Haverbeke","copyright":"Copyright (C) 2014 - 2017 by Marijn Haverbeke <marijnh@gmail.com> and others","authorEmail":"marijnh@gmail.com","authorUrl":"http:\\/\\/codemirror.net\\/","version":"5.25.2","description":"PLG_CODEMIRROR_XML_DESCRIPTION","group":"","filename":"codemirror"}', '{"lineNumbers":"1","lineWrapping":"1","matchTags":"1","matchBrackets":"1","marker-gutter":"1","autoCloseTags":"1","autoCloseBrackets":"1","autoFocus":"1","theme":"default","tabmode":"indent"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 0, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_none","type":"plugin","creationDate":"September 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_NONE_XML_DESCRIPTION","group":"","filename":"none"}', '', '', '', 535, '2017-06-17 08:35:46', 2, 0);
INSERT INTO `lend_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(412, 0, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{"name":"plg_editors_tinymce","type":"plugin","creationDate":"2005-2017","author":"Ephox Corporation","copyright":"Ephox Corporation","authorEmail":"N\\/A","authorUrl":"http:\\/\\/www.tinymce.com","version":"4.5.6","description":"PLG_TINY_XML_DESCRIPTION","group":"","filename":"tinymce"}', '{"configuration":{"toolbars":{"2":{"toolbar1":["bold","underline","strikethrough","|","undo","redo","|","bullist","numlist","|","pastetext"]},"1":{"menu":["edit","insert","view","format","table","tools"],"toolbar1":["bold","italic","underline","strikethrough","|","alignleft","aligncenter","alignright","alignjustify","|","formatselect","|","bullist","numlist","|","outdent","indent","|","undo","redo","|","link","unlink","anchor","code","|","hr","table","|","subscript","superscript","|","charmap","pastetext","preview"]},"0":{"menu":["edit","insert","view","format","table","tools"],"toolbar1":["bold","italic","underline","strikethrough","|","alignleft","aligncenter","alignright","alignjustify","|","styleselect","|","formatselect","fontselect","fontsizeselect","|","searchreplace","|","bullist","numlist","|","outdent","indent","|","undo","redo","|","link","unlink","anchor","image","|","code","|","forecolor","backcolor","|","fullscreen","|","table","|","subscript","superscript","|","charmap","emoticons","media","hr","ltr","rtl","|","cut","copy","paste","pastetext","|","visualchars","visualblocks","nonbreaking","blockquote","template","|","print","preview","codesample","insertdatetime","removeformat"]}},"setoptions":{"2":{"access":["1"],"skin":"0","skin_admin":"0","mobile":"0","drag_drop":"1","path":"","entity_encoding":"raw","lang_mode":"1","text_direction":"ltr","content_css":"1","content_css_custom":"","relative_urls":"1","newlines":"0","use_config_textfilters":"0","invalid_elements":"script,applet,iframe","valid_elements":"","extended_elements":"","resizing":"1","resize_horizontal":"1","element_path":"1","wordcount":"1","image_advtab":"0","advlist":"1","autosave":"1","contextmenu":"1","custom_plugin":"","custom_button":""},"1":{"access":["6","2"],"skin":"0","skin_admin":"0","mobile":"0","drag_drop":"1","path":"","entity_encoding":"raw","lang_mode":"1","text_direction":"ltr","content_css":"1","content_css_custom":"","relative_urls":"1","newlines":"0","use_config_textfilters":"0","invalid_elements":"script,applet,iframe","valid_elements":"","extended_elements":"","resizing":"1","resize_horizontal":"1","element_path":"1","wordcount":"1","image_advtab":"0","advlist":"1","autosave":"1","contextmenu":"1","custom_plugin":"","custom_button":""},"0":{"access":["7","4","8"],"skin":"0","skin_admin":"0","mobile":"0","drag_drop":"1","path":"","entity_encoding":"raw","lang_mode":"1","text_direction":"ltr","content_css":"1","content_css_custom":"","relative_urls":"1","newlines":"0","use_config_textfilters":"0","invalid_elements":"script,applet,iframe","valid_elements":"","extended_elements":"","resizing":"1","resize_horizontal":"1","element_path":"1","wordcount":"1","image_advtab":"1","advlist":"1","autosave":"1","contextmenu":"1","custom_plugin":"","custom_button":""}}},"sets_amount":3,"html_height":"160","html_width":"750"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 0, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_article","type":"plugin","creationDate":"October 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_ARTICLE_XML_DESCRIPTION","group":"","filename":"article"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 0, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_image","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_IMAGE_XML_DESCRIPTION","group":"","filename":"image"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 0, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_pagebreak","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION","group":"","filename":"pagebreak"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 0, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_readmore","type":"plugin","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_READMORE_XML_DESCRIPTION","group":"","filename":"readmore"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 0, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{"name":"plg_search_categories","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION","group":"","filename":"categories"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 0, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{"name":"plg_search_contacts","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTACTS_XML_DESCRIPTION","group":"","filename":"contacts"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 0, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{"name":"plg_search_content","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 0, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{"name":"plg_search_newsfeeds","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 0, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 1, 1, 1, '{"name":"plg_system_languagefilter","type":"plugin","creationDate":"July 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION","group":"","filename":"languagefilter"}', '{"detect_browser":"1","automatic_change":"1","item_associations":"1","alternate_meta":"1","xdefault":"1","xdefault_language":"default","remove_default_prefix":"1","lang_cookie":"0"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 0, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 0, 1, 0, '{"name":"plg_system_p3p","type":"plugin","creationDate":"September 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_P3P_XML_DESCRIPTION","group":"","filename":"p3p"}', '{"headers":"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 0, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{"name":"plg_system_cache","type":"plugin","creationDate":"February 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CACHE_XML_DESCRIPTION","group":"","filename":"cache"}', '{"browsercache":"0","cachetime":"15"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 0, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{"name":"plg_system_debug","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_DEBUG_XML_DESCRIPTION","group":"","filename":"debug"}', '{"profile":"1","queries":"1","memory":"1","language_files":"1","language_strings":"1","strip-first":"1","strip-prefix":"","strip-suffix":""}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 0, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{"name":"plg_system_log","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOG_XML_DESCRIPTION","group":"","filename":"log"}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 0, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{"name":"plg_system_redirect","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION","group":"","filename":"redirect"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 0, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{"name":"plg_system_remember","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_REMEMBER_XML_DESCRIPTION","group":"","filename":"remember"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 0, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{"name":"plg_system_sef","type":"plugin","creationDate":"December 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEF_XML_DESCRIPTION","group":"","filename":"sef"}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 0, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{"name":"plg_system_logout","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION","group":"","filename":"logout"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 0, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{"name":"plg_user_contactcreator","type":"plugin","creationDate":"August 2009","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTACTCREATOR_XML_DESCRIPTION","group":"","filename":"contactcreator"}', '{"autowebpage":"","category":"34","autopublish":"0"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 0, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{"name":"plg_user_joomla","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '{"autoregister":"1","mail_to_user":"1","forceLogout":"1"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 0, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{"name":"plg_user_profile","type":"plugin","creationDate":"January 2008","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_PROFILE_XML_DESCRIPTION","group":"","filename":"profile"}', '{"register-require_address1":"1","register-require_address2":"1","register-require_city":"1","register-require_region":"1","register-require_country":"1","register-require_postal_code":"1","register-require_phone":"1","register-require_website":"1","register-require_favoritebook":"1","register-require_aboutme":"1","register-require_tos":"1","register-require_dob":"1","profile-require_address1":"1","profile-require_address2":"1","profile-require_city":"1","profile-require_region":"1","profile-require_country":"1","profile-require_postal_code":"1","profile-require_phone":"1","profile-require_website":"1","profile-require_favoritebook":"1","profile-require_aboutme":"1","profile-require_tos":"1","profile-require_dob":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 0, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{"name":"plg_extension_joomla","type":"plugin","creationDate":"May 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 0, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{"name":"plg_content_joomla","type":"plugin","creationDate":"November 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 0, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{"name":"plg_system_languagecode","type":"plugin","creationDate":"November 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION","group":"","filename":"languagecode"}', '', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 0, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_joomlaupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION","group":"","filename":"joomlaupdate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 0, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_extensionupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION","group":"","filename":"extensionupdate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 0, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{"name":"plg_captcha_recaptcha","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.0","description":"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION","group":"","filename":"recaptcha"}', '{"public_key":"","private_key":"","theme":"clean"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 0, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{"name":"plg_system_highlight","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION","group":"","filename":"highlight"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 0, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{"name":"plg_content_finder","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_FINDER_XML_DESCRIPTION","group":"","filename":"finder"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 0, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_categories","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CATEGORIES_XML_DESCRIPTION","group":"","filename":"categories"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 0, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_contacts","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTACTS_XML_DESCRIPTION","group":"","filename":"contacts"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 0, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_content","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 0, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_newsfeeds","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(447, 0, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_tags","type":"plugin","creationDate":"February 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 0, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_totp","type":"plugin","creationDate":"August 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION","group":"","filename":"totp"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 0, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{"name":"plg_authentication_cookie","type":"plugin","creationDate":"July 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_COOKIE_XML_DESCRIPTION","group":"","filename":"cookie"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 0, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_yubikey","type":"plugin","creationDate":"September 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION","group":"","filename":"yubikey"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 0, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 1, 1, 0, '{"name":"plg_search_tags","type":"plugin","creationDate":"March 2014","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '{"search_limit":"50","show_tagged_items":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(452, 0, 'plg_system_updatenotification', 'plugin', 'updatenotification', 'system', 0, 1, 1, 0, '{"name":"plg_system_updatenotification","type":"plugin","creationDate":"May 2015","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.5.0","description":"PLG_SYSTEM_UPDATENOTIFICATION_XML_DESCRIPTION","group":"","filename":"updatenotification"}', '{"lastrun":1497910996}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(453, 0, 'plg_editors-xtd_module', 'plugin', 'module', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_module","type":"plugin","creationDate":"October 2015","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.5.0","description":"PLG_MODULE_XML_DESCRIPTION","group":"","filename":"module"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(454, 0, 'plg_system_stats', 'plugin', 'stats', 'system', 0, 1, 1, 0, '{"name":"plg_system_stats","type":"plugin","creationDate":"November 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.5.0","description":"PLG_SYSTEM_STATS_XML_DESCRIPTION","group":"","filename":"stats"}', '{"mode":3,"lastrun":1497463402,"unique_id":"c0c896681978f7ee3ec7a31c7941e81810efa414","interval":12}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(455, 0, 'plg_installer_packageinstaller', 'plugin', 'packageinstaller', 'installer', 0, 1, 1, 1, '{"name":"plg_installer_packageinstaller","type":"plugin","creationDate":"May 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.6.0","description":"PLG_INSTALLER_PACKAGEINSTALLER_PLUGIN_XML_DESCRIPTION","group":"","filename":"packageinstaller"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(456, 0, 'PLG_INSTALLER_FOLDERINSTALLER', 'plugin', 'folderinstaller', 'installer', 0, 1, 1, 1, '{"name":"PLG_INSTALLER_FOLDERINSTALLER","type":"plugin","creationDate":"May 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.6.0","description":"PLG_INSTALLER_FOLDERINSTALLER_PLUGIN_XML_DESCRIPTION","group":"","filename":"folderinstaller"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(457, 0, 'PLG_INSTALLER_URLINSTALLER', 'plugin', 'urlinstaller', 'installer', 0, 1, 1, 1, '{"name":"PLG_INSTALLER_URLINSTALLER","type":"plugin","creationDate":"May 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.6.0","description":"PLG_INSTALLER_URLINSTALLER_PLUGIN_XML_DESCRIPTION","group":"","filename":"urlinstaller"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(458, 0, 'plg_quickicon_phpversioncheck', 'plugin', 'phpversioncheck', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_phpversioncheck","type":"plugin","creationDate":"August 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_QUICKICON_PHPVERSIONCHECK_XML_DESCRIPTION","group":"","filename":"phpversioncheck"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(459, 0, 'plg_editors-xtd_menu', 'plugin', 'menu', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_menu","type":"plugin","creationDate":"August 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_EDITORS-XTD_MENU_XML_DESCRIPTION","group":"","filename":"menu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(460, 0, 'plg_editors-xtd_contact', 'plugin', 'contact', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_contact","type":"plugin","creationDate":"October 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_EDITORS-XTD_CONTACT_XML_DESCRIPTION","group":"","filename":"contact"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(461, 0, 'plg_system_fields', 'plugin', 'fields', 'system', 0, 1, 1, 0, '{"name":"plg_system_fields","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_SYSTEM_FIELDS_XML_DESCRIPTION","group":"","filename":"fields"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(462, 0, 'plg_fields_calendar', 'plugin', 'calendar', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_calendar","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_CALENDAR_XML_DESCRIPTION","group":"","filename":"calendar"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(463, 0, 'plg_fields_checkboxes', 'plugin', 'checkboxes', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_checkboxes","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_CHECKBOXES_XML_DESCRIPTION","group":"","filename":"checkboxes"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(464, 0, 'plg_fields_color', 'plugin', 'color', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_color","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_COLOR_XML_DESCRIPTION","group":"","filename":"color"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(465, 0, 'plg_fields_editor', 'plugin', 'editor', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_editor","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_EDITOR_XML_DESCRIPTION","group":"","filename":"editor"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(466, 0, 'plg_fields_imagelist', 'plugin', 'imagelist', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_imagelist","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_IMAGELIST_XML_DESCRIPTION","group":"","filename":"imagelist"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(467, 0, 'plg_fields_integer', 'plugin', 'integer', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_integer","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_INTEGER_XML_DESCRIPTION","group":"","filename":"integer"}', '{"multiple":"0","first":"1","last":"100","step":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(468, 0, 'plg_fields_list', 'plugin', 'list', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_list","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_LIST_XML_DESCRIPTION","group":"","filename":"list"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(469, 0, 'plg_fields_media', 'plugin', 'media', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_media","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_MEDIA_XML_DESCRIPTION","group":"","filename":"media"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(470, 0, 'plg_fields_radio', 'plugin', 'radio', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_radio","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_RADIO_XML_DESCRIPTION","group":"","filename":"radio"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(471, 0, 'plg_fields_sql', 'plugin', 'sql', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_sql","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_SQL_XML_DESCRIPTION","group":"","filename":"sql"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(472, 0, 'plg_fields_text', 'plugin', 'text', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_text","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_TEXT_XML_DESCRIPTION","group":"","filename":"text"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(473, 0, 'plg_fields_textarea', 'plugin', 'textarea', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_textarea","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_TEXTAREA_XML_DESCRIPTION","group":"","filename":"textarea"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(474, 0, 'plg_fields_url', 'plugin', 'url', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_url","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_URL_XML_DESCRIPTION","group":"","filename":"url"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(475, 0, 'plg_fields_user', 'plugin', 'user', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_user","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_USER_XML_DESCRIPTION","group":"","filename":"user"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(476, 0, 'plg_fields_usergrouplist', 'plugin', 'usergrouplist', 'fields', 0, 1, 1, 0, '{"name":"plg_fields_usergrouplist","type":"plugin","creationDate":"March 2016","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_FIELDS_USERGROUPLIST_XML_DESCRIPTION","group":"","filename":"usergrouplist"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(477, 0, 'plg_content_fields', 'plugin', 'fields', 'content', 0, 1, 1, 0, '{"name":"plg_content_fields","type":"plugin","creationDate":"February 2017","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_CONTENT_FIELDS_XML_DESCRIPTION","group":"","filename":"fields"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(478, 0, 'plg_editors-xtd_fields', 'plugin', 'fields', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_fields","type":"plugin","creationDate":"February 2017","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.0","description":"PLG_EDITORS-XTD_FIELDS_XML_DESCRIPTION","group":"","filename":"fields"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 0, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{"name":"hathor","type":"template","creationDate":"May 2010","author":"Andrea Tarr","copyright":"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"3.0.0","description":"TPL_HATHOR_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"showSiteName":"0","colourChoice":"0","boldText":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 0, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{"name":"isis","type":"template","creationDate":"3\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_ISIS_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"templateColor":"","logoFile":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 802, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{"name":"English (en-GB)","type":"language","creationDate":"May 2017","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.2","description":"en-GB site language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 802, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{"name":"English (en-GB)","type":"language","creationDate":"May 2017","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.2","description":"en-GB administrator language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 0, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{"name":"files_joomla","type":"file","creationDate":"May 2017","author":"Joomla! Project","copyright":"(C) 2005 - 2017 Open Source Matters. All rights reserved","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.2","description":"FILES_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(802, 0, 'English (en-GB) Language Pack', 'package', 'pkg_en-GB', '', 0, 1, 1, 1, '{"name":"English (en-GB) Language Pack","type":"package","creationDate":"May 2017","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.7.2.1","description":"en-GB language pack","group":"","filename":"pkg_en-GB"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10000, 10002, 'Russian', 'language', 'ru-RU', '', 0, 1, 0, 0, '{"name":"Russian","type":"language","creationDate":"2017-05-11","author":"Russian Translation Team","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"smart@joomlaportal.ru","authorUrl":"www.joomlaportal.ru","version":"3.7.0.1","description":"Russian language pack (site) for Joomla! 3.7.0","group":"","filename":"install"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10001, 10002, 'ru-RU', 'language', 'ru-RU', '', 1, 1, 0, 0, '{"name":"\\u0420\\u0443\\u0441\\u0441\\u043a\\u0438\\u0439 (ru-RU)","type":"language","creationDate":"2017-05-11","author":"Russian Translation Team","copyright":"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.","authorEmail":"smart@joomlaportal.ru","authorUrl":"www.joomlaportal.ru","version":"3.7.0.1","description":"Russian language pack (administrator) for Joomla! 3.7.0","group":"","filename":"install"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10002, 0, 'Russian (ru-RU) Language Pack', 'package', 'pkg_ru-RU', '', 0, 1, 1, 0, '{"name":"Russian (ru-RU) Language Pack","type":"package","creationDate":"2017-05-11","author":"Russian Translation Team","copyright":"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.","authorEmail":"smart@joomlaportal.ru","authorUrl":"www.joomlaportal.ru","version":"3.7.0.1","description":"Joomla 3.7 Russian Language Package","group":"","filename":"pkg_ru-RU"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10003, 10005, 'GermanDE', 'language', 'de-DE', '', 0, 1, 0, 0, '{"name":"German (DE)","type":"language","creationDate":"24.05.2017","author":"J!German","copyright":"Copyright (C) 2008 - 2017 J!German. All rights reserved.","authorEmail":"team@jgerman.de","authorUrl":"http:\\/\\/www.jgerman.de","version":"3.7.2.1","description":"\\n\\t  <img style=\\"margin: 5px; vertical-align: middle;\\" alt=\\"German (Deutsch)\\" src=\\"data:;base64,R0lGODlhEgAMAJEAAP\\/OAAAAAN0AAAAAACH5BAAAAAAALAAAAAASAAwAAAIXjI+py+2vhJy02ouz3hb4D4biSJbmKRYAOw==\\" height=\\"12\\" width=\\"18\\" \\/>Deutsche Frontend (Website)-\\u00dcbersetzung f\\u00fcr Joomla! 3.7.2\\n\\t  <br \\/>\\n\\t  <img style=\\"margin: 5px; vertical-align: middle;\\" alt=\\"English (Englisch)\\" src=\\"data:image\\/gif;base64,R0lGODlhEgAMAPcAANQYLe+nr+iPmgElcdQuQtQtQtq\\/zc8UK88TKu2Sm+A4SOucpn2RvxIseCBLmRIpdtIWLAEkctAUK\\/\\/f3g4tguqXodozRcwDHNa8y8fT5h9GlP\\/7+R82fcwIIPOCiRg2fwc0fP\\/6+AEohAwqgffV2QYuhfaTmQApgi1VngAZd9Y0SOmTnaysytIjOPixtbZlgOxVYehUYPbP09FqfWByq\\/XL0BIndO2Fju6AieZ8iQAaed9gcOnm7t28wgEpdImUt2B\\/uOtWYsAPHP\\/o5t5SYdzs98pwd\\/KXn\\/\\/v7tjo9WRyqXBtkgEdbPbu8c0MJHdomvB4gHBglMwGH7Nphm6Zy9Pq6uufqfjh5NUwRM8SKhIqd9c5TNc4TNUxRRIjcxAvg9FpfPCmpiBOjv\\/r6cYgKhIfb\\/\\/i4fSTmdm+zClSnOiMl+dXY1RioK5kgxg5hPOZoNMpPmh\\/tTxalmqFut\\/G0tchLdni765RcOiOmQAgfcHZ7v77+3J4o+6UnfTKz\\/\\/\\/\\/OurtYScx66wzThepMy7vwAeeiJLmumQmv\\/m5QAceN00RmOBqgEnc9zr9+lWY+qWoNY0Rw80eudUYWZ1qytZk+unsAYxiup5g+iSnX6Ww7Vif9EQH\\/Df5dbc6hIqdt3w+\\/\\/q6MwFHfOLkuj6\\/+ylrgAVde+aotPQ3+yMls8VLNbc69+lo+6nr9tHWAAPcLTI480GHssAGf\\/\\/\\/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAASAAwAAAjoAH9wKPOggZYPPepsCiPHRgNPXtzwGVKEwZdShUoYAAArAIpEKSwp0RTDERREjRiMyIOGYwAHIia9kORhApspRC6NsZOJDgRYlQK1WYODxKc5gjJcYeUnxB8ZCKRYQeKihqw9p1goUNRlC6QCBOAcyNICCxcVBApYUBCrrdtYFw6k6vDW7RsBAlYsqJAgBwInO\\/ocwvNoAaYjQPTIkmXKBA9OEkIBGiVrg5oEqqi8aoIqyIwoGjBwJDWIRiczN1rdOQMDzBNDOk5s7JjGFYU4SUCJMrJETIQBPkAQIiNkFaUBjJhEWlQlIAA7\\" height=\\"12\\" width=\\"18\\" \\/>German Frontend (Website) translation for Joomla! 3.7.2\\n\\t","group":"","filename":"install"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 10005, 'GermanDE', 'language', 'de-DE', '', 1, 1, 0, 0, '{"name":"German (DE)","type":"language","creationDate":"24.05.2017","author":"J!German","copyright":"Copyright (C) 2008 - 2017 J!German. All rights reserved.","authorEmail":"team@jgerman.de","authorUrl":"http:\\/\\/www.jgerman.de","version":"3.7.2.1","description":"\\n      <img style=\\"margin: 5px; vertical-align: middle;\\" alt=\\"German (Deutsch)\\" src=\\"data:;base64,R0lGODlhEgAMAJEAAP\\/OAAAAAN0AAAAAACH5BAAAAAAALAAAAAASAAwAAAIXjI+py+2vhJy02ouz3hb4D4biSJbmKRYAOw==\\" height=\\"12\\" width=\\"18\\" \\/>Deutsche Backend (Administrator)-\\u00dcbersetzung f\\u00fcr Joomla! 3.7.2\\n      <br \\/>\\n      <img style=\\"margin: 5px; vertical-align: middle;\\" alt=\\"English (Englisch)\\" src=\\"data:image\\/gif;base64,R0lGODlhEgAMAPcAANQYLe+nr+iPmgElcdQuQtQtQtq\\/zc8UK88TKu2Sm+A4SOucpn2RvxIseCBLmRIpdtIWLAEkctAUK\\/\\/f3g4tguqXodozRcwDHNa8y8fT5h9GlP\\/7+R82fcwIIPOCiRg2fwc0fP\\/6+AEohAwqgffV2QYuhfaTmQApgi1VngAZd9Y0SOmTnaysytIjOPixtbZlgOxVYehUYPbP09FqfWByq\\/XL0BIndO2Fju6AieZ8iQAaed9gcOnm7t28wgEpdImUt2B\\/uOtWYsAPHP\\/o5t5SYdzs98pwd\\/KXn\\/\\/v7tjo9WRyqXBtkgEdbPbu8c0MJHdomvB4gHBglMwGH7Nphm6Zy9Pq6uufqfjh5NUwRM8SKhIqd9c5TNc4TNUxRRIjcxAvg9FpfPCmpiBOjv\\/r6cYgKhIfb\\/\\/i4fSTmdm+zClSnOiMl+dXY1RioK5kgxg5hPOZoNMpPmh\\/tTxalmqFut\\/G0tchLdni765RcOiOmQAgfcHZ7v77+3J4o+6UnfTKz\\/\\/\\/\\/OurtYScx66wzThepMy7vwAeeiJLmumQmv\\/m5QAceN00RmOBqgEnc9zr9+lWY+qWoNY0Rw80eudUYWZ1qytZk+unsAYxiup5g+iSnX6Ww7Vif9EQH\\/Df5dbc6hIqdt3w+\\/\\/q6MwFHfOLkuj6\\/+ylrgAVde+aotPQ3+yMls8VLNbc69+lo+6nr9tHWAAPcLTI480GHssAGf\\/\\/\\/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAASAAwAAAjoAH9wKPOggZYPPepsCiPHRgNPXtzwGVKEwZdShUoYAAArAIpEKSwp0RTDERREjRiMyIOGYwAHIia9kORhApspRC6NsZOJDgRYlQK1WYODxKc5gjJcYeUnxB8ZCKRYQeKihqw9p1goUNRlC6QCBOAcyNICCxcVBApYUBCrrdtYFw6k6vDW7RsBAlYsqJAgBwInO\\/ocwvNoAaYjQPTIkmXKBA9OEkIBGiVrg5oEqqi8aoIqyIwoGjBwJDWIRiczN1rdOQMDzBNDOk5s7JjGFYU4SUCJMrJETIQBPkAQIiNkFaUBjJhEWlQlIAA7\\" height=\\"12\\" width=\\"18\\" \\/>German Backend (Administrator) translation for Joomla! 3.7.2\\n    ","group":"","filename":"install"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10005, 0, 'German (Germany) Language Pack', 'package', 'pkg_de-DE', '', 0, 1, 1, 0, '{"name":"German (Germany) Language Pack","type":"package","creationDate":"24.05.2017","author":"J!German","copyright":"","authorEmail":"team@jgerman.de","authorUrl":"http:\\/\\/www.jgerman.de","version":"3.7.2.1","description":"\\n    <div style=\\"text-align: center;\\">\\n      <h2>Deutsches \\u201eFull\\u201c-Sprachpaket f\\u00fcr Joomla! 3.7.2 von <a title=\\"J!German\\" href=\\"http:\\/\\/www.jgerman.de\\" target=\\"_blank\\">J!German<\\/a><\\/h2>\\n      <h3><span style=\\"color: #008000;\\">\\u00dcbersetzungsversion: 3.7.2v1<\\/span><\\/h3>\\n      <hr \\/>\\n      <table rules=\\"all\\" frame=\\"border\\" style=\\"width: 90%; border-color: #000000; border-width: 1px; border-style: solid;\\" align=\\"center\\" border=\\"1\\">\\n      <colgroup> <col width=\\"30%\\" \\/> <col width=\\"60\\" \\/> <\\/colgroup>\\n      <tbody>\\n        <tr>\\n          <td>\\n            <ul>\\n              <li>Frontend (Website)-\\u00dcbersetzung<\\/li>\\n            <\\/ul>\\n          <\\/td>\\n          <td rowspan=\\"2\\">\\n            <ul>\\n              <li>\\n                <span style=\\"text-decoration: underline;\\">Neuinstallation:<\\/span>\\n                <br \\/>\\n                Legen Sie die deutsche Sprache unter <a title=\\"Language(s)\\" href=\\"index.php?option=com_languages\\" target=\\"_blank\\">\\u201eExtensions\\u201c \\u2192 \\u201eLanguage(s)\\u201c<\\/a> als Standardsprache (\\u201eDefault\\u201c), sowohl f\\u00fcr die Website (\\u201eInstalled - Site\\u201c) als auch f\\u00fcr die Administration (\\u201eInstalled - Administrator\\u201c), fest.\\n              <\\/li>\\n              <br \\/>\\n              <li>\\n                <span style=\\"text-decoration: underline;\\">Aktualisierung:<\\/span>\\n                <br \\/>\\n                Es sind keine weiteren Schritte erforderlich.\\n              <\\/li>\\n            <\\/ul>\\n          <\\/td>\\n        <\\/tr>\\n        <tr>\\n          <td>\\n            <ul>\\n              <li>Backend (Administrator)-\\u00dcbersetzung<\\/li>\\n            <\\/ul>\\n          <\\/td>\\n        <\\/tr>\\n      <\\/tbody>\\n      <\\/table>\\n      <br \\/>\\n      <span style=\\"text-decoration: underline;\\">Hinweis:<\\/span> Dieses Paket unterst\\u00fctzt die Joomla! eigene <a title=\\"Joomla!-Aktualisierungsfunktion\\" href=\\"index.php?option=com_installer&amp;view=update\\" target=\\"_blank\\">Aktualisierungsfunktion<\\/a>!\\n    <\\/div>\\n    ","group":"","filename":"pkg_de-DE"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10006, 0, 'templxemlab', 'template', 'xemlab', '', 0, 1, 1, 0, '{"name":"templxemlab","type":"template","creationDate":"2016-08-25","author":"Sonet","copyright":"\\u00a9 2016 Pixel Cook Laboratory. All Rights Reserved.","authorEmail":"addresok@gmail.com","authorUrl":"http:\\/\\/xemlab.com","version":"1.0.0","description":"\\n        \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0456 \\u043d\\u0430\\u043b\\u0430\\u0448\\u0442\\u0443\\u0432\\u0430\\u043d\\u043d\\u044f\\n        <link href=\\"\\/plugins\\/system\\/ns_fontawesome\\/fontawesome\\/css\\/font-awesome.css\\" rel=\\"stylesheet\\" type=\\"text\\/css\\" \\/>\\n        <style>\\n        .chzn-single span {\\n\\n            display: inline-block;\\n    font: normal normal normal 14px\\/1 FontAwesome;\\n    font-size: inherit;\\n    text-rendering: auto;\\n    -webkit-font-smoothing: antialiased;\\n    -moz-osx-font-smoothing: grayscale;\\n        font-size: 16px;\\n    line-height: 22px;\\n        }\\n        <\\/style>\\n        ","group":"","filename":"templateDetails"}', '{"logoFile":"images\\/logo.png","second_bg":"images\\/bg\\/bg_header.png","slogan":"\\u0412\\u0438\\u0440\\u043e\\u0431\\u043d\\u0438\\u0446\\u0442\\u0432\\u043e \\u0456 \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436 | \\u0442\\u0440\\u043e\\u0442\\u0443\\u0430\\u0440\\u043d\\u043e\\u0457 \\u043f\\u043b\\u0438\\u0442\\u043a\\u0438 \\u0432 \\u0427\\u0435\\u0440\\u043d\\u0456\\u0432\\u0446\\u044f\\u0445","btnHome":"","numbers":"","callme":"\\u0412\\u0430\\u043c \\u043f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u044c?","email":"info@btm.cv.ua","logoFooter":"images\\/logo-footer.png","footertitle":"\\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\u0421\\u0438\\u043d\\u0442\\u0435\\u0437 \\u0411\\u0440\\u0443\\u043a"}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `lend_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(10007, 0, 'Flexheader3', 'module', 'mod_flexheader3', '', 0, 1, 0, 0, '{"name":"Flexheader3","type":"module","creationDate":"08.03.2017","author":"Andre Hotzler","copyright":"Copyright (C) 2008, 2009, 2010, 2011, 2012, 2014, 2015, 2016, 2017 Andre Hotzler. All rights reserved.","authorEmail":"flex_header_nospam@andrehotzler.de","authorUrl":"http:\\/\\/www.andrehotzler.de","version":"2.0.3","description":"\\n\\t\\t<link rel=\\"stylesheet\\" type=\\"text\\/css\\" href=\\"..\\/modules\\/mod_flexheader3\\/flexheader3_backend.css\\" \\/>\\n\\t\\t<a href=\\"https:\\/\\/flexheader.andrehotzler.de\\/\\" target=\\"_blank\\"><img class=\\"flimage\\" src=\\"..\\/modules\\/mod_flexheader3\\/flexheader3_large.png\\" alt=\\"Flexheader3\\"><\\/a>\\n<div id=\\"docucon\\">\\n\\t<div class=\\"denglish\\">\\n\\t\\t<h2>English (in german below on small screens)<\\/h2>\\n\\t\\t<p>Flexheader3 expands the existing possibilities to show pictures or load \\n\\t\\tcss files for flexible styling of your website. Flexheader3 makes it possible \\n\\t\\tto assign an image or css-file at any point in your website, it does not \\n\\t\\tdepend on menu entries.<\\/p>\\n\\t\\t<p><strong>Please turn off the parameter \\"Show Help information on Frontpage\\"<\\/strong>\\n\\t\\t<\\/p>\\n\\t\\t<p>Would you like to <strong>support<\\/strong> the development of Flexheader \\n\\t\\t?<\\/p>\\n\\t\\t<p>Then I would be delighted if you take a look at my services:<\\/p>\\n\\t\\t<p><a href=\\"http:\\/\\/www.andrehotzler.de\\/h\\" target=\\"_blank\\"><strong>fast and \\n\\t\\tgood webspace for Joomla!<\\/strong> <\\/a><\\/p>\\n\\t\\t<ul>\\n\\t\\t\\t<li>guarantees propper execution with the operation of Joomla!<\\/li>\\n\\t\\t\\t<li>servers running in germany<\\/li>\\n\\t\\t\\t<li>includes the installation of joomla!-core-updates<\\/li>\\n\\t\\t<\\/ul>\\n\\t\\t<p>You can also order\\n\\t\\t<a href=\\"http:\\/\\/www.andrehotzler.de\\/kontakt.html\\" target=\\"_blank\\">professional \\n\\t\\tJoomla!-Support<\\/a>.<\\/p>\\n\\t\\t<h3>Online-Documentation<\\/h3>\\n\\t\\t<iframe src=\\"https:\\/\\/flexheader.andrehotzler.de\\/en\\/documentation.html\\"><\\/iframe>\\n\\t<\\/div>\\n\\t<div class=\\"dgerman\\">\\n\\t\\t<h2>Deutsch<\\/h2>\\n\\t\\t<p>Flexheader3 bringt f\\u00fcr Joomla! die F\\u00e4higkeit, Bilder oder eine CSS-Dateien \\n\\t\\tbeliebigen Stellen innerhalb einer Webseite zu zu weisen. Die Abh\\u00e4ngigkeit \\n\\t\\tvon Men\\u00fcpunkten entf\\u00e4llt komplett.<\\/p>\\n\\t\\t<p><strong>Bitte schalten Sie das Fenster \\"Zeige Hilfe-Informationen auf \\n\\t\\tder Startseite an\\" nach der Einrichtung in den Modul-Einstellungen ab.<\\/strong><\\/p>\\n\\t\\t<p>M\\u00f6chten Sie die Entwicklung von Flexheader <strong>unterst\\u00fctzen<\\/strong>?<\\/p>\\n\\t\\t<p>Dann w\\u00fcrde ich mich freuen, wenn Sie einen Blick auf meine Dienstleistungen \\n\\t\\twerfen:<\\/p>\\n\\t\\t<p><a href=\\"http:\\/\\/www.andrehotzler.de\\/h\\"><strong>S<\\/strong><\\/a><a href=\\"http:\\/\\/www.andrehotzler.de\\/h\\" target=\\"_blank\\"><strong>chnell \\n\\t\\tund sauber laufender Webspace f\\u00fcr Joomla! <\\/strong><\\/a><\\/p>\\n\\t\\t<ul>\\n\\t\\t\\t<li>garantiert keine Probleme beim Betrieb von Joomla!<\\/li>\\n\\t\\t\\t<li>Gehostet in Deutschland<\\/li>\\n\\t\\t\\t<li>Bei der Bundesnetzagentur gelisteter Anbieter von Telekommunikationsdienstleistungen<\\/li>\\n\\t\\t\\t<li>Webhosting-Preis beinhaltet die Installation von Joomla!-Core-Updates<\\/li>\\n\\t\\t<\\/ul>\\n\\t\\t<p>Au\\u00dferdem k\\u00f6nnen Sie\\n\\t\\t<a href=\\"http:\\/\\/www.andrehotzler.de\\/kontakt.html\\" target=\\"_blank\\">professionellen \\n\\t\\tJoomla!-Support<\\/a> und die Erstellung von\\n\\t\\t<a href=\\"http:\\/\\/www.feinste-webseiten.de\\/\\" target=\\"_blank\\">handgeschriebenen \\n\\t\\tJoomla!-Templates<\\/a> buchen.<\\/p>\\n\\t\\t<h3>Online-Dokumentation<\\/h3>\\n\\t\\t<iframe src=\\"https:\\/\\/flexheader.andrehotzler.de\\/de\\/dokumentation.html\\"><\\/iframe>\\n\\t<\\/div>\\n\\t<div class=\\"clear\\">&nbsp;<\\/div>\\n<\\/div>\\n\\n\\t","group":"","filename":"mod_flexheader3"}', '{"cversion":"2.0.3","debug":"normal","showebugtoguest":"1","folder_location":"images","folder_name":"header-images","default_image":"header-default","file_type":"jpg","specifydimensions":"1","width":"600","height":"150","dimensionstype":"px","display_type":"img","div_content":"","alt_text_type":"none","alt_text":"flexheader3","linksupport":"0","linktarget":"\\/","jfsupport":"0","cssfilesupport":"0","debug_position":"bottom","debug_bg_color":"#3f6498","debug_opac":"90","moduleclass_sfx":" flexheader","css_class":"flexheader","cache":"0","cache_time":"900"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10008, 0, 'Slideshow CK', 'module', 'mod_slideshowck', '', 0, 1, 0, 0, '{"name":"Slideshow CK","type":"module","creationDate":"Avril 2012","author":"C\\u00e9dric KEIFLIN","copyright":"C\\u00e9dric KEIFLIN","authorEmail":"ced1870@gmail.com","authorUrl":"http:\\/\\/www.joomlack.fr","version":"1.4.43","description":"MOD_SLIDESHOWCK_XML_DESCRIPTION","group":"","filename":"mod_slideshowck"}', '{"slidesssource":"slidesmanager","slides":"[{|qq|imgname|qq|:|qq|modules\\/mod_slideshowck\\/images\\/slides\\/bridge.jpg|qq|,|qq|imgcaption|qq|:|qq|This bridge is very long|qq|,|qq|imgtitle|qq|:|qq|This is a bridge|qq|,|qq|imgthumb|qq|:|qq|..\\/modules\\/mod_slideshowck\\/images\\/slides\\/bridge.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|_parent|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|modules\\/mod_slideshowck\\/images\\/slides\\/road.jpg|qq|,|qq|imgcaption|qq|:|qq|This slideshow uses a JQuery script adapted from <a href=|dq|http:\\/\\/www.pixedelic.com\\/plugins\\/camera\\/|dq|>Pixedelic<\\/a>|qq|,|qq|imgtitle|qq|:|qq|On the road again|qq|,|qq|imgthumb|qq|:|qq|..\\/modules\\/mod_slideshowck\\/images\\/slides\\/road.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|_parent|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|modules\\/mod_slideshowck\\/images\\/slides\\/big_bunny_fake.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|..\\/modules\\/mod_slideshowck\\/images\\/slides\\/big_bunny_fake.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|_parent|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq|http:\\/\\/player.vimeo.com\\/video\\/2203727|qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|}]","skin":"camera_amber_skin","alignment":"center","loader":"pie","width":"auto","height":"62%","minheight":"150","navigation":"2","thumbnails":"1","thumbnailwidth":"100","thumbnailheight":"75","pagination":"1","effect":"random","time":"7000","transperiod":"1500","captioneffect":"random","portrait":"0","autoAdvance":"1","hover":"1","displayorder":"normal","limitslides":"","fullpage":"0","imagetarget":"_parent","linkposition":"fullslide","container":"","usemobileimage":"0","mobileimageresolution":"640","loadjquery":"1","loadjqueryeasing":"1","autocreatethumbs":"1","fixhtml":"0","cache":"1","cache_time":"900","cachemode":"itemid","articlelength":"150","articlelink":"readmore","articletitle":"h3","showarticletitle":"1","usecaption":"1","usecaptiondesc":"1","usecaptionresponsive":"1","captionresponsiveresolution":"480","captionresponsivefontsize":"0.6em","captionresponsivehidecaption":"0","captionstylesusefont":"1","captionstylestextgfont":"Droid Sans","captionstylesfontsize":"1.1em","captionstylesfontcolor":"","captionstylesfontweight":"normal","captionstylesdescfontsize":"0.8em","captionstylesdescfontcolor":"","captionstylesusemargin":"1","captionstylesmargintop":"0","captionstylesmarginright":"0","captionstylesmarginbottom":"0","captionstylesmarginleft":"0","captionstylespaddingtop":"0","captionstylespaddingright":"0","captionstylespaddingbottom":"0","captionstylespaddingleft":"0","captionstylesusebackground":"1","captionstylesbgcolor1":"","captionstylesbgopacity":"0.6","captionstylesbgpositionx":"left","captionstylesbgpositiony":"top","captionstylesbgimagerepeat":"repeat","captionstylesusegradient":"1","captionstylesbgcolor2":"","captionstylesuseroundedcorners":"1","captionstylesroundedcornerstl":"5","captionstylesroundedcornerstr":"5","captionstylesroundedcornersbr":"5","captionstylesroundedcornersbl":"5","captionstylesuseshadow":"1","captionstylesshadowcolor":"","captionstylesshadowblur":"3","captionstylesshadowspread":"0","captionstylesshadowoffsetx":"0","captionstylesshadowoffsety":"0","captionstylesshadowinset":"0","captionstylesuseborders":"1","captionstylesbordercolor":"","captionstylesborderwidth":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10009, 0, 'Slideshow CK', 'module', 'mod_slides', '', 0, 1, 1, 0, '{"name":"Slideshow CK","type":"module","creationDate":"Avril 2012","author":"C\\u00e9dric KEIFLIN","copyright":"C\\u00e9dric KEIFLIN","authorEmail":"ced1870@gmail.com","authorUrl":"http:\\/\\/www.joomlack.fr","version":"1.4.43","description":"MOD_SLIDESHOWCK_XML_DESCRIPTION","group":"","filename":"mod_slides"}', '{"slidesssource":"slidesmanager","slides":"[{|qq|imgname|qq|:|qq|modules\\/mod_slideshowck\\/images\\/slides\\/bridge.jpg|qq|,|qq|imgcaption|qq|:|qq|This bridge is very long|qq|,|qq|imgtitle|qq|:|qq|This is a bridge|qq|,|qq|imgthumb|qq|:|qq|..\\/modules\\/mod_slideshowck\\/images\\/slides\\/bridge.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|_parent|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|modules\\/mod_slideshowck\\/images\\/slides\\/road.jpg|qq|,|qq|imgcaption|qq|:|qq|This slideshow uses a JQuery script adapted from <a href=|dq|http:\\/\\/www.pixedelic.com\\/plugins\\/camera\\/|dq|>Pixedelic<\\/a>|qq|,|qq|imgtitle|qq|:|qq|On the road again|qq|,|qq|imgthumb|qq|:|qq|..\\/modules\\/mod_slideshowck\\/images\\/slides\\/road.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|_parent|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|modules\\/mod_slideshowck\\/images\\/slides\\/big_bunny_fake.jpg|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|..\\/modules\\/mod_slideshowck\\/images\\/slides\\/big_bunny_fake.jpg|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|_parent|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq|http:\\/\\/player.vimeo.com\\/video\\/2203727|qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|}]","skin":"camera_amber_skin","alignment":"center","loader":"pie","width":"auto","height":"62%","minheight":"150","navigation":"2","thumbnails":"1","thumbnailwidth":"100","thumbnailheight":"75","pagination":"1","effect":"random","time":"7000","transperiod":"1500","captioneffect":"random","portrait":"0","autoAdvance":"1","hover":"1","displayorder":"normal","limitslides":"","fullpage":"0","imagetarget":"_parent","linkposition":"fullslide","container":"","usemobileimage":"0","mobileimageresolution":"640","loadjquery":"1","loadjqueryeasing":"1","autocreatethumbs":"1","fixhtml":"0","cache":"1","cache_time":"900","cachemode":"itemid","articlelength":"150","articlelink":"readmore","articletitle":"h3","showarticletitle":"1","usecaption":"1","usecaptiondesc":"1","usecaptionresponsive":"1","captionresponsiveresolution":"480","captionresponsivefontsize":"0.6em","captionresponsivehidecaption":"0","captionstylesusefont":"1","captionstylestextgfont":"Droid Sans","captionstylesfontsize":"1.1em","captionstylesfontcolor":"","captionstylesfontweight":"normal","captionstylesdescfontsize":"0.8em","captionstylesdescfontcolor":"","captionstylesusemargin":"1","captionstylesmargintop":"0","captionstylesmarginright":"0","captionstylesmarginbottom":"0","captionstylesmarginleft":"0","captionstylespaddingtop":"0","captionstylespaddingright":"0","captionstylespaddingbottom":"0","captionstylespaddingleft":"0","captionstylesusebackground":"1","captionstylesbgcolor1":"","captionstylesbgopacity":"0.6","captionstylesbgpositionx":"left","captionstylesbgpositiony":"top","captionstylesbgimagerepeat":"repeat","captionstylesusegradient":"1","captionstylesbgcolor2":"","captionstylesuseroundedcorners":"1","captionstylesroundedcornerstl":"5","captionstylesroundedcornerstr":"5","captionstylesroundedcornersbr":"5","captionstylesroundedcornersbl":"5","captionstylesuseshadow":"1","captionstylesshadowcolor":"","captionstylesshadowblur":"3","captionstylesshadowspread":"0","captionstylesshadowoffsetx":"0","captionstylesshadowoffsety":"0","captionstylesshadowinset":"0","captionstylesuseborders":"1","captionstylesbordercolor":"","captionstylesborderwidth":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10010, 0, 'mod_prodact', 'module', 'mod_prodact', '', 0, 1, 1, 0, '{"name":"mod_prodact","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"\\u0413\\u0430\\u043b\\u0435\\u0440\\u0435\\u044f (\\u0432\\u0438\\u0432\\u043e\\u0434\\u0438\\u043c \\u043d\\u0430 \\u0435\\u043a\\u0440\\u0430\\u043d)","group":"","filename":"mod_prodact"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_fields`
--

CREATE TABLE `lend_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `default_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldparams` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_fields_categories`
--

CREATE TABLE `lend_fields_categories` (
  `field_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_fields_groups`
--

CREATE TABLE `lend_fields_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_fields_values`
--

CREATE TABLE `lend_fields_values` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Allow references to items which have strings as ids, eg. none db systems.',
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_filters`
--

CREATE TABLE `lend_finder_filters` (
  `filter_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links`
--

CREATE TABLE `lend_finder_links` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(400) DEFAULT NULL,
  `description` text,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double UNSIGNED NOT NULL DEFAULT '0',
  `sale_price` double UNSIGNED NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_terms0`
--

CREATE TABLE `lend_finder_links_terms0` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_terms1`
--

CREATE TABLE `lend_finder_links_terms1` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_terms2`
--

CREATE TABLE `lend_finder_links_terms2` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_terms3`
--

CREATE TABLE `lend_finder_links_terms3` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_terms4`
--

CREATE TABLE `lend_finder_links_terms4` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_terms5`
--

CREATE TABLE `lend_finder_links_terms5` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_terms6`
--

CREATE TABLE `lend_finder_links_terms6` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_terms7`
--

CREATE TABLE `lend_finder_links_terms7` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_terms8`
--

CREATE TABLE `lend_finder_links_terms8` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_terms9`
--

CREATE TABLE `lend_finder_links_terms9` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_termsa`
--

CREATE TABLE `lend_finder_links_termsa` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_termsb`
--

CREATE TABLE `lend_finder_links_termsb` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_termsc`
--

CREATE TABLE `lend_finder_links_termsc` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_termsd`
--

CREATE TABLE `lend_finder_links_termsd` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_termse`
--

CREATE TABLE `lend_finder_links_termse` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_links_termsf`
--

CREATE TABLE `lend_finder_links_termsf` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_taxonomy`
--

CREATE TABLE `lend_finder_taxonomy` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `access` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `lend_finder_taxonomy`
--

INSERT INTO `lend_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_taxonomy_map`
--

CREATE TABLE `lend_finder_taxonomy_map` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_terms`
--

CREATE TABLE `lend_finder_terms` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_terms_common`
--

CREATE TABLE `lend_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `lend_finder_terms_common`
--

INSERT INTO `lend_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('any', 'en'),
('are', 'en'),
('aren\'t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn\'t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_tokens`
--

CREATE TABLE `lend_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '1',
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_tokens_aggregate`
--

CREATE TABLE `lend_finder_tokens_aggregate` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `term_weight` float UNSIGNED NOT NULL,
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `context_weight` float UNSIGNED NOT NULL,
  `total_weight` float UNSIGNED NOT NULL,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_finder_types`
--

CREATE TABLE `lend_finder_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_languages`
--

CREATE TABLE `lend_languages` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lang_code` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_native` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sef` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_languages`
--

INSERT INTO `lend_languages` (`lang_id`, `asset_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 61, 'en-GB', 'EN', 'EN', 'en', 'en_gb', '', '', '', '', 1, 1, 3),
(2, 55, 'ru-RU', 'RU', 'RU', 'ru', 'ru_ru', '', '', '', '', 1, 1, 2),
(3, 56, 'de-DE', 'DE', 'DE', 'de', 'de_de', '', '', '', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_menu`
--

CREATE TABLE `lend_menu` (
  `id` int(11) NOT NULL,
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_menu`
--

INSERT INTO `lend_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 91, 0, '*', 0),
(2, 'main', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 1, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'main', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'main', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 1, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'main', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'main', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'main', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 1, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 11, 16, 0, '*', 1),
(8, 'main', 'com_contact_contacts', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 1, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 12, 13, 0, '*', 1),
(9, 'main', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 1, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 14, 15, 0, '*', 1),
(10, 'main', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 1, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 17, 20, 0, '*', 1),
(11, 'main', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 1, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 18, 19, 0, '*', 1),
(13, 'main', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 21, 26, 0, '*', 1),
(14, 'main', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 22, 23, 0, '*', 1),
(15, 'main', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 1, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 24, 25, 0, '*', 1),
(16, 'main', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 1, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 27, 28, 0, '*', 1),
(17, 'main', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 1, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 29, 30, 0, '*', 1),
(18, 'main', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 1, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 31, 32, 0, '*', 1),
(19, 'main', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 33, 34, 0, '*', 1),
(20, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 1, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 35, 36, 0, '', 1),
(21, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 1, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 37, 38, 0, '*', 1),
(22, 'main', 'com_associations', 'Multilingual Associations', '', 'Multilingual Associations', 'index.php?option=com_associations', 'component', 1, 1, 1, 34, 0, '0000-00-00 00:00:00', 0, 0, 'class:associations', 0, '', 39, 40, 0, '*', 1),
(101, 'mainmenu', 'Home', 'home', '', 'home', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"featured_categories":[""],"layout_type":"blog","num_leading_articles":"1","num_intro_articles":"3","num_columns":"3","num_links":"0","multi_column_order":"1","orderby_pri":"","orderby_sec":"front","order_date":"","show_pagination":"2","show_pagination_results":"1","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","show_feed_link":"1","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":1,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 41, 42, 1, '*', 0),
(102, 'rus', 'Главная', 'home-2', '', 'home-2', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 9, '{"featured_categories":[""],"layout_type":"blog","num_leading_articles":"1","num_intro_articles":"3","num_columns":"3","num_links":"0","multi_column_order":"1","orderby_pri":"","orderby_sec":"front","order_date":"","show_pagination":"2","show_pagination_results":"1","show_title":"","link_titles":"","show_intro":"","info_block_position":"","info_block_show_title":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_associations":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"1","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"no-visible","menu_image":"","menu_text":1,"menu_show":1,"page_title":"","show_page_heading":"1","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 43, 44, 1, 'ru-RU', 0),
(103, 'de', 'Haupt', 'home-de', '', 'home-de', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"featured_categories":[""],"layout_type":"blog","num_leading_articles":"1","num_intro_articles":"3","num_columns":"3","num_links":"0","multi_column_order":"1","orderby_pri":"","orderby_sec":"front","order_date":"","show_pagination":"2","show_pagination_results":"1","show_title":"","link_titles":"","show_intro":"","info_block_position":"","info_block_show_title":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_associations":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"1","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"no-visible","menu_image":"","menu_text":1,"menu_show":1,"page_title":"","show_page_heading":"1","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 45, 46, 1, 'de-DE', 0),
(104, 'eng', 'Home', 'home-en', '', 'home-en', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"featured_categories":[""],"layout_type":"blog","num_leading_articles":"1","num_intro_articles":"3","num_columns":"3","num_links":"0","multi_column_order":"1","orderby_pri":"","orderby_sec":"front","order_date":"","show_pagination":"2","show_pagination_results":"1","show_title":"","link_titles":"","show_intro":"","info_block_position":"","info_block_show_title":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_associations":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"1","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"no-visible","menu_image":"","menu_text":1,"menu_show":1,"page_title":"","show_page_heading":"1","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 47, 48, 1, 'en-GB', 0),
(105, 'rus', 'Для кого', 'homesection-ru', '', 'homesection-ru', '#for-whom', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 49, 50, 0, 'ru-RU', 0),
(106, 'rus', 'Система', 'system-ru', '', 'system-ru', '#system', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 55, 56, 0, 'ru-RU', 0),
(107, 'rus', 'Возможности', 'opportunities-ru', '', 'opportunities-ru', '#opportunities', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 61, 62, 0, 'ru-RU', 0),
(108, 'rus', 'Как это работает', 'how-do-ru', '', 'how-do-ru', '#how-do', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 67, 68, 0, 'ru-RU', 0),
(109, 'rus', 'Стоимость', 'rates-ru', '', 'rates-ru', '#rates', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 73, 74, 0, 'ru-RU', 0),
(110, 'rus', 'О компании', 'about-ru', '', 'about-ru', '#about', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 79, 80, 0, 'ru-RU', 0),
(111, 'rus', 'Контакты', 'conracts-ru', '', 'conracts-ru', '#conracts', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 85, 86, 0, 'ru-RU', 0),
(112, 'eng', 'For whom', 'homesection-en', '', 'homesection-en', '#for-whom', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 53, 54, 0, 'en-GB', 0),
(113, 'eng', 'System', 'system-en', '', 'system-en', '#system', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 59, 60, 0, 'en-GB', 0),
(114, 'eng', 'Capabilities', 'opportunities-en', '', 'opportunities-en', '#opportunities', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 65, 66, 0, 'en-GB', 0),
(115, 'eng', 'How it works', 'how-do-en', '', 'how-do-en', '#how-do', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 71, 72, 0, 'en-GB', 0),
(116, 'eng', 'Cost', 'rates-en', '', 'rates-en', '#rates', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 77, 78, 0, 'en-GB', 0),
(117, 'eng', 'About company', 'about-en', '', 'about-en', '#about', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 83, 84, 0, 'en-GB', 0),
(118, 'eng', 'Contacts', 'conracts-en', '', 'conracts-en', '#conracts', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 89, 90, 0, 'en-GB', 0),
(119, 'de', 'Für wen', 'homesection-de', '', 'homesection-de', '#for-whom', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 51, 52, 0, 'de-DE', 0),
(120, 'de', 'System', 'system-de', '', 'system-de', '#system', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 57, 58, 0, 'de-DE', 0),
(121, 'de', 'Chancen', 'opportunities-de', '', 'opportunities-de', '#opportunities', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 63, 64, 0, 'de-DE', 0),
(122, 'de', 'Wie funktioniert das', 'how-do-de', '', 'how-do-de', '#how-do', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 69, 70, 0, 'de-DE', 0),
(123, 'de', 'Kosten', 'rates-de', '', 'rates-de', '#rates', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 75, 76, 0, 'de-DE', 0),
(124, 'de', 'Über die Firma', 'about-de', '', 'about-de', '#about', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 81, 82, 0, 'de-DE', 0),
(125, 'de', 'Kontakte', 'conracts-de', '', 'conracts-de', '#conracts', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu-anchor_rel":"","menu_image":"","menu_text":1,"menu_show":1}', 87, 88, 0, 'de-DE', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_menu_types`
--

CREATE TABLE `lend_menu_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_menu_types`
--

INSERT INTO `lend_menu_types` (`id`, `asset_id`, `menutype`, `title`, `description`, `client_id`) VALUES
(1, 0, 'mainmenu', 'Main Menu', 'The main menu for the site', 0),
(2, 57, 'rus', 'Рус ', '', 0),
(3, 58, 'eng', 'Eng', '', 0),
(4, 59, 'de', 'DE', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_messages`
--

CREATE TABLE `lend_messages` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `user_id_from` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id_to` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_messages_cfg`
--

CREATE TABLE `lend_messages_cfg` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cfg_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_modules`
--

CREATE TABLE `lend_modules` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_modules`
--

INSERT INTO `lend_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 39, 'Main Menu rus', '', '', 1, 'topmenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"rus","base":"","startLevel":"1","endLevel":"0","showAllChildren":"1","tag_id":"mainMenu","class_sfx":" navbar-nav navbar-right","window_open":"","layout":"_:default","moduleclass_sfx":"_menu","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, 'ru-RU'),
(2, 40, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 41, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{"count":"5","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(4, 42, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{"count":"5","ordering":"c_dsc","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(8, 43, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 44, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 45, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{"count":"5","name":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(12, 46, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{"layout":"","moduleclass_sfx":"","shownew":"1","showhelp":"1","cache":"0"}', 1, '*'),
(13, 47, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 48, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 49, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 50, 'Login Form', '', '', 7, 'position-7', 0, '0000-00-00 00:00:00', '2017-06-15 12:46:06', '0000-00-00 00:00:00', -2, 'mod_login', 1, 1, '{"greeting":"1","name":"0"}', 0, '*'),
(17, 51, 'Breadcrumbs', '', '', 1, 'position-2', 0, '0000-00-00 00:00:00', '2017-06-15 12:46:06', '0000-00-00 00:00:00', -2, 'mod_breadcrumbs', 1, 1, '{"moduleclass_sfx":"","showHome":"1","homeText":"","showComponent":"1","separator":"","cache":"0","cache_time":"0","cachemode":"itemid"}', 0, '*'),
(79, 52, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{"layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(86, 53, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{"format":"short","product":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(87, 60, 'Переключение языков ', '', '', 1, 'lang', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_languages', 1, 1, '{"header_text":"","footer_text":"","dropdown":"1","dropdownimage":"1","lineheight":"0","image":"1","show_active":"1","full_name":"1","inline":"1","layout":"_:default","moduleclass_sfx":" modlll","cache":"0","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(88, 62, 'Flexheader3', '', '', 1, 'clients', 0, '0000-00-00 00:00:00', '2017-06-16 22:58:45', '0000-00-00 00:00:00', -2, 'mod_flexheader3', 1, 1, '{"cversion":"2.0.3","debug":"no","showebugtoguest":"1","folder_location":"images","folder_name":"logos","default_image":"1","file_type":"png","specifydimensions":"1","width":"600","height":"150","dimensionstype":"px","display_type":"img","div_content":"","alt_text_type":"none","alt_text":"flexheader3","linksupport":"0","linktarget":"\\/","jfsupport":"0","cssfilesupport":"0","debug_position":"bottom","debug_bg_color":"#ff7777","debug_opac":"90","moduleclass_sfx":" flexheader","css_class":"flexheader","cache":"0","cache_time":"900","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(89, 63, 'Slideshow CK', '', '', 1, 'clients', 0, '0000-00-00 00:00:00', '2017-06-16 22:58:45', '0000-00-00 00:00:00', -2, 'mod_slideshowck', 1, 1, '{"slidesssource":"slidesmanager","slides":"[{|qq|imgname|qq|:|qq|images\\/logos\\/1.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/1.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/2.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/2.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/3.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/3.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/4.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/4.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/6.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/6.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/6.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/6.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/7.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/7.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/8.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/8.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|}]","theme":"default","skin":"camera_amber_skin","alignment":"center","loader":"pie","width":"auto","height":"62%","minheight":"150","navigation":"2","thumbnails":"1","thumbnailwidth":"100","thumbnailheight":"75","pagination":"1","effect":["random"],"time":"7000","transperiod":"1500","captioneffect":"moveFromLeft","portrait":"0","autoAdvance":"1","hover":"1","displayorder":"normal","limitslides":"","fullpage":"0","imagetarget":"_parent","linkposition":"fullslide","container":"","usemobileimage":"0","mobileimageresolution":"640","loadjquery":"0","loadjqueryeasing":"0","autocreatethumbs":"0","fixhtml":"0","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","articlelength":"150","articlelink":"readmore","articletitle":"h3","showarticletitle":"1","usecaption":"1","usecaptiondesc":"1","usecaptionresponsive":"1","captionresponsiveresolution":"480","captionresponsivefontsize":"0.6em","captionresponsivehidecaption":"0","captionstylesusefont":"1","captionstylestextgfont":"Droid Sans","captionstylesfontsize":"1.1em","captionstylesfontcolor":"","captionstylesfontweight":"normal","captionstylesdescfontsize":"0.8em","captionstylesdescfontcolor":"","captionstylesusemargin":"1","captionstylesmargintop":"0","captionstylesmarginright":"0","captionstylesmarginbottom":"0","captionstylesmarginleft":"0","captionstylespaddingtop":"0","captionstylespaddingright":"0","captionstylespaddingbottom":"0","captionstylespaddingleft":"0","captionstylesusebackground":"1","captionstylesbgcolor1":"","captionstylesbgopacity":"0.6","captionstylesbgimage":"","captionstylesbgpositionx":"left","captionstylesbgpositiony":"top","captionstylesbgimagerepeat":"repeat","captionstylesusegradient":"1","captionstylesbgcolor2":"","captionstylesuseroundedcorners":"1","captionstylesroundedcornerstl":"5","captionstylesroundedcornerstr":"5","captionstylesroundedcornersbr":"5","captionstylesroundedcornersbl":"5","captionstylesuseshadow":"1","captionstylesshadowcolor":"","captionstylesshadowblur":"3","captionstylesshadowspread":"0","captionstylesshadowoffsetx":"0","captionstylesshadowoffsety":"0","captionstylesshadowinset":"0","captionstylesuseborders":"1","captionstylesbordercolor":"","captionstylesborderwidth":"1","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(90, 64, 'клиенты en', '', '', 1, 'clients', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_slides', 1, 0, '{"b11-title":"<p>\\u041d\\u0430\\u0448\\u0438 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u044b<\\/p>","slides":"[{|qq|imgname|qq|:|qq|images\\/logos\\/1.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/1.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/1.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/1.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/3.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/3.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/4.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/4.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/5.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/5.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/6.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/6.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/7.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/7.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/8.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/8.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/8.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/8.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|}]","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, 'en-GB'),
(91, 65, 'rates en', '', '', 1, 'rates', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_prodact', 1, 1, '{"title":"\\u0414\\u043b\\u044f \\u043a\\u0440\\u0443\\u043f\\u043d\\u044b\\u0445 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0439 EN","textt":"<p>\\u041f\\u0440\\u0435\\u0438\\u043c\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u0430: \\u043f\\u043e\\u043b\\u043d\\u044b\\u0439 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044c \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u00ab\\u0427\\u0435\\u043a \\u043b\\u0438\\u0441\\u0442\\u00bb \\u0432\\u043d\\u0443\\u0442\\u0440\\u0438 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438, <br \\/> \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c \\u043f\\u0435\\u0440\\u0435\\u0434\\u0435\\u043b\\u0430\\u0442\\u044c \\u0444\\u0443\\u043d\\u043a\\u0446\\u0438\\u043e\\u043d\\u0430\\u043b \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u0441 \\u0443\\u0447\\u0435\\u0442\\u043e\\u043c \\u0438\\u043d\\u0434\\u0438\\u0432\\u0438\\u0434\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u0435\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438<\\/p>","tarif-name-1":"\\u041b\\u0438\\u0446\\u0435\\u043d\\u0437\\u0438\\u044f","tarif-name-2":"\\u0410\\u0440\\u0435\\u043d\\u0434\\u0430","valuta":"\\u0440\\u0443\\u0431.","polz":"\\u041f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u0438","polz-kl-t1":"5","polz-kl-t2":"10","polz-kl-t3":"20","sub-title":" - \\u0412 \\u0432\\u0438\\u0434\\u0435 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u044b \\u043d\\u0430 \\u0441\\u0435\\u0440\\u0432\\u0435\\u0440\\u0435 \\u0437\\u0430\\u043a\\u0430\\u0437\\u0447\\u0438\\u043a\\u0430","list-1":"{\\"name\\":[\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. |\\\\n\\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. |\\\\n\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"1\\u0421:\\u041f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0435 8.|\\\\n\\u041a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0441\\u043a\\u0430\\u044f \\u043b\\u0438\\u0446\\u0435\\u043d\\u0437\\u0438\\u044f \\u043d\\u0430 10 |\\\\n\\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0447\\u0438\\u0445 \\u043c\\u0435\\u0441\\u0442. |\\\\n\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. |\\\\n\\u0418\\u0422\\u0421 \\u043d\\u0430 12 \\u043c\\u0435\\u0441\\u044f\\u0446\\u0435\\u0432\\",\\"\\u0422\\u0435\\u0445\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043e\\u0434\\u0434\\u0435\\u0440\\u0436\\u043a\\u0430 |\\\\n\\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u044f (\\u0432 \\u043c\\u0435\\u0441\\u044f\\u0446)\\",\\"\\u0414\\u043e\\u0441\\u0442\\u0443\\u043f \\u043a \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u043c\\u0443 \\u0441\\u0430\\u0439\\u0442\\u0443\\"],\\"t1\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"4 400\\",\\"500\\"],\\"t2\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"54 400\\",\\"4 400\\",\\"500\\"],\\"t3\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"84 400\\",\\"4 400\\",\\"500\\"],\\"mandatory\\":[\\"1\\",\\"1\\",\\"1\\",null,null,null]}","list-2":"{\\"name\\":[\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\"],\\"t1\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"424 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\"],\\"t2\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"224 400\\",\\"24 400\\",\\"24 400\\"],\\"t3\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"124 400\\",\\"24 400\\"],\\"mandatory\\":[\\"1\\",null,\\"1\\",null,\\"1\\",null,\\"1\\",\\"1\\",null]}","btn":"\\u041e\\u0444\\u043e\\u0440\\u043c\\u0438\\u0442\\u044c \\u0437\\u0430\\u044f\\u0432\\u043a\\u0443","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, 'en-GB'),
(92, 66, 'Main Menu en', '', '', 1, 'topmenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"eng","base":"","startLevel":"1","endLevel":"0","showAllChildren":"1","tag_id":"mainMenu","class_sfx":" navbar-nav navbar-right","window_open":"","layout":"_:default","moduleclass_sfx":"_menu","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, 'en-GB'),
(93, 67, 'Main Menu de', '', '', 1, 'topmenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"de","base":"","startLevel":"1","endLevel":"0","showAllChildren":"1","tag_id":"mainMenu","class_sfx":" navbar-nav navbar-right","window_open":"","layout":"_:default","moduleclass_sfx":"_menu","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, 'de-DE'),
(94, 68, 'rates rus', '', '', 1, 'rates', 535, '2017-06-19 21:52:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_prodact', 1, 1, '{"title":"\\u0414\\u043b\\u044f \\u043a\\u0440\\u0443\\u043f\\u043d\\u044b\\u0445 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0439 RUS","textt":"<p>\\u041f\\u0440\\u0435\\u0438\\u043c\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u0430: \\u043f\\u043e\\u043b\\u043d\\u044b\\u0439 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044c \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u00ab\\u0427\\u0435\\u043a \\u043b\\u0438\\u0441\\u0442\\u00bb \\u0432\\u043d\\u0443\\u0442\\u0440\\u0438 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438, <br \\/> \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c \\u043f\\u0435\\u0440\\u0435\\u0434\\u0435\\u043b\\u0430\\u0442\\u044c \\u0444\\u0443\\u043d\\u043a\\u0446\\u0438\\u043e\\u043d\\u0430\\u043b \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u0441 \\u0443\\u0447\\u0435\\u0442\\u043e\\u043c \\u0438\\u043d\\u0434\\u0438\\u0432\\u0438\\u0434\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u0435\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438<\\/p>","tarif-name-1":"\\u041b\\u0438\\u0446\\u0435\\u043d\\u0437\\u0438\\u044f","polz":"\\u041f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u0438","polz-kl-t1":"5","polz-kl-t2":"10","polz-kl-t3":"20","sub-title":" - \\u0412 \\u0432\\u0438\\u0434\\u0435 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u044b \\u043d\\u0430 \\u0441\\u0435\\u0440\\u0432\\u0435\\u0440\\u0435 \\u0437\\u0430\\u043a\\u0430\\u0437\\u0447\\u0438\\u043a\\u0430","list-1":"{\\"name\\":[\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. |\\\\n\\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. |\\\\n\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"1\\u0421:\\u041f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0435 8.|\\\\n\\u041a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0441\\u043a\\u0430\\u044f \\u043b\\u0438\\u0446\\u0435\\u043d\\u0437\\u0438\\u044f \\u043d\\u0430 10 |\\\\n\\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0447\\u0438\\u0445 \\u043c\\u0435\\u0441\\u0442. |\\\\n\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. |\\\\n\\u0418\\u0422\\u0421 \\u043d\\u0430 12 \\u043c\\u0435\\u0441\\u044f\\u0446\\u0435\\u0432\\",\\"\\u0422\\u0435\\u0445\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043e\\u0434\\u0434\\u0435\\u0440\\u0436\\u043a\\u0430 |\\\\n\\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u044f (\\u0432 \\u043c\\u0435\\u0441\\u044f\\u0446)\\",\\"\\u0414\\u043e\\u0441\\u0442\\u0443\\u043f \\u043a \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u043c\\u0443 \\u0441\\u0430\\u0439\\u0442\\u0443\\"],\\"t1\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"4 400\\",\\"500\\"],\\"t2\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"54 400\\",\\"4 400\\",\\"500\\"],\\"t3\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"84 400\\",\\"4 400\\",\\"500\\"],\\"mandatory\\":[\\"1\\",\\"1\\",\\"1\\",null,null,null]}","list-2":"{\\"name\\":[\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\"],\\"t1\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"424 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\"],\\"t2\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"224 400\\",\\"24 400\\",\\"24 400\\"],\\"t3\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"124 400\\",\\"24 400\\"],\\"mandatory\\":[\\"1\\",null,\\"1\\",null,\\"1\\",null,\\"1\\",\\"1\\",null]}","title2":"\\u0414\\u043b\\u044f \\u043d\\u0435\\u0431\\u043e\\u043b\\u044c\\u0448\\u0438\\u0445 \\u0438 \\u043d\\u0430\\u0447\\u0438\\u043d\\u0430\\u044e\\u0449\\u0438\\u0445 \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0445 \\u0441\\u0435\\u0442\\u0435\\u0439","textt2":"<p>\\u041f\\u0440\\u0435\\u0438\\u043c\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u0430: \\u043f\\u0440\\u043e\\u0441\\u0442\\u043e\\u0442\\u0430 \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u043d\\u0438\\u044f,<br \\/> \\u043d\\u0438\\u0437\\u043a\\u0438\\u0439 \\u0446\\u0435\\u043d\\u043e\\u0432\\u043e\\u0439 \\u043f\\u043e\\u0440\\u043e\\u0433 \\u043f\\u0440\\u0438 \\u0437\\u0430\\u043f\\u0443\\u0441\\u043a\\u0435, \\u043d\\u0435 \\u0442\\u0440\\u0435\\u0431\\u0443\\u0435\\u0442 \\u0441\\u043e\\u0431\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0445 \\u0441\\u0435\\u0440\\u0432\\u0435\\u0440\\u043e\\u0432<\\/p>","tarif-name-2":"\\u0410\\u0440\\u0435\\u043d\\u0434\\u0430","sub-title2":" - \\u0412 \\u0432\\u0438\\u0434\\u0435 \\u043e\\u0431\\u043b\\u0430\\u0447\\u043d\\u043e\\u0433\\u043e \\u0441\\u0435\\u0440\\u0432\\u0438\\u0441\\u0430","tarif-2-1-name":"\\u0422\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0445 \\u0442\\u043e\\u0447\\u0435\\u043a","tarif-kl-2-1":"1","tarif-2-1-\\u0441":"316 746","tarif-kl-2-2":"5","tarif-2-2-\\u0441":"216 746","tarif-kl-2-3":"10","tarif-2-3-\\u0441":"116 7461","list-2-1":"{\\"name\\":[\\"\\u0411\\u0430\\u0437\\u0430 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0432 \\u043e\\u0431\\u043b\\u0430\\u043a\\u0435\\",\\"\\u0414\\u043e\\u0441\\u0442\\u0443\\u043f \\u043a \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u043c\\u0443 \\u0441\\u0430\\u0439\\u0442\\u0443\\",\\"\\u0414\\u043e\\u0441\\u0442\\u0443\\u043f \\u043a \\u0430\\u0434\\u043c\\u0438\\u043d\\u0438\\u0441\\u0442\\u0440\\u0430\\u0442\\u0438\\u0432\\u043d\\u043e\\u0439\\\\n\\u0447\\u0430\\u0441\\u0442\\u0438\\",\\"\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435\\\\n\\u0434\\u043b\\u044f \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440\\u043e\\u0432\\",\\"\\u0422\\u0435\\u0445\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043e\\u0434\\u0434\\u0435\\u0440\\u0436\\u043a\\u0430\\"]}","tarif-2-2-name":"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0432","tarif-kl-2-2-1":"100","tarif-2-2-1-\\u0441":"116 746","tarif-kl-2-2-2":"500","tarif-2-2-2-\\u0441":"216 746","tarif-kl-2-3k":"1000","tarif-2-2-3-\\u0441c":"316 7461","list-2-2":"{\\"name\\":[\\"\\u0411\\u0430\\u0437\\u0430 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0432 \\u043e\\u0431\\u043b\\u0430\\u043a\\u0435\\",\\"\\u0414\\u043e\\u0441\\u0442\\u0443\\u043f \\u043a \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u043c\\u0443 \\u0441\\u0430\\u0439\\u0442\\u0443\\",\\"\\u0414\\u043e\\u0441\\u0442\\u0443\\u043f \\u043a \\u0430\\u0434\\u043c\\u0438\\u043d\\u0438\\u0441\\u0442\\u0440\\u0430\\u0442\\u0438\\u0432\\u043d\\u043e\\u0439\\\\n\\u0447\\u0430\\u0441\\u0442\\u0438\\",\\"\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435\\\\n\\u0434\\u043b\\u044f \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440\\u043e\\u0432\\",\\"\\u0422\\u0435\\u0445\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043e\\u0434\\u0434\\u0435\\u0440\\u0436\\u043a\\u0430\\"]}","btn":"\\u041e\\u0444\\u043e\\u0440\\u043c\\u0438\\u0442\\u044c \\u0437\\u0430\\u044f\\u0432\\u043a\\u0443","cost":"\\u0421\\u0442\\u043e\\u0438\\u043c\\u043e\\u0441\\u0442\\u044c","vkl":"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u043e","valuta":"\\u0440\\u0443\\u0431.","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(95, 69, 'rates de', '', '', 1, 'rates', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_prodact', 1, 1, '{"title":"\\u0414\\u043b\\u044f \\u043a\\u0440\\u0443\\u043f\\u043d\\u044b\\u0445 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0439 DE","textt":"<p>\\u041f\\u0440\\u0435\\u0438\\u043c\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u0430: \\u043f\\u043e\\u043b\\u043d\\u044b\\u0439 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044c \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u00ab\\u0427\\u0435\\u043a \\u043b\\u0438\\u0441\\u0442\\u00bb \\u0432\\u043d\\u0443\\u0442\\u0440\\u0438 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438, <br \\/> \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c \\u043f\\u0435\\u0440\\u0435\\u0434\\u0435\\u043b\\u0430\\u0442\\u044c \\u0444\\u0443\\u043d\\u043a\\u0446\\u0438\\u043e\\u043d\\u0430\\u043b \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u0441 \\u0443\\u0447\\u0435\\u0442\\u043e\\u043c \\u0438\\u043d\\u0434\\u0438\\u0432\\u0438\\u0434\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u0435\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438<\\/p>","tarif-name-1":"\\u041b\\u0438\\u0446\\u0435\\u043d\\u0437\\u0438\\u044f","tarif-name-2":"\\u0410\\u0440\\u0435\\u043d\\u0434\\u0430","valuta":"\\u0440\\u0443\\u0431.","polz":"\\u041f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u0438","polz-kl-t1":"5","polz-kl-t2":"10","polz-kl-t3":"20","sub-title":" - \\u0412 \\u0432\\u0438\\u0434\\u0435 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u044b \\u043d\\u0430 \\u0441\\u0435\\u0440\\u0432\\u0435\\u0440\\u0435 \\u0437\\u0430\\u043a\\u0430\\u0437\\u0447\\u0438\\u043a\\u0430","list-1":"{\\"name\\":[\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. |\\\\n\\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. |\\\\n\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"1\\u0421:\\u041f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0435 8.|\\\\n\\u041a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0441\\u043a\\u0430\\u044f \\u043b\\u0438\\u0446\\u0435\\u043d\\u0437\\u0438\\u044f \\u043d\\u0430 10 |\\\\n\\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0431\\u043e\\u0447\\u0438\\u0445 \\u043c\\u0435\\u0441\\u0442. |\\\\n\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. |\\\\n\\u0418\\u0422\\u0421 \\u043d\\u0430 12 \\u043c\\u0435\\u0441\\u044f\\u0446\\u0435\\u0432\\",\\"\\u0422\\u0435\\u0445\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043e\\u0434\\u0434\\u0435\\u0440\\u0436\\u043a\\u0430 |\\\\n\\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u044f (\\u0432 \\u043c\\u0435\\u0441\\u044f\\u0446)\\",\\"\\u0414\\u043e\\u0441\\u0442\\u0443\\u043f \\u043a \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u043c\\u0443 \\u0441\\u0430\\u0439\\u0442\\u0443\\"],\\"t1\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"4 400\\",\\"500\\"],\\"t2\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"54 400\\",\\"4 400\\",\\"500\\"],\\"t3\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"84 400\\",\\"4 400\\",\\"500\\"],\\"mandatory\\":[\\"1\\",\\"1\\",\\"1\\",null,null,null]}","list-2":"{\\"name\\":[\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\",\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442. | \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430. | \\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430.\\"],\\"t1\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"424 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\"],\\"t2\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"224 400\\",\\"24 400\\",\\"24 400\\"],\\"t3\\":[\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"24 400\\",\\"124 400\\",\\"24 400\\"],\\"mandatory\\":[\\"1\\",null,\\"1\\",null,\\"1\\",null,\\"1\\",\\"1\\",null]}","btn":"\\u041e\\u0444\\u043e\\u0440\\u043c\\u0438\\u0442\\u044c \\u0437\\u0430\\u044f\\u0432\\u043a\\u0443","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, 'de-DE'),
(96, 70, 'клиенты rus', '', '', 1, 'clients', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_slides', 1, 0, '{"b11-title":"<p>\\u041d\\u0430\\u0448\\u0438 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u044b<\\/p>","slides":"[{|qq|imgname|qq|:|qq|images\\/logos\\/1.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/1.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/1.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/1.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/3.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/3.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/4.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/4.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/5.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/5.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/6.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/6.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/7.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/7.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/8.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/8.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/8.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/8.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|}]","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, 'ru-RU');
INSERT INTO `lend_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(97, 71, 'клиенты de', '', '', 1, 'clients', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_slides', 1, 0, '{"b11-title":"<p>\\u041d\\u0430\\u0448\\u0438 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u044b<\\/p>","slides":"[{|qq|imgname|qq|:|qq|images\\/logos\\/1.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/1.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/1.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/1.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/3.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/3.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/4.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/4.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/5.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/5.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/6.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/6.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/7.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/7.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/8.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/8.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|},{|qq|imgname|qq|:|qq|images\\/logos\\/8.png|qq|,|qq|imgcaption|qq|:|qq||qq|,|qq|imgtitle|qq|:|qq||qq|,|qq|imgthumb|qq|:|qq|http:\\/\\/magento.loc\\/images\\/logos\\/8.png|qq|,|qq|imglink|qq|:|qq||qq|,|qq|imgtarget|qq|:|qq|default|qq|,|qq|imgalignment|qq|:|qq|default|qq|,|qq|imgvideo|qq|:|qq||qq|,|qq|slidearticleid|qq|:|qq||qq|,|qq|slidearticlename|qq|:|qq||qq|,|qq|imgtime|qq|:|qq||qq|,|qq|state|qq|:|qq|1|qq|,|qq|startdate|qq|:|qq||qq|,|qq|enddate|qq|:|qq||qq|}]","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, 'de-DE');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_modules_menu`
--

CREATE TABLE `lend_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_modules_menu`
--

INSERT INTO `lend_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(79, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0),
(90, 0),
(91, 0),
(92, 0),
(93, 0),
(94, 0),
(95, 0),
(96, 0),
(97, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_newsfeeds`
--

CREATE TABLE `lend_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `cache_time` int(10) UNSIGNED NOT NULL DEFAULT '3600',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_overrider`
--

CREATE TABLE `lend_overrider` (
  `id` int(10) NOT NULL COMMENT 'Primary Key',
  `constant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `string` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_postinstall_messages`
--

CREATE TABLE `lend_postinstall_messages` (
  `postinstall_message_id` bigint(20) UNSIGNED NOT NULL,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language_extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_postinstall_messages`
--

INSERT INTO `lend_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 0),
(2, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 0),
(3, 700, 'COM_CPANEL_MSG_STATS_COLLECTION_TITLE', 'COM_CPANEL_MSG_STATS_COLLECTION_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/statscollection.php', 'admin_postinstall_statscollection_condition', '3.5.0', 0),
(4, 700, 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_BODY', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_ACTION', 'plg_system_updatenotification', 1, 'action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_condition', '3.6.3', 1),
(5, 700, 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_TITLE', 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/joomla40checks.php', 'admin_postinstall_joomla40checks_condition', '3.7.0', 1),
(6, 700, 'TPL_HATHOR_MESSAGE_POSTINSTALL_TITLE', 'TPL_HATHOR_MESSAGE_POSTINSTALL_BODY', 'TPL_HATHOR_MESSAGE_POSTINSTALL_ACTION', 'tpl_hathor', 1, 'action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_condition', '3.7.0', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_redirect_links`
--

CREATE TABLE `lend_redirect_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_url` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_url` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_schemas`
--

CREATE TABLE `lend_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_schemas`
--

INSERT INTO `lend_schemas` (`extension_id`, `version_id`) VALUES
(700, '3.7.0-2017-04-19');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_session`
--

CREATE TABLE `lend_session` (
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `guest` tinyint(4) UNSIGNED DEFAULT '1',
  `time` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_session`
--

INSERT INTO `lend_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('2a2ek3hk0kac9gp4kav7v5r425', 0, 1, '1497921611', 'joomla|s:644:"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjozOntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjI6e3M6NzoiY291bnRlciI7aToxO3M6NToidGltZXIiO086ODoic3RkQ2xhc3MiOjM6e3M6NToic3RhcnQiO2k6MTQ5NzkyMTYxMTtzOjQ6Imxhc3QiO2k6MTQ5NzkyMTYxMTtzOjM6Im5vdyI7aToxNDk3OTIxNjExO319czo4OiJyZWdpc3RyeSI7TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjA6e31zOjE0OiIAKgBpbml0aWFsaXplZCI7YjowO3M6OToic2VwYXJhdG9yIjtzOjE6Ii4iO31zOjQ6InVzZXIiO086NToiSlVzZXIiOjE6e3M6MjoiaWQiO2k6MDt9fX1zOjE0OiIAKgBpbml0aWFsaXplZCI7YjowO3M6OToic2VwYXJhdG9yIjtzOjE6Ii4iO30=";', 0, ''),
('8lcp5d30j7dpk0htrrogi41f57', 0, 1, '1497921431', 'joomla|s:648:"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjozOntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjI6e3M6NzoiY291bnRlciI7aToyNzQ7czo1OiJ0aW1lciI7Tzo4OiJzdGRDbGFzcyI6Mzp7czo1OiJzdGFydCI7aToxNDk3ODg5MTYzO3M6NDoibGFzdCI7aToxNDk3OTIxMzE0O3M6Mzoibm93IjtpOjE0OTc5MjE0MzE7fX1zOjg6InJlZ2lzdHJ5IjtPOjI0OiJKb29tbGFcUmVnaXN0cnlcUmVnaXN0cnkiOjM6e3M6NzoiACoAZGF0YSI7Tzo4OiJzdGRDbGFzcyI6MDp7fXM6MTQ6IgAqAGluaXRpYWxpemVkIjtiOjA7czo5OiJzZXBhcmF0b3IiO3M6MToiLiI7fXM6NDoidXNlciI7Tzo1OiJKVXNlciI6MTp7czoyOiJpZCI7aTowO319fXM6MTQ6IgAqAGluaXRpYWxpemVkIjtiOjA7czo5OiJzZXBhcmF0b3IiO3M6MToiLiI7fQ==";', 0, ''),
('tj88ehutce609g5ftddeedlnb2', 1, 0, '1497916344', 'joomla|s:1228:"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjo0OntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjM6e3M6NzoiY291bnRlciI7aTo4NjtzOjU6InRpbWVyIjtPOjg6InN0ZENsYXNzIjozOntzOjU6InN0YXJ0IjtpOjE0OTc4OTA1NzE7czo0OiJsYXN0IjtpOjE0OTc5MTI3NDM7czozOiJub3ciO2k6MTQ5NzkxNjM0NDt9czo1OiJ0b2tlbiI7czozMjoidXR6WFI2MHc5RUY1N0NaMDBBS3ZadHRmSDJaV0hhM0EiO31zOjg6InJlZ2lzdHJ5IjtPOjI0OiJKb29tbGFcUmVnaXN0cnlcUmVnaXN0cnkiOjM6e3M6NzoiACoAZGF0YSI7Tzo4OiJzdGRDbGFzcyI6Mjp7czoxMzoiY29tX2luc3RhbGxlciI7Tzo4OiJzdGRDbGFzcyI6Mjp7czo3OiJtZXNzYWdlIjtzOjA6IiI7czoxNzoiZXh0ZW5zaW9uX21lc3NhZ2UiO3M6MDoiIjt9czoxMToiY29tX21vZHVsZXMiO086ODoic3RkQ2xhc3MiOjI6e3M6NDoiZWRpdCI7Tzo4OiJzdGRDbGFzcyI6MTp7czo2OiJtb2R1bGUiO086ODoic3RkQ2xhc3MiOjI6e3M6MjoiaWQiO2E6MTp7aTowO2k6OTQ7fXM6NDoiZGF0YSI7Tjt9fXM6MzoiYWRkIjtPOjg6InN0ZENsYXNzIjoxOntzOjY6Im1vZHVsZSI7Tzo4OiJzdGRDbGFzcyI6Mjp7czoxMjoiZXh0ZW5zaW9uX2lkIjtOO3M6NjoicGFyYW1zIjtOO319fX1zOjE0OiIAKgBpbml0aWFsaXplZCI7YjowO3M6OToic2VwYXJhdG9yIjtzOjE6Ii4iO31zOjQ6InVzZXIiO086NToiSlVzZXIiOjE6e3M6MjoiaWQiO3M6MzoiNTM1Ijt9czoxMToiYXBwbGljYXRpb24iO086ODoic3RkQ2xhc3MiOjE6e3M6NToicXVldWUiO047fX19czoxNDoiACoAaW5pdGlhbGl6ZWQiO2I6MDtzOjk6InNlcGFyYXRvciI7czoxOiIuIjt9";', 535, 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_tags`
--

CREATE TABLE `lend_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_tags`
--

INSERT INTO `lend_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 535, '2017-06-14 17:54:32', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_template_styles`
--

CREATE TABLE `lend_template_styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `home` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_template_styles`
--

INSERT INTO `lend_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '0', 'Beez3 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/joomla_black.png","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","templatecolor":"personal","html5":"0"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{"showSiteName":"0","colourChoice":"","boldText":"0"}'),
(7, 'protostar', 0, '0', 'protostar - Default', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}'),
(8, 'isis', 1, '1', 'isis - Default', '{"templateColor":"","logoFile":""}'),
(9, 'xemlab', 0, 'ru-RU', 'ru', '{"logoFile":"images\\/logo.png","number":"+7 (495) 268 07 49","hedderCall":"\\u0417\\u0430\\u043a\\u0430\\u0437\\u0430\\u0442\\u044c \\u0437\\u0432\\u043e\\u043d\\u043e\\u043a","f-link-text":"\\u041f\\u043e\\u043b\\u0438\\u0442\\u0438\\u043a\\u0430 \\u043a\\u043e\\u043d\\u0444\\u0438\\u0434\\u0435\\u043d\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u043e\\u0441\\u0442\\u0438","f-link":"#","f-copy":"<p>\\u00a9 \\"\\u0427\\u0435\\u043a \\u041b\\u0438\\u0441\\u0442\\" <br \\/>\\u0412\\u0441\\u0435 \\u043f\\u0440\\u0430\\u0432\\u0430 \\u0437\\u0430\\u0449\\u0438\\u0449\\u0435\\u043d\\u044b<\\/p>","c-num":"+7 (495) 268 07 49","c-addres":"<p>\\u041e\\u041e\\u041e \\u00ab\\u041e\\u043f\\u0442\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0430\\u043b\\u0433\\u043e\\u0440\\u0438\\u0442\\u043c\\u044b\\u00bb <br \\/> \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u041e\\u0440\\u0434\\u0436\\u043e\\u043d\\u0438\\u043a\\u0438\\u0434\\u0437\\u0435, \\u0434. 11<\\/p>","c-mail":"info@mail.ru","c-f-title":"\\u041e\\u0441\\u0442\\u0430\\u043b\\u0438\\u0441\\u044c \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b?","c-f-text":"<p>\\u0417\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435 \\u0444\\u043e\\u0440\\u043c\\u0443 \\u043a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u043e\\u0432 \\u0438 \\u043d\\u0430\\u0448 <br \\/> \\u043c\\u0435\\u043d\\u0435\\u0434\\u0436\\u0435\\u0440 \\u0441\\u0432\\u044f\\u0436\\u0438\\u0442\\u0441\\u044f \\u0441 \\u0432\\u0430\\u043c\\u0438, \\u0438 \\u043e\\u0442\\u0432\\u0435\\u0442\\u0438\\u0442 \\u043d\\u0430 \\u0432\\u0441\\u0435 \\u0432\\u0430\\u0448\\u0438 \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b<\\/p>","c-f-btn":"\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e","c-f-error":"\\u0417\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435 \\u0432\\u0441\\u0435 \\u043f\\u043e\\u043b\\u044f!","c-f-compile":"\\u041e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u043e!","b1-title":"<p>\\u0423\\u0441\\u043a\\u043e\\u0440\\u044c\\u0442\\u0435 \\u0440\\u0430\\u0437\\u0432\\u0438\\u0442\\u0438\\u0435 \\u0432\\u0430\\u0448\\u0435\\u0433\\u043e \\u0431\\u0438\\u0437\\u043d\\u0435\\u0441\\u0430 <br \\/> \\u0443\\u043c\\u043d\\u043e\\u0439 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u043e\\u0439 \\u201c\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u201d<\\/p>","b1-img":"images\\/bg\\/ipadhome.png","b1-list-1":"<p><b>\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435:<\\/b> \\u0441\\u0430\\u043c\\u044b\\u0439 \\u0443\\u0434\\u043e\\u0431\\u043d\\u044b\\u0439 \\u0438\\u043d\\u0442\\u0435\\u0440\\u0444\\u0435\\u0439\\u0441 \\u0434\\u043b\\u044f \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u0430;<br \\/> \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b \\u0432 \\u0430\\u0432\\u0442\\u043e\\u043d\\u043e\\u043c\\u043d\\u043e\\u043c \\u0440\\u0435\\u0436\\u0438\\u043c\\u0435<\\/p>","b1-list-2":"<p><b>\\u0415\\u0434\\u0438\\u043d\\u0430\\u044f \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430, \\u0441 \\u0440\\u0430\\u0437\\u0433\\u0440\\u0430\\u043d\\u0438\\u0447\\u0435\\u043d\\u0438\\u0435\\u043c \\u043f\\u0440\\u0430\\u0432 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u0430,<\\/b> \\u0434\\u043b\\u044f \\u0432\\u0441\\u0435\\u0445 \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043b\\u0438\\u0446: \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u043e\\u0432, \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u043e\\u0432 \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0445 \\u0442\\u043e\\u0447\\u0435\\u043a, \\u0440\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0435\\u0439 \\u041e\\u041a\\u041a, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0445 \\u0438 \\u0434\\u0440.<\\/p>","b1-list-3":"<p><b>\\u041f\\u0440\\u043e\\u0437\\u0440\\u0430\\u0447\\u043d\\u043e\\u0441\\u0442\\u044c \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a,<\\/b> \\u0431\\u043b\\u0430\\u0433\\u043e\\u0434\\u0430\\u0440\\u044f \\u0432\\u0435\\u0441\\u043e\\u043c\\u043e\\u043c\\u0443 <br \\/> \\u043d\\u0430\\u0431\\u043e\\u0440\\u0443 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432<\\/p>","b1-f-link-text":"\\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0430\\u044f \\u0434\\u0435\\u043c\\u043e-\\u0432\\u0435\\u0440\\u0441\\u0438\\u044f","b1-f-link":"#","b2-f-link-text":"\\u041a\\u0430\\u043a \\u044d\\u0442\\u043e \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u0442","b2-f-link":"#","b2-title":"<p>\\u041f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c\\u044b \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b, <br \\/> \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0435 \\u0437\\u0430\\u043c\\u0435\\u0434\\u043b\\u044f\\u044e\\u0442 \\u0432\\u0430\\u0448 \\u0431\\u0438\\u0437\\u043d\\u0435\\u0441<\\/p>","b2b1-title":"<p>\\u041d\\u0435\\u0442 \\u0435\\u0434\\u0438\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b <br \\/> \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445<\\/p>","b2b1-text":"<p>\\u041f\\u0440\\u0438\\u0445\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u044c \\u0442\\u0430\\u0431\\u043b\\u0438\\u0446\\u044b Excel, \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0443\\u044e \\u043f\\u043e\\u0447\\u0442\\u0443, \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f, \\u0434\\u0440\\u0443\\u0433\\u0438\\u0435 \\u0441\\u043f\\u043e\\u0441\\u043e\\u0431\\u044b, \\u0447\\u0442\\u043e \\u043e\\u0447\\u0435\\u043d\\u044c \\u043d\\u0435\\u0443\\u0434\\u043e\\u0431\\u043d\\u043e<\\/p>","b2b2-title":"<p>\\u041d\\u0435\\u0442 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 <br \\/> \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432 \\u0437\\u0430 \\u0432\\u044b\\u0431\\u0440\\u0430\\u043d\\u043d\\u044b\\u0439 \\u043f\\u0435\\u0440\\u0438\\u043e\\u0434<\\/p>","b2b2-text":"<p>\\u0411\\u0435\\u0437 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432 \\u0441\\u043b\\u043e\\u0436\\u043d\\u043e \\u043f\\u043e\\u043d\\u044f\\u0442\\u044c \\u0434\\u0438\\u043d\\u0430\\u043c\\u0438\\u043a\\u0443 \\u0440\\u043e\\u0441\\u0442\\u0430, \\u0438\\u0437\\u043c\\u0435\\u043d\\u0435\\u043d\\u0438\\u0439; \\u043f\\u0440\\u0438\\u0445\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0441\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0442\\u044c \\u0438\\u0445 \\u0432\\u0440\\u0443\\u0447\\u043d\\u0443\\u044e, \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u044f \\u0441\\u0442\\u043e\\u0440\\u043e\\u043d\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u044b<\\/p>","b2b3-title":"<p>\\u041d\\u0435\\u0443\\u0434\\u043e\\u0431\\u043d\\u044b\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f <br \\/> \\u0434\\u043b\\u044f \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f<\\/p>","b2b3-text":"<p>\\u041e\\u043f\\u0440\\u043e\\u0441 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u043e\\u0432, \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0435 \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u044e\\u0442 \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f \\u0434\\u043b\\u044f \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a \\u0434\\u0430\\u043b \\u043f\\u043e\\u043d\\u044f\\u0442\\u044c, \\u0447\\u0442\\u043e \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435 \\u043d\\u0435\\u0442 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f, \\u043a\\u043e\\u0442\\u043e\\u0440\\u043e\\u0435 \\u0434\\u0435\\u0439\\u0441\\u0442\\u0432\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e \\u0443\\u0434\\u043e\\u0431\\u043d\\u043e \\u043d\\u0430 \\u043f\\u0440\\u0430\\u043a\\u0442\\u0438\\u043a\\u0435<\\/p>","b2b4-title":"<p>\\u0412\\u044b\\u044f\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c\\u044b <br \\/> \\u0443\\u0441\\u0442\\u0440\\u0430\\u043d\\u044f\\u044e\\u0442\\u0441\\u044f \\u043c\\u0435\\u0434\\u043b\\u0435\\u043d\\u043d\\u043e<\\/p>","b2b4-text":"<p>\\u041e\\u0442\\u0441\\u0443\\u0442\\u0441\\u0442\\u0432\\u0438\\u0435 \\u0446\\u0435\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438, \\u0441\\u043a\\u0430\\u0437\\u044b\\u0432\\u0430\\u0435\\u0442\\u0441\\u044f \\u043d\\u0430 \\u0441\\u043a\\u043e\\u0440\\u043e\\u0441\\u0442\\u0438 \\u043f\\u0435\\u0440\\u0435\\u0434\\u0430\\u0447\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445, \\u0443\\u0441\\u0442\\u0440\\u0430\\u043d\\u0435\\u043d\\u0438\\u0438 \\u0432\\u044b\\u044f\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c; \\u0447\\u0442\\u043e \\u0432 \\u0441\\u0432\\u043e\\u044e \\u043e\\u0447\\u0435\\u0440\\u0435\\u0434\\u044c \\u0437\\u0430\\u043c\\u0435\\u0434\\u043b\\u044f\\u0435\\u0442 \\u0440\\u0430\\u0437\\u0432\\u0438\\u0442\\u0438\\u0435 \\u0431\\u0438\\u0437\\u043d\\u0435\\u0441\\u0430 \\u0432 \\u0446\\u0435\\u043b\\u043e\\u043c<\\/p>","b2b5-title":"<p>\\u0421\\u043f\\u043e\\u0440\\u044b \\u00ab\\u043a\\u0442\\u043e \\u043f\\u0440\\u0430\\u0432\\u2026\\u00bb <br \\/> \\u0432\\u043c\\u0435\\u0441\\u0442\\u043e \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u044f \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c<\\/p>","b2b5-text":"<p>\\u041d\\u0435\\u043f\\u0440\\u043e\\u0437\\u0440\\u0430\\u0447\\u043d\\u043e\\u0441\\u0442\\u044c \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a, \\u043f\\u0440\\u0438\\u0432\\u043e\\u0434\\u0438\\u0442 \\u043a \\u0441\\u043f\\u043e\\u0440\\u0430\\u043c \\u0441\\u0442\\u043e\\u0440\\u043e\\u043d, \\u0431\\u0435\\u0437 \\u0444\\u0430\\u043a\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0433\\u043e \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0435\\u043d\\u0438\\u044f \\u0432\\u044b\\u044f\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c, \\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u0438\\u0442\\u0441\\u044f \\u0441\\u043b\\u043e\\u0436\\u043d\\u043e \\u0443\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u0438\\u0442\\u044c \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043b\\u0438\\u0446<\\/p>","b3-title":"<p>\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\" - \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u044f \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u044f <br \\/> \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430, \\u0438\\u0434\\u0435\\u0430\\u043b\\u044c\\u043d\\u043e \\u043f\\u043e\\u0434\\u0445\\u043e\\u0434\\u0438\\u0442 \\u0434\\u043b\\u044f:<\\/p>","b3-s1-title":"<p>\\u041e\\u0431\\u0449\\u0435\\u043f\\u0438\\u0442<\\/p>","b3-s1-list-1":"{\\"text\\":[\\"\\u0411\\u0430\\u0440\\",\\"\\u0411\\u0438\\u0441\\u0442\\u0440\\u043e\\",\\"\\u0411\\u0443\\u0440\\u0433\\u0435\\u0440\\u043d\\u0430\\u044f\\",\\"\\u0417\\u0430\\u043a\\u0443\\u0441\\u043e\\u0447\\u043d\\u0430\\u044f\\",\\"\\u041a\\u0430\\u0444\\u0435\\",\\"\\u041a\\u043e\\u0444\\u0435\\u0439\\u043d\\u044f\\",\\"\\u041f\\u0430\\u0431\\",\\"\\u041f\\u0438\\u0446\\u0446\\u0435\\u0440\\u0438\\u044f\\",\\"\\u0420\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d\\",\\"\\u0420\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d \\u0431\\/\\u043f\\",\\"\\u0421\\u0442\\u043e\\u043b\\u043e\\u0432\\u0430\\u044f\\",\\"\\u0421\\u0443\\u0448\\u0438-\\u0431\\u0430\\u0440\\",\\"\\u0425\\u0438\\u043d\\u043a\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\\",\\"\\u0427\\u0430\\u0439\\u043d\\u0430\\u044f\\",\\"\\u0427\\u0430\\u0439\\u0445\\u0430\\u043d\\u0430\\"]}","b3-s2-title":"<p>\\u041e\\u0431\\u0440\\u0430\\u0437\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435<\\/p>","b3-s2":"{\\"text\\":[\\"\\u0412\\u0423\\u0417\\",\\"\\u0414\\u0435\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0434\\",\\"\\u041a\\u043e\\u043b\\u043b\\u0435\\u0434\\u0436\\",\\"\\u041f\\u0422\\u0423\\",\\"\\u0428\\u043a\\u043e\\u043b\\u0430\\"]}","b3-s3-title":"<p>\\u041c\\u0443\\u043d\\u0438\\u0446\\u0438\\u043f\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 <br \\/> \\u043f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u044f<\\/p>","b3-s3":"{\\"text\\":[\\"\\u0410\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u043d\\u044b\\u0439 \\u043f\\u0430\\u0440\\u043a\\",\\"\\u0410\\u044d\\u0440\\u043e\\u043f\\u043e\\u0440\\u0442\\",\\"\\u0412\\u043e\\u043a\\u0437\\u0430\\u043b\\",\\"\\u0414\\u0435\\u043f\\u043e \\u043c\\u0435\\u0442\\u0440\\u043e\\u043f\\u043e\\u043b\\u0438\\u0442\\u0435\\u043d\\u0430\\",\\"\\u0422\\u0440\\u0430\\u043c\\u0432\\u0430\\u0439\\u043d\\u043e\\u0435 \\u0434\\u0435\\u043f\\u043e\\",\\"\\u0422\\u0440\\u043e\\u043b\\u043b\\u0435\\u0439\\u0431\\u0443\\u0441\\u043d\\u044b\\u0439 \\u043f\\u0430\\u0440\\"]}","b3-s4-title":"<p>\\u041a\\u0440\\u0430\\u0441\\u043e\\u0442\\u0430 <br \\/> \\u0438 \\u0437\\u0434\\u043e\\u0440\\u043e\\u0432\\u044c\\u0435<\\/p>","b3-s4":"{\\"text\\":[\\"\\u0411\\u0430\\u043d\\u044f\\",\\"\\u041c\\u0430\\u043d\\u0438\\u043a\\u044e\\u0440\\u043d\\u0430\\u044f\\",\\"\\u041f\\u0430\\u0440\\u0438\\u043a\\u043c\\u0430\\u0445\\u0435\\u0440\\u0441\\u043a\\u0430\\u044f\\",\\"\\u0421\\u0430\\u043b\\u043e\\u043d \\u043a\\u0440\\u0430\\u0441\\u043e\\u0442\\u044b\\",\\"\\u0421\\u043f\\u043e\\u0440\\u0442\\u0438\\u0432\\u043d\\u0430\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u044f\\",\\"\\u0421\\u0442\\u043e\\u043c\\u0430\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u043a\\u0430\\u0431\\u0438\\u043d\\u0435\\u0442\\",\\"\\u0424\\u0438\\u0442\\u043d\\u0435\\u0441-\\u043a\\u043b\\u0443\\u0431\\",\\"\\u0427\\u0430\\u0441\\u0442\\u043d\\u0430\\u044f \\u043a\\u043b\\u0438\\u043d\\u0438\\u043a\\u0430\\",\\"SPA-\\u0441\\u0430\\u043b\\u043e\\u043d\\"]}","b3-s5-title":"<p>\\u0420\\u0430\\u0437\\u0432\\u043b\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f<\\/p>","b3-s5":"{\\"text\\":[\\"\\u0410\\u043a\\u0432\\u0430\\u043f\\u0430\\u0440\\u043a\\",\\"\\u0411\\u0438\\u043b\\u044c\\u044f\\u0440\\u0434\\u043d\\u0430\\u044f\\",\\"\\u0411\\u043e\\u0443\\u043b\\u0438\\u043d\\u0433\\",\\"\\u0412\\u044b\\u0441\\u0442\\u0430\\u0432\\u043e\\u0447\\u043d\\u044b\\u0439 \\u043a\\u043e\\u043c\\u043f\\u043b\\u0435\\u043a\\u0441\\",\\"\\u0417\\u043e\\u043e\\u043f\\u0430\\u0440\\u043a\\",\\"\\u041a\\u0438\\u043d\\u043e\\u0442\\u0435\\u0430\\u0442\\u0440\\",\\"\\u041a\\u043e\\u043d\\u0446\\u0435\\u0440\\u0442\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043b\\",\\"\\u041c\\u0443\\u0437\\u0435\\u0439\\",\\"\\u041d\\u043e\\u0447\\u043d\\u043e\\u0439 \\u043a\\u043b\\u0443\\u0431\\",\\"\\u041f\\u0430\\u0440\\u043a \\u0430\\u0442\\u0442\\u0440\\u0430\\u043a\\u0446\\u0438\\u043e\\u043d\\u043e\\u0432\\",\\"\\u0421\\u043f\\u043e\\u0440\\u0442\\u0438\\u0432\\u043d\\u0430\\u044f \\u0430\\u0440\\u0435\\u043d\\u0430\\",\\"\\u0421\\u043f\\u043e\\u0440\\u0442\\u0438\\u0432\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043b\\",\\"\\u0422\\u0435\\u0430\\u0442\\u0440\\"]}","b3-s6-title":"<p>\\u041f\\u043e\\u0432\\u0441\\u0435\\u0434\\u043d\\u0435\\u0432\\u043d\\u044b\\u0435 <br \\/> \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438<\\/p>","b3-s6":"{\\"text\\":[\\"\\u0410\\u0432\\u0442\\u043e\\u0437\\u0430\\u043f\\u0440\\u0430\\u0432\\u043a\\u0430\\",\\"\\u0410\\u0432\\u0442\\u043e\\u043c\\u043e\\u0439\\u043a\\u0430\\",\\"\\u0411\\u0430\\u043d\\u043a\\",\\"\\u0421\\u043b\\u0443\\u0436\\u0431\\u0430 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0438\\",\\"\\u0421\\u0422\\u041e\\",\\"\\u0422\\u0430\\u043a\\u0441\\u0438\\",\\"\\u0428\\u0438\\u043d\\u043e\\u043c\\u043e\\u043d\\u0442\\u0430\\u0436\\"]}","b3-s7-title":"<p>\\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u044b<\\/p>","b3-s7":"{\\"text\\":[\\"\\u0413\\u0438\\u043f\\u0435\\u0440\\u043c\\u0430\\u0440\\u043a\\u0435\\u0442 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u0438\\u043a\\u0438\\",\\"\\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\\\\\"\\u0432\\u043e\\u0437\\u043b\\u0435 \\u0434\\u043e\\u043c\\u0430\\\\\\"\\",\\"\\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\u043e\\u0434\\u0435\\u0436\\u0434\\u044b \\u0438 \\u043e\\u0431\\u0443\\u0432\\u0438\\",\\"\\u041f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u043e\\u0432\\u044b\\u0439 \\u0433\\u0438\\u043f\\u0435\\u0440\\u043c\\u0430\\u0440\\u043a\\u0435\\u0442\\",\\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u0438\\u0437\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0435 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u044b\\",\\"\\u0421\\u0442\\u0440\\u043e\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0433\\u0438\\u043f\\u0435\\u0440\\u043c\\u0430\\u0440\\u043a\\u0435\\u0442\\"]}","b4-title":"<p>\\u0415\\u0434\\u0438\\u043d\\u0430\\u044f \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f <br \\/> \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430, \\u0438\\u0437 \\u0442\\u0440\\u0435\\u0445 \\u043a\\u043e\\u043c\\u043f\\u043e\\u043d\\u0435\\u043d\\u0442\\u043e\\u0432, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0435 1\\u0421<\\/p>","shemFile":"images\\/system.png","b4b1-title":"<p>\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442<\\/p>","b4b1-text":"<p>\\u0424\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432 \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b; \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u044f \\u043e\\u0431\\u0440\\u0430\\u0442\\u043d\\u043e\\u0439 \\u0441\\u0432\\u044f\\u0437\\u0438 \\u043c\\u0435\\u0436\\u0434\\u0443 \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438 \\u0440\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d\\u043e\\u0432 \\u0438 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438<\\/p>","b4b2-title":"<p>\\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f \\u0431\\u0430\\u0437\\u0430,<br \\/> \\u0440\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u043d\\u043d\\u0430\\u044f \\u043d\\u0430 \\u043f\\u043b\\u0430\\u0442\\u0444\\u043e\\u0440\\u043c\\u0435 \\u00ab1\\u0421:\\u041f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u044f\\u00bb<\\/p>","b4b2-text":"<p>\\u0412 \\u043d\\u0435\\u0439 \\u0441\\u043e\\u0431\\u0438\\u0440\\u0430\\u0435\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u043e \\u043f\\u0440\\u043e\\u0432\\u0435\\u0434\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0430\\u0445 \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0445 \\u0442\\u043e\\u0447\\u0435\\u043a \\u0438 \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u0443\\u044e\\u0442\\u0441\\u044f \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0435 \\u043e\\u0442\\u0447\\u0435\\u0442\\u044b<\\/p>","b4b3-title":"<p>\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435 \\u00ab\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u00bb<\\/p>","b4b3-text":"<p>\\u0421 \\u0435\\u0433\\u043e \\u043f\\u043e\\u043c\\u043e\\u0449\\u044c\\u044e \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u044f\\u0435\\u0442 \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u0443\\u044e \\u0442\\u043e\\u0447\\u043a\\u0443 \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u0435\\u0442 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438 \\u0432 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u0443\\u044e \\u0431\\u0430\\u0437\\u0443<\\/p>","b5-title":"<p>\\u0421\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u043f\\u043e\\u0437\\u0432\\u043e\\u043b\\u044f\\u0435\\u0442 \\u0430\\u0432\\u0442\\u043e\\u043c\\u0430\\u0442\\u0438\\u0437\\u0438\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c <br \\/> \\u0441\\u043b\\u0435\\u0434\\u0443\\u044e\\u0449\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0446\\u0435\\u0441\\u0441\\u044b:<\\/p>","screen-1":"images\\/iphone-6.png","screen-2":"images\\/screen-1.png","screen-3":"images\\/screen-2.png","b5-list":"{\\"text\\":[\\"\\u041f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u043e\\u0432 \\u0440\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c\\u0438, \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438\\",\\"\\u041f\\u0440\\u043e\\u0432\\u0435\\u0434\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440\\u0430\\u043c\\u0438 \\u043f\\u043e \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u0430\\u043c\\",\\"\\u041a\\u043e\\u043d\\u0441\\u043e\\u043b\\u0438\\u0434\\u0430\\u0446\\u0438\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0438 \\u043e \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u0430\\u0445 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438 \\u0432 \\u0435\\u0434\\u0438\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435\\",\\"\\u0420\\u0430\\u0437\\u043d\\u043e\\u0441\\u0442\\u043e\\u0440\\u043e\\u043d\\u043d\\u0438\\u0439 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0437 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a (\\u0434\\u0438\\u043d\\u0430\\u043c\\u0438\\u043a\\u0430 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432,                         \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433 \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0445, \\u043d\\u0430\\u0438\\u0431\\u043e\\u043b\\u0435\\u0435 \\u0447\\u0430\\u0441\\u0442\\u044b\\u0435 \\u043f\\u0440\\u0438\\u0447\\u0438\\u043d\\u044b                         \\u043f\\u043b\\u043e\\u0445\\u0438\\u0445 \\u043e\\u0446\\u0435\\u043d\\u043e\\u043a \\u0438 \\u043c\\u043d\\u043e\\u0433\\u043e\\u0435 \\u0434\\u0440\\u0443\\u0433\\u043e\\u0435)\\",\\"\\u041f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0438 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044c \\u043c\\u0435\\u0440\\u043e\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0439, \\u043d\\u0430\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043d\\u0430 \\u0443\\u0441\\u0442\\u0440\\u0430\\u043d\\u0435\\u043d\\u0438\\u0435                         \\u043f\\u0440\\u0438\\u0447\\u0438\\u043d \\u00ab\\u043e\\u0442\\u0440\\u0438\\u0446\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445\\u00bb \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a\\",\\"\\u041e\\u0431\\u0440\\u0430\\u0442\\u043d\\u0430\\u044f \\u0441\\u0432\\u044f\\u0437\\u044c \\u043c\\u0435\\u0436\\u0434\\u0443 \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438 \\u0438 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438: \\u043f\\u0440\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u0435                         \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0432, \\u0441 \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c\\u044e \\u0434\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c \\u0444\\u043e\\u0442\\u043e, \\u043a\\u0430\\u043a \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435                         \\u0444\\u0430\\u043a\\u0442\\u0430 \\u043d\\u0430\\u0440\\u0443\\u0448\\u0435\\u043d\\u0438\\u0439\\",\\"\\u0420\\u0430\\u0437\\u0433\\u0440\\u0430\\u043d\\u0438\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0443\\u0440\\u043e\\u0432\\u043d\\u0435\\u0439 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u0430 \\u043a \\u0434\\u0430\\u043d\\u043d\\u044b\\u043c \\u0434\\u043b\\u044f \\u0440\\u0430\\u0437\\u043b\\u0438\\u0447\\u043d\\u044b\\u0445 \\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                         (\\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u044b \\u0443\\u0440\\u043e\\u0432\\u043d\\u0438: \\u0414\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440, \\u0410\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440, \\u0422\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0439)\\",\\"\\u0411\\u044b\\u0441\\u0442\\u0440\\u044b\\u0439, \\u0443\\u0434\\u043e\\u0431\\u043d\\u044b\\u0439 \\u043e\\u0431\\u043c\\u0435\\u043d \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0435\\u0439 \\u043c\\u0435\\u0436\\u0434\\u0443 \\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u044f\\u043c\\u0438 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b                         (\\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 \\u043f\\u0440\\u043e\\u0432\\u0435\\u043b \\u0430\\u0443\\u0434\\u0438\\u0442, \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u043b \\u0434\\u0430\\u043d\\u043d\\u044b\\u0435 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438 \\u0432 \\u0431\\u0430\\u0437\\u0443, \\u0440\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0438                         \\u043f\\u043e\\u043b\\u0443\\u0447\\u0438\\u043b\\u0438 \\u043e\\u0442\\u0447\\u0435\\u0442\\u044b), \\u043d\\u0438\\u043a\\u0430\\u043a\\u0438\\u0445 \\u043f\\u0435\\u0440\\u0435\\u0441\\u044b\\u043b\\u043e\\u043a \\u043f\\u043e \\u043f\\u043e\\u0447\\u0442\\u0435, \\u0440\\u0443\\u0447\\u043d\\u043e\\u0433\\u043e \\u0432\\u0432\\u043e\\u0434\\u0430 \\u0438 \\u043f\\u0440\\u043e\\u0447\\u0435\\u0433\\u043e.\\",\\"\\u0418\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u00ab\\u0431\\u0435\\u0437\\u0431\\u0443\\u043c\\u0430\\u0436\\u043d\\u043e\\u0439\\u00bb \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u0438 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430\\"]}","uznB":"\\u0423\\u0437\\u043d\\u0430\\u0442\\u044c \\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0439","uznBl":"#","b6-title":"<p>\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0435 \\u043e\\u0442\\u0447\\u0435\\u0442\\u044b<\\/p>","b6-list-text-1":"\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-1":"images\\/slider\\/Layer-0.png","b6-list-text-2":"2 \\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-2":"images\\/slider\\/Layer-0.png","b6-list-text-3":"\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-3":"images\\/slider\\/Layer-0.png","b6-list-text-4":"3 \\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-4":"images\\/shem.png","b6-list-text-5":"4  \\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-5":"images\\/slider\\/Layer-0.png","b7-title":"<p>\\u041a\\u0430\\u043a \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u0442 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430?<\\/p>","b7-text":"<p>\\u0426\\u0438\\u043a\\u043b \\u0432\\u0437\\u0430\\u0438\\u043c\\u043e\\u0434\\u0435\\u0439\\u0441\\u0442\\u0432\\u0438\\u044f \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b \\u0438 <br \\/> \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0439<\\/p>","b7b3-title":"<p>\\u0421\\u0438\\u043d\\u0445\\u0440\\u043e\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u044f<\\/p>","planFile":"images\\/bg\\/how-do-CENTR.png","b7-list-text":"{\\"text\\":[\\"1 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"2 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"3 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"4 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"5 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\"]}","b7b1-title":"<p>\\u041f\\u0440\\u043e\\u0432\\u0435\\u0434\\u0435\\u043d\\u0438\\u0435 <br \\/> \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0432<\\/p>","b7b1l1-title":"\\u041c\\u041e\\u0421\\u041a\\u0412\\u0410","b7b1l1-list":"{\\"text\\":[\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 1\\",\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 2\\"]}","b7b1l2-title":"\\u0420\\u0415\\u0413\\u0418\\u041e\\u041d\\u042b","b7b1l2-list":"{\\"text\\":[\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 3\\",\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 4\\",\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 5\\"]}","b7b2-title":"<p>\\u041f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 <br \\/> \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0432<\\/p>","b7b2l1-list":"{\\"text\\":[\\"\\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0438 \\u041e\\u041a\\u041a\\",\\"\\u0422\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0435\\",\\"\\u0410\\u043a\\u0446\\u0438\\u043e\\u043d\\u0435\\u0440\\u044b\\"]}","b8-title":"<p>\\u041d\\u0415 \\u0417\\u041d\\u0410\\u0415\\u0422\\u0415 \\u041a\\u0410\\u041a\\u041e\\u0419 \\u0422\\u0410\\u0420\\u0418\\u0424 \\u0412\\u042b\\u0411\\u0420\\u0410\\u0422\\u042c?<\\/p>","b8-text":"<p>\\u041c\\u044b \\u0432\\u0430\\u043c \\u043f\\u043e\\u043c\\u043e\\u0436\\u0435\\u043c!<\\/p>","b8-button":"\\u041f\\u043e\\u0434\\u043e\\u0431\\u0440\\u0430\\u0442\\u044c","b9-title":"<p>\\u041f\\u043e\\u0447\\u0435\\u043c\\u0443 \\u043d\\u0430\\u043c \\u0441\\u0442\\u043e\\u0438\\u0442 \\u0434\\u043e\\u0432\\u0435\\u0440\\u044f\\u0442\\u044c?<\\/p>","b9-img-1":"images\\/icon\\/wherefore-1.png","b9-text1":"<p>\\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e\\u0435 \\u043f\\u043e\\u0434\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u043e\\u0431\\u0443\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0441\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u043a\\u043e\\u0432 \\u0437\\u0430 1 \\u0434\\u0435\\u043d\\u044c<\\/p>","b9-img-2":"images\\/icon\\/wherefore-1.png","b9-text2":"<p>\\u041f\\u0440\\u043e\\u0444\\u0435\\u0441\\u0441\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f \\u043f\\u043e\\u0434\\u0434\\u0435\\u0440\\u0436\\u043a\\u0430 \\u0438 \\u0441\\u043e\\u043f\\u0440\\u043e\\u0432\\u043e\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435 (\\u043f\\u0435\\u0440\\u0438\\u043e\\u0434\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0432\\u044b\\u043f\\u0443\\u0441\\u043a \\u043e\\u0431\\u043d\\u043e\\u0432\\u043b\\u0435\\u043d\\u0438\\u0439)<\\/p>","b9-img-3":"images\\/icon\\/wherefore-1.png","b9-text3":"<p>\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u0438 \\u0441\\u043e\\u0445\\u0440\\u0430\\u043d\\u043d\\u043e\\u0441\\u0442\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445, \\u043f\\u043e\\u0434\\u043a\\u0440\\u0435\\u043f\\u043b\\u0435\\u043d\\u043d\\u0430\\u044f \\u043e\\u0444\\u0438\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c<\\/p>","b10-show":"1","b10-title":"<p>\\u0421\\u043a\\u0430\\u0447\\u0430\\u0439\\u0442\\u0435 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u0434\\u0435\\u043c\\u043e-\\u0432\\u0435\\u0440\\u0441\\u0438\\u044e <br \\/> \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u043e\\u0433\\u043e \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f \\u201c\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u201d<\\/p>","b10-img":"images\\/iphone-6-2.png","b10-text":"<p>\\u0423\\u0431\\u0435\\u0434\\u0438\\u0442\\u0435\\u0441\\u044c \\u0432 \\u0443\\u0434\\u043e\\u0431\\u0441\\u0442\\u0432\\u0435 \\u0438 \\u0444\\u0443\\u043d\\u043a\\u0446\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u043e\\u0441\\u0442\\u0438 \\u0438\\u043d\\u0442\\u0435\\u0440\\u0444\\u0435\\u0439\\u0441\\u0430<\\/p>","b10-button-1":"App Store","b10-button-1-url":"#","b10-button-2":"Google play","b10-button-2-url":"#","b11-img":"images\\/sert.png","b11-title":"<p>\\u041d\\u0430\\u0448\\u0438 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u044b<\\/p>","b11-text":"<p>\\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\u00ab\\u041e\\u043f\\u0442\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0430\\u043b\\u0433\\u043e\\u0440\\u0438\\u0442\\u043c\\u044b\\u00bb \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0430 \\u0432 2010 \\u0433\\u043e\\u0434\\u0443 \\u044f\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u043e\\u0444\\u0438\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u043c \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u043c \\u0444\\u0438\\u0440\\u043c\\u044b \\u00ab1\\u0421\\u00bb. <br \\/> <br \\/> \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u044b\\u043c \\u043d\\u0430\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u0435\\u043c \\u0434\\u0435\\u044f\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0441\\u0442\\u0438 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\u044f\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0440\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430, \\u0432\\u043d\\u0435\\u0434\\u0440\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u0441\\u043e\\u043f\\u0440\\u043e\\u0432\\u043e\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435 \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u043e\\u043d\\u043d\\u044b\\u0445 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c \\u043d\\u0430 \\u0431\\u0430\\u0437\\u0435 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u043e\\u0432 \\u0444\\u0438\\u0440\\u043c\\u044b \\u00ab1\\u0421\\u00bb.<\\/p>","b11-img-1c":"images\\/1c.jpg","b11-text-sub":"<p>\\u041f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442 \\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\" \\u0444\\u0438\\u0440\\u043c\\u044b \\"\\u041e\\u043f\\u0442\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 <br \\/> \\u0430\\u043b\\u0433\\u043e\\u0440\\u0438\\u0442\\u043c\\u044b\\" \\u043f\\u043e\\u043b\\u0443\\u0447\\u0438\\u043b \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442 \\"\\u0421\\u043e\\u0432\\u043c\\u0435\\u0441\\u0442\\u0438\\u043c\\u043e! <br \\/> \\u0421\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c 1\\u0421:\\u041f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0435\\"<\\/p>","b11-button":"\\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0430\\u044f \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f"}');
INSERT INTO `lend_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(10, 'xemlab', 0, 'de-DE', 'de', '{"logoFile":"images\\/logo.png","number":"+7 (495) 268 07 49","hedderCall":"Bestellen Up","f-link-text":"\\u041f\\u043e\\u043b\\u0438\\u0442\\u0438\\u043a\\u0430 \\u043a\\u043e\\u043d\\u0444\\u0438\\u0434\\u0435\\u043d\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u043e\\u0441\\u0442\\u0438","f-link":"#","f-copy":"<p>\\u00a9 \\"\\u0427\\u0435\\u043a \\u041b\\u0438\\u0441\\u0442\\" <br \\/> \\u0412\\u0441\\u0435 \\u043f\\u0440\\u0430\\u0432\\u0430 \\u0437\\u0430\\u0449\\u0438\\u0449\\u0435\\u043d\\u044b<\\/p>","c-num":"+7 (495) 268 07 49","c-addres":"<p>\\u041e\\u041e\\u041e \\u00ab\\u041e\\u043f\\u0442\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0430\\u043b\\u0433\\u043e\\u0440\\u0438\\u0442\\u043c\\u044b\\u00bb <br \\/> \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u041e\\u0440\\u0434\\u0436\\u043e\\u043d\\u0438\\u043a\\u0438\\u0434\\u0437\\u0435, \\u0434. 11<\\/p>","c-mail":"info@mail.ru","c-f-title":"\\u041e\\u0441\\u0442\\u0430\\u043b\\u0438\\u0441\\u044c \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b?","c-f-text":"<p>\\u0417\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435 \\u0444\\u043e\\u0440\\u043c\\u0443 \\u043a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u043e\\u0432 \\u0438 \\u043d\\u0430\\u0448 <br \\/> \\u043c\\u0435\\u043d\\u0435\\u0434\\u0436\\u0435\\u0440 \\u0441\\u0432\\u044f\\u0436\\u0438\\u0442\\u0441\\u044f \\u0441 \\u0432\\u0430\\u043c\\u0438, \\u0438 \\u043e\\u0442\\u0432\\u0435\\u0442\\u0438\\u0442 \\u043d\\u0430 \\u0432\\u0441\\u0435 \\u0432\\u0430\\u0448\\u0438 \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b<\\/p>","c-f-btn":"\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e","c-f-error":"\\u0417\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435 \\u0432\\u0441\\u0435 \\u043f\\u043e\\u043b\\u044f!","c-f-compile":"\\u041e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u043e!","b1-title":"<p>\\u0423\\u0441\\u043a\\u043e\\u0440\\u044c\\u0442\\u0435 \\u0440\\u0430\\u0437\\u0432\\u0438\\u0442\\u0438\\u0435 \\u0432\\u0430\\u0448\\u0435\\u0433\\u043e \\u0431\\u0438\\u0437\\u043d\\u0435\\u0441\\u0430 <br \\/> \\u0443\\u043c\\u043d\\u043e\\u0439 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u043e\\u0439 \\u201c\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u201d<\\/p>","b1-img":"images\\/bg\\/ipadhome.png","b1-list-1":"<p><b>\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435:<\\/b> \\u0441\\u0430\\u043c\\u044b\\u0439 \\u0443\\u0434\\u043e\\u0431\\u043d\\u044b\\u0439 \\u0438\\u043d\\u0442\\u0435\\u0440\\u0444\\u0435\\u0439\\u0441 \\u0434\\u043b\\u044f \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u0430;<br \\/> \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b \\u0432 \\u0430\\u0432\\u0442\\u043e\\u043d\\u043e\\u043c\\u043d\\u043e\\u043c \\u0440\\u0435\\u0436\\u0438\\u043c\\u0435<\\/p>","b1-list-2":"<p><b>\\u0415\\u0434\\u0438\\u043d\\u0430\\u044f \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430, \\u0441 \\u0440\\u0430\\u0437\\u0433\\u0440\\u0430\\u043d\\u0438\\u0447\\u0435\\u043d\\u0438\\u0435\\u043c \\u043f\\u0440\\u0430\\u0432 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u0430,<\\/b> \\u0434\\u043b\\u044f \\u0432\\u0441\\u0435\\u0445 \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043b\\u0438\\u0446: \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u043e\\u0432, \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u043e\\u0432 \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0445 \\u0442\\u043e\\u0447\\u0435\\u043a, \\u0440\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0435\\u0439 \\u041e\\u041a\\u041a, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0445 \\u0438 \\u0434\\u0440.<\\/p>","b1-list-3":"<p><b>\\u041f\\u0440\\u043e\\u0437\\u0440\\u0430\\u0447\\u043d\\u043e\\u0441\\u0442\\u044c \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a,<\\/b> \\u0431\\u043b\\u0430\\u0433\\u043e\\u0434\\u0430\\u0440\\u044f \\u0432\\u0435\\u0441\\u043e\\u043c\\u043e\\u043c\\u0443 <br \\/> \\u043d\\u0430\\u0431\\u043e\\u0440\\u0443 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432<\\/p>","b1-f-link-text":"\\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0430\\u044f \\u0434\\u0435\\u043c\\u043e-\\u0432\\u0435\\u0440\\u0441\\u0438\\u044f","b1-f-link":"#","b2-f-link-text":"\\u041a\\u0430\\u043a \\u044d\\u0442\\u043e \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u0442","b2-f-link":"#","b2-title":"<p>\\u041f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c\\u044b \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b, <br \\/> \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0435 \\u0437\\u0430\\u043c\\u0435\\u0434\\u043b\\u044f\\u044e\\u0442 \\u0432\\u0430\\u0448 \\u0431\\u0438\\u0437\\u043d\\u0435\\u0441<\\/p>","b2b1-title":"<p>\\u041d\\u0435\\u0442 \\u0435\\u0434\\u0438\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b <br \\/> \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445<\\/p>","b2b1-text":"<p>\\u041f\\u0440\\u0438\\u0445\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u044c \\u0442\\u0430\\u0431\\u043b\\u0438\\u0446\\u044b Excel, \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0443\\u044e \\u043f\\u043e\\u0447\\u0442\\u0443, \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f, \\u0434\\u0440\\u0443\\u0433\\u0438\\u0435 \\u0441\\u043f\\u043e\\u0441\\u043e\\u0431\\u044b, \\u0447\\u0442\\u043e \\u043e\\u0447\\u0435\\u043d\\u044c \\u043d\\u0435\\u0443\\u0434\\u043e\\u0431\\u043d\\u043e<\\/p>","b2b2-title":"<p>\\u041d\\u0435\\u0442 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 <br \\/> \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432 \\u0437\\u0430 \\u0432\\u044b\\u0431\\u0440\\u0430\\u043d\\u043d\\u044b\\u0439 \\u043f\\u0435\\u0440\\u0438\\u043e\\u0434<\\/p>","b2b2-text":"<p>\\u0411\\u0435\\u0437 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432 \\u0441\\u043b\\u043e\\u0436\\u043d\\u043e \\u043f\\u043e\\u043d\\u044f\\u0442\\u044c \\u0434\\u0438\\u043d\\u0430\\u043c\\u0438\\u043a\\u0443 \\u0440\\u043e\\u0441\\u0442\\u0430, \\u0438\\u0437\\u043c\\u0435\\u043d\\u0435\\u043d\\u0438\\u0439; \\u043f\\u0440\\u0438\\u0445\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0441\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0442\\u044c \\u0438\\u0445 \\u0432\\u0440\\u0443\\u0447\\u043d\\u0443\\u044e, \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u044f \\u0441\\u0442\\u043e\\u0440\\u043e\\u043d\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u044b<\\/p>","b2b3-title":"<p>\\u041d\\u0435\\u0443\\u0434\\u043e\\u0431\\u043d\\u044b\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f <br \\/> \\u0434\\u043b\\u044f \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f<\\/p>","b2b3-text":"<p>\\u041e\\u043f\\u0440\\u043e\\u0441 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u043e\\u0432, \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0435 \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u044e\\u0442 \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f \\u0434\\u043b\\u044f \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a \\u0434\\u0430\\u043b \\u043f\\u043e\\u043d\\u044f\\u0442\\u044c, \\u0447\\u0442\\u043e \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435 \\u043d\\u0435\\u0442 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f, \\u043a\\u043e\\u0442\\u043e\\u0440\\u043e\\u0435 \\u0434\\u0435\\u0439\\u0441\\u0442\\u0432\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e \\u0443\\u0434\\u043e\\u0431\\u043d\\u043e \\u043d\\u0430 \\u043f\\u0440\\u0430\\u043a\\u0442\\u0438\\u043a\\u0435<\\/p>","b2b4-title":"<p>\\u0412\\u044b\\u044f\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c\\u044b <br \\/> \\u0443\\u0441\\u0442\\u0440\\u0430\\u043d\\u044f\\u044e\\u0442\\u0441\\u044f \\u043c\\u0435\\u0434\\u043b\\u0435\\u043d\\u043d\\u043e<\\/p>","b2b4-text":"<p>\\u041e\\u0442\\u0441\\u0443\\u0442\\u0441\\u0442\\u0432\\u0438\\u0435 \\u0446\\u0435\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438, \\u0441\\u043a\\u0430\\u0437\\u044b\\u0432\\u0430\\u0435\\u0442\\u0441\\u044f \\u043d\\u0430 \\u0441\\u043a\\u043e\\u0440\\u043e\\u0441\\u0442\\u0438 \\u043f\\u0435\\u0440\\u0435\\u0434\\u0430\\u0447\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445, \\u0443\\u0441\\u0442\\u0440\\u0430\\u043d\\u0435\\u043d\\u0438\\u0438 \\u0432\\u044b\\u044f\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c; \\u0447\\u0442\\u043e \\u0432 \\u0441\\u0432\\u043e\\u044e \\u043e\\u0447\\u0435\\u0440\\u0435\\u0434\\u044c \\u0437\\u0430\\u043c\\u0435\\u0434\\u043b\\u044f\\u0435\\u0442 \\u0440\\u0430\\u0437\\u0432\\u0438\\u0442\\u0438\\u0435 \\u0431\\u0438\\u0437\\u043d\\u0435\\u0441\\u0430 \\u0432 \\u0446\\u0435\\u043b\\u043e\\u043c<\\/p>","b2b5-title":"<p>\\u0421\\u043f\\u043e\\u0440\\u044b \\u00ab\\u043a\\u0442\\u043e \\u043f\\u0440\\u0430\\u0432\\u2026\\u00bb <br \\/> \\u0432\\u043c\\u0435\\u0441\\u0442\\u043e \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u044f \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c<\\/p>","b2b5-text":"<p>\\u041d\\u0435\\u043f\\u0440\\u043e\\u0437\\u0440\\u0430\\u0447\\u043d\\u043e\\u0441\\u0442\\u044c \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a, \\u043f\\u0440\\u0438\\u0432\\u043e\\u0434\\u0438\\u0442 \\u043a \\u0441\\u043f\\u043e\\u0440\\u0430\\u043c \\u0441\\u0442\\u043e\\u0440\\u043e\\u043d, \\u0431\\u0435\\u0437 \\u0444\\u0430\\u043a\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0433\\u043e \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0435\\u043d\\u0438\\u044f \\u0432\\u044b\\u044f\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c, \\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u0438\\u0442\\u0441\\u044f \\u0441\\u043b\\u043e\\u0436\\u043d\\u043e \\u0443\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u0438\\u0442\\u044c \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043b\\u0438\\u0446<\\/p>","b3-title":"<p>\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\" - \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u044f \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u044f <br \\/> \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430, \\u0438\\u0434\\u0435\\u0430\\u043b\\u044c\\u043d\\u043e \\u043f\\u043e\\u0434\\u0445\\u043e\\u0434\\u0438\\u0442 \\u0434\\u043b\\u044f:<\\/p>","b3-s1-title":"<p>\\u041e\\u0431\\u0449\\u0435\\u043f\\u0438\\u0442<\\/p>","b3-s1-list-1":"{\\"text\\":[\\"\\u0411\\u0430\\u0440\\",\\"\\u0411\\u0438\\u0441\\u0442\\u0440\\u043e\\",\\"\\u0411\\u0443\\u0440\\u0433\\u0435\\u0440\\u043d\\u0430\\u044f\\",\\"\\u0417\\u0430\\u043a\\u0443\\u0441\\u043e\\u0447\\u043d\\u0430\\u044f\\",\\"\\u041a\\u0430\\u0444\\u0435\\",\\"\\u041a\\u043e\\u0444\\u0435\\u0439\\u043d\\u044f\\",\\"\\u041f\\u0430\\u0431\\",\\"\\u041f\\u0438\\u0446\\u0446\\u0435\\u0440\\u0438\\u044f\\",\\"\\u0420\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d\\",\\"\\u0420\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d \\u0431\\/\\u043f\\",\\"\\u0421\\u0442\\u043e\\u043b\\u043e\\u0432\\u0430\\u044f\\",\\"\\u0421\\u0443\\u0448\\u0438-\\u0431\\u0430\\u0440\\",\\"\\u0425\\u0438\\u043d\\u043a\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\\",\\"\\u0427\\u0430\\u0439\\u043d\\u0430\\u044f\\",\\"\\u0427\\u0430\\u0439\\u0445\\u0430\\u043d\\u0430\\"]}","b3-s2-title":"<p>\\u041e\\u0431\\u0440\\u0430\\u0437\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435<\\/p>","b3-s2":"{\\"text\\":[\\"\\u0412\\u0423\\u0417\\",\\"\\u0414\\u0435\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0434\\",\\"\\u041a\\u043e\\u043b\\u043b\\u0435\\u0434\\u0436\\",\\"\\u041f\\u0422\\u0423\\",\\"\\u0428\\u043a\\u043e\\u043b\\u0430\\"]}","b3-s3-title":"<p>\\u041c\\u0443\\u043d\\u0438\\u0446\\u0438\\u043f\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 <br \\/> \\u043f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u044f<\\/p>","b3-s3":"{\\"text\\":[\\"\\u0410\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u043d\\u044b\\u0439 \\u043f\\u0430\\u0440\\u043a\\",\\"\\u0410\\u044d\\u0440\\u043e\\u043f\\u043e\\u0440\\u0442\\",\\"\\u0412\\u043e\\u043a\\u0437\\u0430\\u043b\\",\\"\\u0414\\u0435\\u043f\\u043e \\u043c\\u0435\\u0442\\u0440\\u043e\\u043f\\u043e\\u043b\\u0438\\u0442\\u0435\\u043d\\u0430\\",\\"\\u0422\\u0440\\u0430\\u043c\\u0432\\u0430\\u0439\\u043d\\u043e\\u0435 \\u0434\\u0435\\u043f\\u043e\\",\\"\\u0422\\u0440\\u043e\\u043b\\u043b\\u0435\\u0439\\u0431\\u0443\\u0441\\u043d\\u044b\\u0439 \\u043f\\u0430\\u0440\\"]}","b3-s4-title":"<p>\\u041a\\u0440\\u0430\\u0441\\u043e\\u0442\\u0430 <br \\/> \\u0438 \\u0437\\u0434\\u043e\\u0440\\u043e\\u0432\\u044c\\u0435<\\/p>","b3-s4":"{\\"text\\":[\\"\\u0411\\u0430\\u043d\\u044f\\",\\"\\u041c\\u0430\\u043d\\u0438\\u043a\\u044e\\u0440\\u043d\\u0430\\u044f\\",\\"\\u041f\\u0430\\u0440\\u0438\\u043a\\u043c\\u0430\\u0445\\u0435\\u0440\\u0441\\u043a\\u0430\\u044f\\",\\"\\u0421\\u0430\\u043b\\u043e\\u043d \\u043a\\u0440\\u0430\\u0441\\u043e\\u0442\\u044b\\",\\"\\u0421\\u043f\\u043e\\u0440\\u0442\\u0438\\u0432\\u043d\\u0430\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u044f\\",\\"\\u0421\\u0442\\u043e\\u043c\\u0430\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u043a\\u0430\\u0431\\u0438\\u043d\\u0435\\u0442\\",\\"\\u0424\\u0438\\u0442\\u043d\\u0435\\u0441-\\u043a\\u043b\\u0443\\u0431\\",\\"\\u0427\\u0430\\u0441\\u0442\\u043d\\u0430\\u044f \\u043a\\u043b\\u0438\\u043d\\u0438\\u043a\\u0430\\",\\"SPA-\\u0441\\u0430\\u043b\\u043e\\u043d\\"]}","b3-s5-title":"<p>\\u0420\\u0430\\u0437\\u0432\\u043b\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f<\\/p>","b3-s5":"{\\"text\\":[\\"\\u0410\\u043a\\u0432\\u0430\\u043f\\u0430\\u0440\\u043a\\",\\"\\u0411\\u0438\\u043b\\u044c\\u044f\\u0440\\u0434\\u043d\\u0430\\u044f\\",\\"\\u0411\\u043e\\u0443\\u043b\\u0438\\u043d\\u0433\\",\\"\\u0412\\u044b\\u0441\\u0442\\u0430\\u0432\\u043e\\u0447\\u043d\\u044b\\u0439 \\u043a\\u043e\\u043c\\u043f\\u043b\\u0435\\u043a\\u0441\\",\\"\\u0417\\u043e\\u043e\\u043f\\u0430\\u0440\\u043a\\",\\"\\u041a\\u0438\\u043d\\u043e\\u0442\\u0435\\u0430\\u0442\\u0440\\",\\"\\u041a\\u043e\\u043d\\u0446\\u0435\\u0440\\u0442\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043b\\",\\"\\u041c\\u0443\\u0437\\u0435\\u0439\\",\\"\\u041d\\u043e\\u0447\\u043d\\u043e\\u0439 \\u043a\\u043b\\u0443\\u0431\\",\\"\\u041f\\u0430\\u0440\\u043a \\u0430\\u0442\\u0442\\u0440\\u0430\\u043a\\u0446\\u0438\\u043e\\u043d\\u043e\\u0432\\",\\"\\u0421\\u043f\\u043e\\u0440\\u0442\\u0438\\u0432\\u043d\\u0430\\u044f \\u0430\\u0440\\u0435\\u043d\\u0430\\",\\"\\u0421\\u043f\\u043e\\u0440\\u0442\\u0438\\u0432\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043b\\",\\"\\u0422\\u0435\\u0430\\u0442\\u0440\\"]}","b3-s6-title":"<p>\\u041f\\u043e\\u0432\\u0441\\u0435\\u0434\\u043d\\u0435\\u0432\\u043d\\u044b\\u0435 <br \\/> \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438<\\/p>","b3-s6":"{\\"text\\":[\\"\\u0410\\u0432\\u0442\\u043e\\u0437\\u0430\\u043f\\u0440\\u0430\\u0432\\u043a\\u0430\\",\\"\\u0410\\u0432\\u0442\\u043e\\u043c\\u043e\\u0439\\u043a\\u0430\\",\\"\\u0411\\u0430\\u043d\\u043a\\",\\"\\u0421\\u043b\\u0443\\u0436\\u0431\\u0430 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0438\\",\\"\\u0421\\u0422\\u041e\\",\\"\\u0422\\u0430\\u043a\\u0441\\u0438\\",\\"\\u0428\\u0438\\u043d\\u043e\\u043c\\u043e\\u043d\\u0442\\u0430\\u0436\\"]}","b3-s7-title":"<p>\\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u044b<\\/p>","b3-s7":"{\\"text\\":[\\"\\u0413\\u0438\\u043f\\u0435\\u0440\\u043c\\u0430\\u0440\\u043a\\u0435\\u0442 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u0438\\u043a\\u0438\\",\\"\\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\\\\\"\\u0432\\u043e\\u0437\\u043b\\u0435 \\u0434\\u043e\\u043c\\u0430\\\\\\"\\",\\"\\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\u043e\\u0434\\u0435\\u0436\\u0434\\u044b \\u0438 \\u043e\\u0431\\u0443\\u0432\\u0438\\",\\"\\u041f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u043e\\u0432\\u044b\\u0439 \\u0433\\u0438\\u043f\\u0435\\u0440\\u043c\\u0430\\u0440\\u043a\\u0435\\u0442\\",\\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u0438\\u0437\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0435 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u044b\\",\\"\\u0421\\u0442\\u0440\\u043e\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0433\\u0438\\u043f\\u0435\\u0440\\u043c\\u0430\\u0440\\u043a\\u0435\\u0442\\"]}","b4-title":"<p>\\u0415\\u0434\\u0438\\u043d\\u0430\\u044f \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f <br \\/> \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430, \\u0438\\u0437 \\u0442\\u0440\\u0435\\u0445 \\u043a\\u043e\\u043c\\u043f\\u043e\\u043d\\u0435\\u043d\\u0442\\u043e\\u0432, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0435 1\\u0421<\\/p>","shemFile":"images\\/system.png","b4b1-title":"<p>\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442<\\/p>","b4b1-text":"<p>\\u0424\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432 \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b; \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u044f \\u043e\\u0431\\u0440\\u0430\\u0442\\u043d\\u043e\\u0439 \\u0441\\u0432\\u044f\\u0437\\u0438 \\u043c\\u0435\\u0436\\u0434\\u0443 \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438 \\u0440\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d\\u043e\\u0432 \\u0438 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438<\\/p>","b4b2-title":"<p>\\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f \\u0431\\u0430\\u0437\\u0430,<br \\/> \\u0440\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u043d\\u043d\\u0430\\u044f \\u043d\\u0430 \\u043f\\u043b\\u0430\\u0442\\u0444\\u043e\\u0440\\u043c\\u0435 \\u00ab1\\u0421:\\u041f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u044f\\u00bb<\\/p>","b4b2-text":"<p>\\u0412 \\u043d\\u0435\\u0439 \\u0441\\u043e\\u0431\\u0438\\u0440\\u0430\\u0435\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u043e \\u043f\\u0440\\u043e\\u0432\\u0435\\u0434\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0430\\u0445 \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0445 \\u0442\\u043e\\u0447\\u0435\\u043a \\u0438 \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u0443\\u044e\\u0442\\u0441\\u044f \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0435 \\u043e\\u0442\\u0447\\u0435\\u0442\\u044b<\\/p>","b4b3-title":"<p>\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435 \\u00ab\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u00bb<\\/p>","b4b3-text":"<p>\\u0421 \\u0435\\u0433\\u043e \\u043f\\u043e\\u043c\\u043e\\u0449\\u044c\\u044e \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u044f\\u0435\\u0442 \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u0443\\u044e \\u0442\\u043e\\u0447\\u043a\\u0443 \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u0435\\u0442 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438 \\u0432 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u0443\\u044e \\u0431\\u0430\\u0437\\u0443<\\/p>","b5-title":"<p>\\u0421\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u043f\\u043e\\u0437\\u0432\\u043e\\u043b\\u044f\\u0435\\u0442 \\u0430\\u0432\\u0442\\u043e\\u043c\\u0430\\u0442\\u0438\\u0437\\u0438\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c <br \\/> \\u0441\\u043b\\u0435\\u0434\\u0443\\u044e\\u0449\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0446\\u0435\\u0441\\u0441\\u044b:<\\/p>","screen-1":"images\\/iphone-6.png","screen-2":"images\\/screen-1.png","screen-3":"images\\/screen-2.png","b5-list":"{\\"text\\":[\\"\\u041f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u043e\\u0432 \\u0440\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c\\u0438, \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438\\",\\"\\u041f\\u0440\\u043e\\u0432\\u0435\\u0434\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440\\u0430\\u043c\\u0438 \\u043f\\u043e \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u0430\\u043c\\",\\"\\u041a\\u043e\\u043d\\u0441\\u043e\\u043b\\u0438\\u0434\\u0430\\u0446\\u0438\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0438 \\u043e \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u0430\\u0445 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438 \\u0432 \\u0435\\u0434\\u0438\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435\\",\\"\\u0420\\u0430\\u0437\\u043d\\u043e\\u0441\\u0442\\u043e\\u0440\\u043e\\u043d\\u043d\\u0438\\u0439 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0437 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a (\\u0434\\u0438\\u043d\\u0430\\u043c\\u0438\\u043a\\u0430 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432,                         \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433 \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0445, \\u043d\\u0430\\u0438\\u0431\\u043e\\u043b\\u0435\\u0435 \\u0447\\u0430\\u0441\\u0442\\u044b\\u0435 \\u043f\\u0440\\u0438\\u0447\\u0438\\u043d\\u044b                         \\u043f\\u043b\\u043e\\u0445\\u0438\\u0445 \\u043e\\u0446\\u0435\\u043d\\u043e\\u043a \\u0438 \\u043c\\u043d\\u043e\\u0433\\u043e\\u0435 \\u0434\\u0440\\u0443\\u0433\\u043e\\u0435)\\",\\"\\u041f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0438 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044c \\u043c\\u0435\\u0440\\u043e\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0439, \\u043d\\u0430\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043d\\u0430 \\u0443\\u0441\\u0442\\u0440\\u0430\\u043d\\u0435\\u043d\\u0438\\u0435                         \\u043f\\u0440\\u0438\\u0447\\u0438\\u043d \\u00ab\\u043e\\u0442\\u0440\\u0438\\u0446\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445\\u00bb \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a\\",\\"\\u041e\\u0431\\u0440\\u0430\\u0442\\u043d\\u0430\\u044f \\u0441\\u0432\\u044f\\u0437\\u044c \\u043c\\u0435\\u0436\\u0434\\u0443 \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438 \\u0438 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438: \\u043f\\u0440\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u0435                         \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0432, \\u0441 \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c\\u044e \\u0434\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c \\u0444\\u043e\\u0442\\u043e, \\u043a\\u0430\\u043a \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435                         \\u0444\\u0430\\u043a\\u0442\\u0430 \\u043d\\u0430\\u0440\\u0443\\u0448\\u0435\\u043d\\u0438\\u0439\\",\\"\\u0420\\u0430\\u0437\\u0433\\u0440\\u0430\\u043d\\u0438\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0443\\u0440\\u043e\\u0432\\u043d\\u0435\\u0439 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u0430 \\u043a \\u0434\\u0430\\u043d\\u043d\\u044b\\u043c \\u0434\\u043b\\u044f \\u0440\\u0430\\u0437\\u043b\\u0438\\u0447\\u043d\\u044b\\u0445 \\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                         (\\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u044b \\u0443\\u0440\\u043e\\u0432\\u043d\\u0438: \\u0414\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440, \\u0410\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440, \\u0422\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0439)\\",\\"\\u0411\\u044b\\u0441\\u0442\\u0440\\u044b\\u0439, \\u0443\\u0434\\u043e\\u0431\\u043d\\u044b\\u0439 \\u043e\\u0431\\u043c\\u0435\\u043d \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0435\\u0439 \\u043c\\u0435\\u0436\\u0434\\u0443 \\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u044f\\u043c\\u0438 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b                         (\\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 \\u043f\\u0440\\u043e\\u0432\\u0435\\u043b \\u0430\\u0443\\u0434\\u0438\\u0442, \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u043b \\u0434\\u0430\\u043d\\u043d\\u044b\\u0435 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438 \\u0432 \\u0431\\u0430\\u0437\\u0443, \\u0440\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0438                         \\u043f\\u043e\\u043b\\u0443\\u0447\\u0438\\u043b\\u0438 \\u043e\\u0442\\u0447\\u0435\\u0442\\u044b), \\u043d\\u0438\\u043a\\u0430\\u043a\\u0438\\u0445 \\u043f\\u0435\\u0440\\u0435\\u0441\\u044b\\u043b\\u043e\\u043a \\u043f\\u043e \\u043f\\u043e\\u0447\\u0442\\u0435, \\u0440\\u0443\\u0447\\u043d\\u043e\\u0433\\u043e \\u0432\\u0432\\u043e\\u0434\\u0430 \\u0438 \\u043f\\u0440\\u043e\\u0447\\u0435\\u0433\\u043e.\\",\\"\\u0418\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u00ab\\u0431\\u0435\\u0437\\u0431\\u0443\\u043c\\u0430\\u0436\\u043d\\u043e\\u0439\\u00bb \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u0438 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430\\"]}","uznB":"\\u0423\\u0437\\u043d\\u0430\\u0442\\u044c \\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0439","uznBl":"#","b6-title":"<p>\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0435 \\u043e\\u0442\\u0447\\u0435\\u0442\\u044b<\\/p>","b6-list-text-1":"\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-1":"images\\/slider\\/Layer-0.png","b6-list-text-2":"2 \\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-2":"images\\/slider\\/Layer-0.png","b6-list-text-3":"\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-3":"images\\/slider\\/Layer-0.png","b6-list-text-4":"3 \\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-4":"images\\/shem.png","b6-list-text-5":"4  \\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-5":"images\\/slider\\/Layer-0.png","b7-title":"<p>\\u041a\\u0430\\u043a \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u0442 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430?<\\/p>","b7-text":"<p>\\u0426\\u0438\\u043a\\u043b \\u0432\\u0437\\u0430\\u0438\\u043c\\u043e\\u0434\\u0435\\u0439\\u0441\\u0442\\u0432\\u0438\\u044f \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b \\u0438 <br \\/> \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0439<\\/p>","b7b3-title":"<p>\\u0421\\u0438\\u043d\\u0445\\u0440\\u043e\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u044f<\\/p>","planFile":"images\\/bg\\/how-do-CENTR.png","b7-list-text":"{\\"text\\":[\\"1 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"2 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"3 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"4 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"5 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\"]}","b7b1-title":"<p>\\u041f\\u0440\\u043e\\u0432\\u0435\\u0434\\u0435\\u043d\\u0438\\u0435 <br \\/> \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0432<\\/p>","b7b1l1-title":"\\u041c\\u041e\\u0421\\u041a\\u0412\\u0410","b7b1l1-list":"{\\"text\\":[\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 1\\",\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 2\\"]}","b7b1l2-title":"\\u0420\\u0415\\u0413\\u0418\\u041e\\u041d\\u042b","b7b1l2-list":"{\\"text\\":[\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 3\\",\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 4\\",\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 5\\"]}","b7b2-title":"<p>\\u041f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 <br \\/> \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0432<\\/p>","b7b2l1-list":"{\\"text\\":[\\"\\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0438 \\u041e\\u041a\\u041a\\",\\"\\u0422\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0435\\",\\"\\u0410\\u043a\\u0446\\u0438\\u043e\\u043d\\u0435\\u0440\\u044b\\"]}","b8-title":"<p>\\u041d\\u0415 \\u0417\\u041d\\u0410\\u0415\\u0422\\u0415 \\u041a\\u0410\\u041a\\u041e\\u0419 \\u0422\\u0410\\u0420\\u0418\\u0424 \\u0412\\u042b\\u0411\\u0420\\u0410\\u0422\\u042c?<\\/p>","b8-text":"<p>\\u041c\\u044b \\u0432\\u0430\\u043c \\u043f\\u043e\\u043c\\u043e\\u0436\\u0435\\u043c!<\\/p>","b8-button":"\\u041f\\u043e\\u0434\\u043e\\u0431\\u0440\\u0430\\u0442\\u044c","b9-title":"<p>\\u041f\\u043e\\u0447\\u0435\\u043c\\u0443 \\u043d\\u0430\\u043c \\u0441\\u0442\\u043e\\u0438\\u0442 \\u0434\\u043e\\u0432\\u0435\\u0440\\u044f\\u0442\\u044c?<\\/p>","b9-img-1":"images\\/icon\\/wherefore-1.png","b9-text1":"<p>\\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e\\u0435 \\u043f\\u043e\\u0434\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u043e\\u0431\\u0443\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0441\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u043a\\u043e\\u0432 \\u0437\\u0430 1 \\u0434\\u0435\\u043d\\u044c<\\/p>","b9-img-2":"images\\/icon\\/wherefore-1.png","b9-text2":"<p>\\u041f\\u0440\\u043e\\u0444\\u0435\\u0441\\u0441\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f \\u043f\\u043e\\u0434\\u0434\\u0435\\u0440\\u0436\\u043a\\u0430 \\u0438 \\u0441\\u043e\\u043f\\u0440\\u043e\\u0432\\u043e\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435 (\\u043f\\u0435\\u0440\\u0438\\u043e\\u0434\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0432\\u044b\\u043f\\u0443\\u0441\\u043a \\u043e\\u0431\\u043d\\u043e\\u0432\\u043b\\u0435\\u043d\\u0438\\u0439)<\\/p>","b9-img-3":"images\\/icon\\/wherefore-1.png","b9-text3":"<p>\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u0438 \\u0441\\u043e\\u0445\\u0440\\u0430\\u043d\\u043d\\u043e\\u0441\\u0442\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445, \\u043f\\u043e\\u0434\\u043a\\u0440\\u0435\\u043f\\u043b\\u0435\\u043d\\u043d\\u0430\\u044f \\u043e\\u0444\\u0438\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c<\\/p>","b10-show":"1","b10-title":"<p>\\u0421\\u043a\\u0430\\u0447\\u0430\\u0439\\u0442\\u0435 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u0434\\u0435\\u043c\\u043e-\\u0432\\u0435\\u0440\\u0441\\u0438\\u044e <br \\/> \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u043e\\u0433\\u043e \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f \\u201c\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u201d<\\/p>","b10-img":"images\\/iphone-6-2.png","b10-text":"<p>\\u0423\\u0431\\u0435\\u0434\\u0438\\u0442\\u0435\\u0441\\u044c \\u0432 \\u0443\\u0434\\u043e\\u0431\\u0441\\u0442\\u0432\\u0435 \\u0438 \\u0444\\u0443\\u043d\\u043a\\u0446\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u043e\\u0441\\u0442\\u0438 \\u0438\\u043d\\u0442\\u0435\\u0440\\u0444\\u0435\\u0439\\u0441\\u0430<\\/p>","b10-button-1":"App Store","b10-button-1-url":"#","b10-button-2":"Google play","b10-button-2-url":"#","b11-img":"images\\/sert.png","b11-title":"<p>\\u041d\\u0430\\u0448\\u0438 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u044b<\\/p>","b11-text":"<p>\\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\u00ab\\u041e\\u043f\\u0442\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0430\\u043b\\u0433\\u043e\\u0440\\u0438\\u0442\\u043c\\u044b\\u00bb \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0430 \\u0432 2010 \\u0433\\u043e\\u0434\\u0443 \\u044f\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u043e\\u0444\\u0438\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u043c \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u043c \\u0444\\u0438\\u0440\\u043c\\u044b \\u00ab1\\u0421\\u00bb. <br \\/> <br \\/> \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u044b\\u043c \\u043d\\u0430\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u0435\\u043c \\u0434\\u0435\\u044f\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0441\\u0442\\u0438 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\u044f\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0440\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430, \\u0432\\u043d\\u0435\\u0434\\u0440\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u0441\\u043e\\u043f\\u0440\\u043e\\u0432\\u043e\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435 \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u043e\\u043d\\u043d\\u044b\\u0445 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c \\u043d\\u0430 \\u0431\\u0430\\u0437\\u0435 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u043e\\u0432 \\u0444\\u0438\\u0440\\u043c\\u044b \\u00ab1\\u0421\\u00bb.<\\/p>","b11-img-1c":"images\\/1c.jpg","b11-text-sub":"<p>\\u041f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442 \\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\" \\u0444\\u0438\\u0440\\u043c\\u044b \\"\\u041e\\u043f\\u0442\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 <br \\/> \\u0430\\u043b\\u0433\\u043e\\u0440\\u0438\\u0442\\u043c\\u044b\\" \\u043f\\u043e\\u043b\\u0443\\u0447\\u0438\\u043b \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442 \\"\\u0421\\u043e\\u0432\\u043c\\u0435\\u0441\\u0442\\u0438\\u043c\\u043e! <br \\/> \\u0421\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c 1\\u0421:\\u041f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0435\\"<\\/p>","b11-button":"\\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0430\\u044f \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f"}');
INSERT INTO `lend_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(11, 'xemlab', 0, '1', 'en', '{"logoFile":"images\\/logo.png","number":"+7 (495) 268 07 49","hedderCall":"Request a call","f-link-text":"\\u041f\\u043e\\u043b\\u0438\\u0442\\u0438\\u043a\\u0430 \\u043a\\u043e\\u043d\\u0444\\u0438\\u0434\\u0435\\u043d\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u043e\\u0441\\u0442\\u0438","f-link":"#","f-copy":"<p>\\u00a9 \\"\\u0427\\u0435\\u043a \\u041b\\u0438\\u0441\\u0442\\" <br \\/> \\u0412\\u0441\\u0435 \\u043f\\u0440\\u0430\\u0432\\u0430 \\u0437\\u0430\\u0449\\u0438\\u0449\\u0435\\u043d\\u044b<\\/p>","c-num":"+7 (495) 268 07 49","c-addres":"<p>\\u041e\\u041e\\u041e \\u00ab\\u041e\\u043f\\u0442\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0430\\u043b\\u0433\\u043e\\u0440\\u0438\\u0442\\u043c\\u044b\\u00bb <br \\/> \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u041e\\u0440\\u0434\\u0436\\u043e\\u043d\\u0438\\u043a\\u0438\\u0434\\u0437\\u0435, \\u0434. 11<\\/p>","c-mail":"info@mail.ru","c-f-title":"\\u041e\\u0441\\u0442\\u0430\\u043b\\u0438\\u0441\\u044c \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b?","c-f-text":"<p>\\u0417\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435 \\u0444\\u043e\\u0440\\u043c\\u0443 \\u043a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u043e\\u0432 \\u0438 \\u043d\\u0430\\u0448 <br \\/> \\u043c\\u0435\\u043d\\u0435\\u0434\\u0436\\u0435\\u0440 \\u0441\\u0432\\u044f\\u0436\\u0438\\u0442\\u0441\\u044f \\u0441 \\u0432\\u0430\\u043c\\u0438, \\u0438 \\u043e\\u0442\\u0432\\u0435\\u0442\\u0438\\u0442 \\u043d\\u0430 \\u0432\\u0441\\u0435 \\u0432\\u0430\\u0448\\u0438 \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b<\\/p>","c-f-btn":"\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e","c-f-error":"\\u0417\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435 \\u0432\\u0441\\u0435 \\u043f\\u043e\\u043b\\u044f!","c-f-compile":"\\u041e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u043e!","b1-title":"<p>\\u0423\\u0441\\u043a\\u043e\\u0440\\u044c\\u0442\\u0435 \\u0440\\u0430\\u0437\\u0432\\u0438\\u0442\\u0438\\u0435 \\u0432\\u0430\\u0448\\u0435\\u0433\\u043e \\u0431\\u0438\\u0437\\u043d\\u0435\\u0441\\u0430 <br \\/> \\u0443\\u043c\\u043d\\u043e\\u0439 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u043e\\u0439 \\u201c\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u201d<\\/p>","b1-img":"images\\/bg\\/ipadhome.png","b1-list-1":"<p><b>\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435:<\\/b> \\u0441\\u0430\\u043c\\u044b\\u0439 \\u0443\\u0434\\u043e\\u0431\\u043d\\u044b\\u0439 \\u0438\\u043d\\u0442\\u0435\\u0440\\u0444\\u0435\\u0439\\u0441 \\u0434\\u043b\\u044f \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u0430;<br \\/> \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b \\u0432 \\u0430\\u0432\\u0442\\u043e\\u043d\\u043e\\u043c\\u043d\\u043e\\u043c \\u0440\\u0435\\u0436\\u0438\\u043c\\u0435<\\/p>","b1-list-2":"<p><b>\\u0415\\u0434\\u0438\\u043d\\u0430\\u044f \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430, \\u0441 \\u0440\\u0430\\u0437\\u0433\\u0440\\u0430\\u043d\\u0438\\u0447\\u0435\\u043d\\u0438\\u0435\\u043c \\u043f\\u0440\\u0430\\u0432 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u0430,<\\/b> \\u0434\\u043b\\u044f \\u0432\\u0441\\u0435\\u0445 \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043b\\u0438\\u0446: \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u043e\\u0432, \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u043e\\u0432 \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0445 \\u0442\\u043e\\u0447\\u0435\\u043a, \\u0440\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0435\\u0439 \\u041e\\u041a\\u041a, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0445 \\u0438 \\u0434\\u0440.<\\/p>","b1-list-3":"<p><b>\\u041f\\u0440\\u043e\\u0437\\u0440\\u0430\\u0447\\u043d\\u043e\\u0441\\u0442\\u044c \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a,<\\/b> \\u0431\\u043b\\u0430\\u0433\\u043e\\u0434\\u0430\\u0440\\u044f \\u0432\\u0435\\u0441\\u043e\\u043c\\u043e\\u043c\\u0443 <br \\/> \\u043d\\u0430\\u0431\\u043e\\u0440\\u0443 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432<\\/p>","b1-f-link-text":"\\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0430\\u044f \\u0434\\u0435\\u043c\\u043e-\\u0432\\u0435\\u0440\\u0441\\u0438\\u044f","b1-f-link":"#","b2-f-link-text":"\\u041a\\u0430\\u043a \\u044d\\u0442\\u043e \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u0442","b2-f-link":"#","b2-title":"<p>\\u041f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c\\u044b \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b, <br \\/> \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0435 \\u0437\\u0430\\u043c\\u0435\\u0434\\u043b\\u044f\\u044e\\u0442 \\u0432\\u0430\\u0448 \\u0431\\u0438\\u0437\\u043d\\u0435\\u0441<\\/p>","b2b1-title":"<p>\\u041d\\u0435\\u0442 \\u0435\\u0434\\u0438\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b <br \\/> \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445<\\/p>","b2b1-text":"<p>\\u041f\\u0440\\u0438\\u0445\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u044c \\u0442\\u0430\\u0431\\u043b\\u0438\\u0446\\u044b Excel, \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0443\\u044e \\u043f\\u043e\\u0447\\u0442\\u0443, \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f, \\u0434\\u0440\\u0443\\u0433\\u0438\\u0435 \\u0441\\u043f\\u043e\\u0441\\u043e\\u0431\\u044b, \\u0447\\u0442\\u043e \\u043e\\u0447\\u0435\\u043d\\u044c \\u043d\\u0435\\u0443\\u0434\\u043e\\u0431\\u043d\\u043e<\\/p>","b2b2-title":"<p>\\u041d\\u0435\\u0442 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 <br \\/> \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432 \\u0437\\u0430 \\u0432\\u044b\\u0431\\u0440\\u0430\\u043d\\u043d\\u044b\\u0439 \\u043f\\u0435\\u0440\\u0438\\u043e\\u0434<\\/p>","b2b2-text":"<p>\\u0411\\u0435\\u0437 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432 \\u0441\\u043b\\u043e\\u0436\\u043d\\u043e \\u043f\\u043e\\u043d\\u044f\\u0442\\u044c \\u0434\\u0438\\u043d\\u0430\\u043c\\u0438\\u043a\\u0443 \\u0440\\u043e\\u0441\\u0442\\u0430, \\u0438\\u0437\\u043c\\u0435\\u043d\\u0435\\u043d\\u0438\\u0439; \\u043f\\u0440\\u0438\\u0445\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0441\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0442\\u044c \\u0438\\u0445 \\u0432\\u0440\\u0443\\u0447\\u043d\\u0443\\u044e, \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u044f \\u0441\\u0442\\u043e\\u0440\\u043e\\u043d\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u044b<\\/p>","b2b3-title":"<p>\\u041d\\u0435\\u0443\\u0434\\u043e\\u0431\\u043d\\u044b\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f <br \\/> \\u0434\\u043b\\u044f \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f<\\/p>","b2b3-text":"<p>\\u041e\\u043f\\u0440\\u043e\\u0441 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u043e\\u0432, \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0435 \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u044e\\u0442 \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f \\u0434\\u043b\\u044f \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a \\u0434\\u0430\\u043b \\u043f\\u043e\\u043d\\u044f\\u0442\\u044c, \\u0447\\u0442\\u043e \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435 \\u043d\\u0435\\u0442 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f, \\u043a\\u043e\\u0442\\u043e\\u0440\\u043e\\u0435 \\u0434\\u0435\\u0439\\u0441\\u0442\\u0432\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e \\u0443\\u0434\\u043e\\u0431\\u043d\\u043e \\u043d\\u0430 \\u043f\\u0440\\u0430\\u043a\\u0442\\u0438\\u043a\\u0435<\\/p>","b2b4-title":"<p>\\u0412\\u044b\\u044f\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c\\u044b <br \\/> \\u0443\\u0441\\u0442\\u0440\\u0430\\u043d\\u044f\\u044e\\u0442\\u0441\\u044f \\u043c\\u0435\\u0434\\u043b\\u0435\\u043d\\u043d\\u043e<\\/p>","b2b4-text":"<p>\\u041e\\u0442\\u0441\\u0443\\u0442\\u0441\\u0442\\u0432\\u0438\\u0435 \\u0446\\u0435\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438, \\u0441\\u043a\\u0430\\u0437\\u044b\\u0432\\u0430\\u0435\\u0442\\u0441\\u044f \\u043d\\u0430 \\u0441\\u043a\\u043e\\u0440\\u043e\\u0441\\u0442\\u0438 \\u043f\\u0435\\u0440\\u0435\\u0434\\u0430\\u0447\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445, \\u0443\\u0441\\u0442\\u0440\\u0430\\u043d\\u0435\\u043d\\u0438\\u0438 \\u0432\\u044b\\u044f\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c; \\u0447\\u0442\\u043e \\u0432 \\u0441\\u0432\\u043e\\u044e \\u043e\\u0447\\u0435\\u0440\\u0435\\u0434\\u044c \\u0437\\u0430\\u043c\\u0435\\u0434\\u043b\\u044f\\u0435\\u0442 \\u0440\\u0430\\u0437\\u0432\\u0438\\u0442\\u0438\\u0435 \\u0431\\u0438\\u0437\\u043d\\u0435\\u0441\\u0430 \\u0432 \\u0446\\u0435\\u043b\\u043e\\u043c<\\/p>","b2b5-title":"<p>\\u0421\\u043f\\u043e\\u0440\\u044b \\u00ab\\u043a\\u0442\\u043e \\u043f\\u0440\\u0430\\u0432\\u2026\\u00bb <br \\/> \\u0432\\u043c\\u0435\\u0441\\u0442\\u043e \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u044f \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c<\\/p>","b2b5-text":"<p>\\u041d\\u0435\\u043f\\u0440\\u043e\\u0437\\u0440\\u0430\\u0447\\u043d\\u043e\\u0441\\u0442\\u044c \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a, \\u043f\\u0440\\u0438\\u0432\\u043e\\u0434\\u0438\\u0442 \\u043a \\u0441\\u043f\\u043e\\u0440\\u0430\\u043c \\u0441\\u0442\\u043e\\u0440\\u043e\\u043d, \\u0431\\u0435\\u0437 \\u0444\\u0430\\u043a\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0433\\u043e \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0435\\u043d\\u0438\\u044f \\u0432\\u044b\\u044f\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c, \\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u0438\\u0442\\u0441\\u044f \\u0441\\u043b\\u043e\\u0436\\u043d\\u043e \\u0443\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u0438\\u0442\\u044c \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043b\\u0438\\u0446<\\/p>","b3-title":"<p>\\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\" - \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u044f \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u044f <br \\/> \\u043f\\u0440\\u043e\\u0431\\u043b\\u0435\\u043c \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430, \\u0438\\u0434\\u0435\\u0430\\u043b\\u044c\\u043d\\u043e \\u043f\\u043e\\u0434\\u0445\\u043e\\u0434\\u0438\\u0442 \\u0434\\u043b\\u044f:<\\/p>","b3-s1-title":"<p>\\u041e\\u0431\\u0449\\u0435\\u043f\\u0438\\u0442<\\/p>","b3-s1-list-1":"{\\"text\\":[\\"\\u0411\\u0430\\u0440\\",\\"\\u0411\\u0438\\u0441\\u0442\\u0440\\u043e\\",\\"\\u0411\\u0443\\u0440\\u0433\\u0435\\u0440\\u043d\\u0430\\u044f\\",\\"\\u0417\\u0430\\u043a\\u0443\\u0441\\u043e\\u0447\\u043d\\u0430\\u044f\\",\\"\\u041a\\u0430\\u0444\\u0435\\",\\"\\u041a\\u043e\\u0444\\u0435\\u0439\\u043d\\u044f\\",\\"\\u041f\\u0430\\u0431\\",\\"\\u041f\\u0438\\u0446\\u0446\\u0435\\u0440\\u0438\\u044f\\",\\"\\u0420\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d\\",\\"\\u0420\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d \\u0431\\/\\u043f\\",\\"\\u0421\\u0442\\u043e\\u043b\\u043e\\u0432\\u0430\\u044f\\",\\"\\u0421\\u0443\\u0448\\u0438-\\u0431\\u0430\\u0440\\",\\"\\u0425\\u0438\\u043d\\u043a\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\\",\\"\\u0427\\u0430\\u0439\\u043d\\u0430\\u044f\\",\\"\\u0427\\u0430\\u0439\\u0445\\u0430\\u043d\\u0430\\"]}","b3-s2-title":"<p>\\u041e\\u0431\\u0440\\u0430\\u0437\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435<\\/p>","b3-s2":"{\\"text\\":[\\"\\u0412\\u0423\\u0417\\",\\"\\u0414\\u0435\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0434\\",\\"\\u041a\\u043e\\u043b\\u043b\\u0435\\u0434\\u0436\\",\\"\\u041f\\u0422\\u0423\\",\\"\\u0428\\u043a\\u043e\\u043b\\u0430\\"]}","b3-s3-title":"<p>\\u041c\\u0443\\u043d\\u0438\\u0446\\u0438\\u043f\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 <br \\/> \\u043f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u044f<\\/p>","b3-s3":"{\\"text\\":[\\"\\u0410\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u043d\\u044b\\u0439 \\u043f\\u0430\\u0440\\u043a\\",\\"\\u0410\\u044d\\u0440\\u043e\\u043f\\u043e\\u0440\\u0442\\",\\"\\u0412\\u043e\\u043a\\u0437\\u0430\\u043b\\",\\"\\u0414\\u0435\\u043f\\u043e \\u043c\\u0435\\u0442\\u0440\\u043e\\u043f\\u043e\\u043b\\u0438\\u0442\\u0435\\u043d\\u0430\\",\\"\\u0422\\u0440\\u0430\\u043c\\u0432\\u0430\\u0439\\u043d\\u043e\\u0435 \\u0434\\u0435\\u043f\\u043e\\",\\"\\u0422\\u0440\\u043e\\u043b\\u043b\\u0435\\u0439\\u0431\\u0443\\u0441\\u043d\\u044b\\u0439 \\u043f\\u0430\\u0440\\"]}","b3-s4-title":"<p>\\u041a\\u0440\\u0430\\u0441\\u043e\\u0442\\u0430 <br \\/> \\u0438 \\u0437\\u0434\\u043e\\u0440\\u043e\\u0432\\u044c\\u0435<\\/p>","b3-s4":"{\\"text\\":[\\"\\u0411\\u0430\\u043d\\u044f\\",\\"\\u041c\\u0430\\u043d\\u0438\\u043a\\u044e\\u0440\\u043d\\u0430\\u044f\\",\\"\\u041f\\u0430\\u0440\\u0438\\u043a\\u043c\\u0430\\u0445\\u0435\\u0440\\u0441\\u043a\\u0430\\u044f\\",\\"\\u0421\\u0430\\u043b\\u043e\\u043d \\u043a\\u0440\\u0430\\u0441\\u043e\\u0442\\u044b\\",\\"\\u0421\\u043f\\u043e\\u0440\\u0442\\u0438\\u0432\\u043d\\u0430\\u044f \\u0441\\u0435\\u043a\\u0446\\u0438\\u044f\\",\\"\\u0421\\u0442\\u043e\\u043c\\u0430\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u043a\\u0430\\u0431\\u0438\\u043d\\u0435\\u0442\\",\\"\\u0424\\u0438\\u0442\\u043d\\u0435\\u0441-\\u043a\\u043b\\u0443\\u0431\\",\\"\\u0427\\u0430\\u0441\\u0442\\u043d\\u0430\\u044f \\u043a\\u043b\\u0438\\u043d\\u0438\\u043a\\u0430\\",\\"SPA-\\u0441\\u0430\\u043b\\u043e\\u043d\\"]}","b3-s5-title":"<p>\\u0420\\u0430\\u0437\\u0432\\u043b\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f<\\/p>","b3-s5":"{\\"text\\":[\\"\\u0410\\u043a\\u0432\\u0430\\u043f\\u0430\\u0440\\u043a\\",\\"\\u0411\\u0438\\u043b\\u044c\\u044f\\u0440\\u0434\\u043d\\u0430\\u044f\\",\\"\\u0411\\u043e\\u0443\\u043b\\u0438\\u043d\\u0433\\",\\"\\u0412\\u044b\\u0441\\u0442\\u0430\\u0432\\u043e\\u0447\\u043d\\u044b\\u0439 \\u043a\\u043e\\u043c\\u043f\\u043b\\u0435\\u043a\\u0441\\",\\"\\u0417\\u043e\\u043e\\u043f\\u0430\\u0440\\u043a\\",\\"\\u041a\\u0438\\u043d\\u043e\\u0442\\u0435\\u0430\\u0442\\u0440\\",\\"\\u041a\\u043e\\u043d\\u0446\\u0435\\u0440\\u0442\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043b\\",\\"\\u041c\\u0443\\u0437\\u0435\\u0439\\",\\"\\u041d\\u043e\\u0447\\u043d\\u043e\\u0439 \\u043a\\u043b\\u0443\\u0431\\",\\"\\u041f\\u0430\\u0440\\u043a \\u0430\\u0442\\u0442\\u0440\\u0430\\u043a\\u0446\\u0438\\u043e\\u043d\\u043e\\u0432\\",\\"\\u0421\\u043f\\u043e\\u0440\\u0442\\u0438\\u0432\\u043d\\u0430\\u044f \\u0430\\u0440\\u0435\\u043d\\u0430\\",\\"\\u0421\\u043f\\u043e\\u0440\\u0442\\u0438\\u0432\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043b\\",\\"\\u0422\\u0435\\u0430\\u0442\\u0440\\"]}","b3-s6-title":"<p>\\u041f\\u043e\\u0432\\u0441\\u0435\\u0434\\u043d\\u0435\\u0432\\u043d\\u044b\\u0435 <br \\/> \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438<\\/p>","b3-s6":"{\\"text\\":[\\"\\u0410\\u0432\\u0442\\u043e\\u0437\\u0430\\u043f\\u0440\\u0430\\u0432\\u043a\\u0430\\",\\"\\u0410\\u0432\\u0442\\u043e\\u043c\\u043e\\u0439\\u043a\\u0430\\",\\"\\u0411\\u0430\\u043d\\u043a\\",\\"\\u0421\\u043b\\u0443\\u0436\\u0431\\u0430 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0438\\",\\"\\u0421\\u0422\\u041e\\",\\"\\u0422\\u0430\\u043a\\u0441\\u0438\\",\\"\\u0428\\u0438\\u043d\\u043e\\u043c\\u043e\\u043d\\u0442\\u0430\\u0436\\"]}","b3-s7-title":"<p>\\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u044b<\\/p>","b3-s7":"{\\"text\\":[\\"\\u0413\\u0438\\u043f\\u0435\\u0440\\u043c\\u0430\\u0440\\u043a\\u0435\\u0442 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u0438\\u043a\\u0438\\",\\"\\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\\\\\"\\u0432\\u043e\\u0437\\u043b\\u0435 \\u0434\\u043e\\u043c\\u0430\\\\\\"\\",\\"\\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\u043e\\u0434\\u0435\\u0436\\u0434\\u044b \\u0438 \\u043e\\u0431\\u0443\\u0432\\u0438\\",\\"\\u041f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u043e\\u0432\\u044b\\u0439 \\u0433\\u0438\\u043f\\u0435\\u0440\\u043c\\u0430\\u0440\\u043a\\u0435\\u0442\\",\\"\\u0421\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u0438\\u0437\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0435 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u044b\\",\\"\\u0421\\u0442\\u0440\\u043e\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0433\\u0438\\u043f\\u0435\\u0440\\u043c\\u0430\\u0440\\u043a\\u0435\\u0442\\"]}","b4-title":"<p>\\u0415\\u0434\\u0438\\u043d\\u0430\\u044f \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f <br \\/> \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430, \\u0438\\u0437 \\u0442\\u0440\\u0435\\u0445 \\u043a\\u043e\\u043c\\u043f\\u043e\\u043d\\u0435\\u043d\\u0442\\u043e\\u0432, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0435 1\\u0421<\\/p>","shemFile":"images\\/system.png","b4b1-title":"<p>\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442<\\/p>","b4b1-text":"<p>\\u0424\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432 \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b; \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u044f \\u043e\\u0431\\u0440\\u0430\\u0442\\u043d\\u043e\\u0439 \\u0441\\u0432\\u044f\\u0437\\u0438 \\u043c\\u0435\\u0436\\u0434\\u0443 \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438 \\u0440\\u0435\\u0441\\u0442\\u043e\\u0440\\u0430\\u043d\\u043e\\u0432 \\u0438 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438<\\/p>","b4b2-title":"<p>\\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f \\u0431\\u0430\\u0437\\u0430,<br \\/> \\u0440\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u043d\\u043d\\u0430\\u044f \\u043d\\u0430 \\u043f\\u043b\\u0430\\u0442\\u0444\\u043e\\u0440\\u043c\\u0435 \\u00ab1\\u0421:\\u041f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u044f\\u00bb<\\/p>","b4b2-text":"<p>\\u0412 \\u043d\\u0435\\u0439 \\u0441\\u043e\\u0431\\u0438\\u0440\\u0430\\u0435\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u043e \\u043f\\u0440\\u043e\\u0432\\u0435\\u0434\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0430\\u0445 \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0445 \\u0442\\u043e\\u0447\\u0435\\u043a \\u0438 \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u0443\\u044e\\u0442\\u0441\\u044f \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0435 \\u043e\\u0442\\u0447\\u0435\\u0442\\u044b<\\/p>","b4b3-title":"<p>\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435 \\u00ab\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u00bb<\\/p>","b4b3-text":"<p>\\u0421 \\u0435\\u0433\\u043e \\u043f\\u043e\\u043c\\u043e\\u0449\\u044c\\u044e \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u044f\\u0435\\u0442 \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u0443\\u044e \\u0442\\u043e\\u0447\\u043a\\u0443 \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u0435\\u0442 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438 \\u0432 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u0443\\u044e \\u0431\\u0430\\u0437\\u0443<\\/p>","b5-title":"<p>\\u0421\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u043f\\u043e\\u0437\\u0432\\u043e\\u043b\\u044f\\u0435\\u0442 \\u0430\\u0432\\u0442\\u043e\\u043c\\u0430\\u0442\\u0438\\u0437\\u0438\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c <br \\/> \\u0441\\u043b\\u0435\\u0434\\u0443\\u044e\\u0449\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0446\\u0435\\u0441\\u0441\\u044b:<\\/p>","screen-1":"images\\/iphone-6.png","screen-2":"images\\/screen-1.png","screen-3":"images\\/screen-2.png","b5-list":"{\\"text\\":[\\"\\u041f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u043e\\u0432 \\u0440\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c\\u0438, \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438\\",\\"\\u041f\\u0440\\u043e\\u0432\\u0435\\u0434\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440\\u0430\\u043c\\u0438 \\u043f\\u043e \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u0430\\u043c\\",\\"\\u041a\\u043e\\u043d\\u0441\\u043e\\u043b\\u0438\\u0434\\u0430\\u0446\\u0438\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0438 \\u043e \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u0430\\u0445 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438 \\u0432 \\u0435\\u0434\\u0438\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435\\",\\"\\u0420\\u0430\\u0437\\u043d\\u043e\\u0441\\u0442\\u043e\\u0440\\u043e\\u043d\\u043d\\u0438\\u0439 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0437 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a (\\u0434\\u0438\\u043d\\u0430\\u043c\\u0438\\u043a\\u0430 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432,                         \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433 \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0445, \\u043d\\u0430\\u0438\\u0431\\u043e\\u043b\\u0435\\u0435 \\u0447\\u0430\\u0441\\u0442\\u044b\\u0435 \\u043f\\u0440\\u0438\\u0447\\u0438\\u043d\\u044b                         \\u043f\\u043b\\u043e\\u0445\\u0438\\u0445 \\u043e\\u0446\\u0435\\u043d\\u043e\\u043a \\u0438 \\u043c\\u043d\\u043e\\u0433\\u043e\\u0435 \\u0434\\u0440\\u0443\\u0433\\u043e\\u0435)\\",\\"\\u041f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0438 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044c \\u043c\\u0435\\u0440\\u043e\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0439, \\u043d\\u0430\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043d\\u0430 \\u0443\\u0441\\u0442\\u0440\\u0430\\u043d\\u0435\\u043d\\u0438\\u0435                         \\u043f\\u0440\\u0438\\u0447\\u0438\\u043d \\u00ab\\u043e\\u0442\\u0440\\u0438\\u0446\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445\\u00bb \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043e\\u043a\\",\\"\\u041e\\u0431\\u0440\\u0430\\u0442\\u043d\\u0430\\u044f \\u0441\\u0432\\u044f\\u0437\\u044c \\u043c\\u0435\\u0436\\u0434\\u0443 \\u0434\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438 \\u0438 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440\\u0430\\u043c\\u0438: \\u043f\\u0440\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u0435                         \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442\\u043e\\u0432 \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0432, \\u0441 \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c\\u044e \\u0434\\u043e\\u0431\\u0430\\u0432\\u0438\\u0442\\u044c \\u0444\\u043e\\u0442\\u043e, \\u043a\\u0430\\u043a \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435                         \\u0444\\u0430\\u043a\\u0442\\u0430 \\u043d\\u0430\\u0440\\u0443\\u0448\\u0435\\u043d\\u0438\\u0439\\",\\"\\u0420\\u0430\\u0437\\u0433\\u0440\\u0430\\u043d\\u0438\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0443\\u0440\\u043e\\u0432\\u043d\\u0435\\u0439 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u0430 \\u043a \\u0434\\u0430\\u043d\\u043d\\u044b\\u043c \\u0434\\u043b\\u044f \\u0440\\u0430\\u0437\\u043b\\u0438\\u0447\\u043d\\u044b\\u0445 \\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                         (\\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u044b \\u0443\\u0440\\u043e\\u0432\\u043d\\u0438: \\u0414\\u0438\\u0440\\u0435\\u043a\\u0442\\u043e\\u0440, \\u0410\\u0443\\u0434\\u0438\\u0442\\u043e\\u0440, \\u0422\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0439)\\",\\"\\u0411\\u044b\\u0441\\u0442\\u0440\\u044b\\u0439, \\u0443\\u0434\\u043e\\u0431\\u043d\\u044b\\u0439 \\u043e\\u0431\\u043c\\u0435\\u043d \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0435\\u0439 \\u043c\\u0435\\u0436\\u0434\\u0443 \\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u044f\\u043c\\u0438 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b                         (\\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 \\u043f\\u0440\\u043e\\u0432\\u0435\\u043b \\u0430\\u0443\\u0434\\u0438\\u0442, \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u043b \\u0434\\u0430\\u043d\\u043d\\u044b\\u0435 \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438 \\u0432 \\u0431\\u0430\\u0437\\u0443, \\u0440\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0438                         \\u043f\\u043e\\u043b\\u0443\\u0447\\u0438\\u043b\\u0438 \\u043e\\u0442\\u0447\\u0435\\u0442\\u044b), \\u043d\\u0438\\u043a\\u0430\\u043a\\u0438\\u0445 \\u043f\\u0435\\u0440\\u0435\\u0441\\u044b\\u043b\\u043e\\u043a \\u043f\\u043e \\u043f\\u043e\\u0447\\u0442\\u0435, \\u0440\\u0443\\u0447\\u043d\\u043e\\u0433\\u043e \\u0432\\u0432\\u043e\\u0434\\u0430 \\u0438 \\u043f\\u0440\\u043e\\u0447\\u0435\\u0433\\u043e.\\",\\"\\u0418\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u00ab\\u0431\\u0435\\u0437\\u0431\\u0443\\u043c\\u0430\\u0436\\u043d\\u043e\\u0439\\u00bb \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u0438 \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430\\"]}","uznB":"\\u0423\\u0437\\u043d\\u0430\\u0442\\u044c \\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0439","uznBl":"#","b6-title":"<p>\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0435 \\u043e\\u0442\\u0447\\u0435\\u0442\\u044b<\\/p>","b6-list-text-1":"\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-1":"images\\/slider\\/Layer-0.png","b6-list-text-2":"2 \\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-2":"images\\/slider\\/Layer-0.png","b6-list-text-3":"\\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-3":"images\\/slider\\/Layer-0.png","b6-list-text-4":"3 \\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-4":"images\\/shem.png","b6-list-text-5":"4  \\u0410\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0441\\u0430\\u0439\\u0442: \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0430\\u043d\\u0430\\u043b\\u0438\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043e\\u0442\\u0447\\u0435\\u0442\\u043e\\u0432, \\u043f\\u043e\\u0441\\u0442\\u0440\\u043e\\u0435\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0439\\u0442\\u0438\\u043d\\u0433\\u043e\\u0432  \\u0432 \\u0431\\u0440\\u0430\\u0443\\u0437\\u0435\\u0440\\u0435, \\u043d\\u0430 \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445 \\u0438\\u0437 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b","b6-list-screen-5":"images\\/slider\\/Layer-0.png","b7-title":"<p>\\u041a\\u0430\\u043a \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u0442 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430?<\\/p>","b7-text":"<p>\\u0426\\u0438\\u043a\\u043b \\u0432\\u0437\\u0430\\u0438\\u043c\\u043e\\u0434\\u0435\\u0439\\u0441\\u0442\\u0432\\u0438\\u044f \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u044b \\u0438 <br \\/> \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0439<\\/p>","b7b3-title":"<p>\\u0421\\u0438\\u043d\\u0445\\u0440\\u043e\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u044f<\\/p>","planFile":"images\\/bg\\/how-do-CENTR.png","b7-list-text":"{\\"text\\":[\\"1 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"2 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"3 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"4 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\",\\"5 \\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c, \\u0432 \\u0446\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0431\\u0430\\u0437\\u0435 \\u0432\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u043f\\u043b\\u0430\\u043d\\u0443                             \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0438: \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f, \\u0442\\u043e\\u0440\\u0433\\u043e\\u0432\\u044b\\u0435 \\u0442\\u043e\\u0447\\u043a\\u0438, \\u0442\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0433\\u0440\\u0443\\u043f\\u043f\\u044b,                             \\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043b\\u0438\\u0446\\u0430 \\u0438 \\u0447\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u044b - \\u0441\\u043f\\u0438\\u0441\\u043a\\u0438 \\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439                             \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043a \\u043f\\u0440\\u043e\\u0432\\u0435\\u0440\\u043a\\u0435.\\"]}","b7b1-title":"<p>\\u041f\\u0440\\u043e\\u0432\\u0435\\u0434\\u0435\\u043d\\u0438\\u0435 <br \\/> \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0432<\\/p>","b7b1l1-title":"\\u041c\\u041e\\u0421\\u041a\\u0412\\u0410","b7b1l1-list":"{\\"text\\":[\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 1\\",\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 2\\"]}","b7b1l2-title":"\\u0420\\u0415\\u0413\\u0418\\u041e\\u041d\\u042b","b7b1l2-list":"{\\"text\\":[\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 3\\",\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 4\\",\\"\\u041a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u0435\\u0440 5\\"]}","b7b2-title":"<p>\\u041f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 <br \\/> \\u0430\\u0443\\u0434\\u0438\\u0442\\u043e\\u0432<\\/p>","b7b2l1-list":"{\\"text\\":[\\"\\u0420\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0438 \\u041e\\u041a\\u041a\\",\\"\\u0422\\u0435\\u0440\\u0440\\u0438\\u0442\\u043e\\u0440\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u044f\\u044e\\u0449\\u0438\\u0435\\",\\"\\u0410\\u043a\\u0446\\u0438\\u043e\\u043d\\u0435\\u0440\\u044b\\"]}","b8-title":"<p>\\u041d\\u0415 \\u0417\\u041d\\u0410\\u0415\\u0422\\u0415 \\u041a\\u0410\\u041a\\u041e\\u0419 \\u0422\\u0410\\u0420\\u0418\\u0424 \\u0412\\u042b\\u0411\\u0420\\u0410\\u0422\\u042c?<\\/p>","b8-text":"<p>\\u041c\\u044b \\u0432\\u0430\\u043c \\u043f\\u043e\\u043c\\u043e\\u0436\\u0435\\u043c!<\\/p>","b8-button":"\\u041f\\u043e\\u0434\\u043e\\u0431\\u0440\\u0430\\u0442\\u044c","b9-title":"<p>\\u041f\\u043e\\u0447\\u0435\\u043c\\u0443 \\u043d\\u0430\\u043c \\u0441\\u0442\\u043e\\u0438\\u0442 \\u0434\\u043e\\u0432\\u0435\\u0440\\u044f\\u0442\\u044c?<\\/p>","b9-img-1":"images\\/icon\\/wherefore-1.png","b9-text1":"<p>\\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e\\u0435 \\u043f\\u043e\\u0434\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u043e\\u0431\\u0443\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0441\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u043a\\u043e\\u0432 \\u0437\\u0430 1 \\u0434\\u0435\\u043d\\u044c<\\/p>","b9-img-2":"images\\/icon\\/wherefore-1.png","b9-text2":"<p>\\u041f\\u0440\\u043e\\u0444\\u0435\\u0441\\u0441\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f \\u043f\\u043e\\u0434\\u0434\\u0435\\u0440\\u0436\\u043a\\u0430 \\u0438 \\u0441\\u043e\\u043f\\u0440\\u043e\\u0432\\u043e\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435 (\\u043f\\u0435\\u0440\\u0438\\u043e\\u0434\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0439 \\u0432\\u044b\\u043f\\u0443\\u0441\\u043a \\u043e\\u0431\\u043d\\u043e\\u0432\\u043b\\u0435\\u043d\\u0438\\u0439)<\\/p>","b9-img-3":"images\\/icon\\/wherefore-1.png","b9-text3":"<p>\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b \\u0438 \\u0441\\u043e\\u0445\\u0440\\u0430\\u043d\\u043d\\u043e\\u0441\\u0442\\u0438 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445, \\u043f\\u043e\\u0434\\u043a\\u0440\\u0435\\u043f\\u043b\\u0435\\u043d\\u043d\\u0430\\u044f \\u043e\\u0444\\u0438\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c<\\/p>","b10-show":"1","b10-title":"<p>\\u0421\\u043a\\u0430\\u0447\\u0430\\u0439\\u0442\\u0435 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u0434\\u0435\\u043c\\u043e-\\u0432\\u0435\\u0440\\u0441\\u0438\\u044e <br \\/> \\u043c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u043e\\u0433\\u043e \\u043f\\u0440\\u0438\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f \\u201c\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\u201d<\\/p>","b10-img":"images\\/iphone-6-2.png","b10-text":"<p>\\u0423\\u0431\\u0435\\u0434\\u0438\\u0442\\u0435\\u0441\\u044c \\u0432 \\u0443\\u0434\\u043e\\u0431\\u0441\\u0442\\u0432\\u0435 \\u0438 \\u0444\\u0443\\u043d\\u043a\\u0446\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u043e\\u0441\\u0442\\u0438 \\u0438\\u043d\\u0442\\u0435\\u0440\\u0444\\u0435\\u0439\\u0441\\u0430<\\/p>","b10-button-1":"App Store","b10-button-1-url":"#","b10-button-2":"Google play","b10-button-2-url":"#","b11-img":"images\\/sert.png","b11-title":"<p>\\u041d\\u0430\\u0448\\u0438 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u044b<\\/p>","b11-text":"<p>\\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\u00ab\\u041e\\u043f\\u0442\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0430\\u043b\\u0433\\u043e\\u0440\\u0438\\u0442\\u043c\\u044b\\u00bb \\u043e\\u0441\\u043d\\u043e\\u0432\\u0430\\u043d\\u0430 \\u0432 2010 \\u0433\\u043e\\u0434\\u0443 \\u044f\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u043e\\u0444\\u0438\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u043c \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u043c \\u0444\\u0438\\u0440\\u043c\\u044b \\u00ab1\\u0421\\u00bb. <br \\/> <br \\/> \\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u044b\\u043c \\u043d\\u0430\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u0435\\u043c \\u0434\\u0435\\u044f\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0441\\u0442\\u0438 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\u044f\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0440\\u0430\\u0437\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430, \\u0432\\u043d\\u0435\\u0434\\u0440\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u0441\\u043e\\u043f\\u0440\\u043e\\u0432\\u043e\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435 \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u043e\\u043d\\u043d\\u044b\\u0445 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c \\u043d\\u0430 \\u0431\\u0430\\u0437\\u0435 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u043e\\u0432 \\u0444\\u0438\\u0440\\u043c\\u044b \\u00ab1\\u0421\\u00bb.<\\/p>","b11-img-1c":"images\\/1c.jpg","b11-text-sub":"<p>\\u041f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442 \\"\\u0427\\u0435\\u043a-\\u043b\\u0438\\u0441\\u0442\\" \\u0444\\u0438\\u0440\\u043c\\u044b \\"\\u041e\\u043f\\u0442\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 <br \\/> \\u0430\\u043b\\u0433\\u043e\\u0440\\u0438\\u0442\\u043c\\u044b\\" \\u043f\\u043e\\u043b\\u0443\\u0447\\u0438\\u043b \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442 \\"\\u0421\\u043e\\u0432\\u043c\\u0435\\u0441\\u0442\\u0438\\u043c\\u043e! <br \\/> \\u0421\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c 1\\u0421:\\u041f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u0435\\"<\\/p>","b11-button":"\\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0430\\u044f \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f"}');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_ucm_base`
--

CREATE TABLE `lend_ucm_base` (
  `ucm_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_ucm_content`
--

CREATE TABLE `lend_ucm_content` (
  `core_content_id` int(10) UNSIGNED NOT NULL,
  `core_type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `core_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_checked_out_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_featured` tinyint(4) UNSIGNED NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_content_item_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID from the individual type table',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `core_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Contains core content data in name spaced fields';

-- --------------------------------------------------------

--
-- Структура таблицы `lend_ucm_history`
--

CREATE TABLE `lend_ucm_history` (
  `version_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) UNSIGNED NOT NULL,
  `ucm_type_id` int(10) UNSIGNED NOT NULL,
  `version_note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `character_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_updates`
--

CREATE TABLE `lend_updates` (
  `update_id` int(11) NOT NULL,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `folder` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailsurl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `infourl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Available Updates';

--
-- Дамп данных таблицы `lend_updates`
--

INSERT INTO `lend_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1, 2, 0, 'Armenian', '', 'pkg_hy-AM', 'package', '', 0, '3.4.4.1', '', 'https://update.joomla.org/language/details3/hy-AM_details.xml', '', ''),
(2, 2, 0, 'Malay', '', 'pkg_ms-MY', 'package', '', 0, '3.4.1.2', '', 'https://update.joomla.org/language/details3/ms-MY_details.xml', '', ''),
(3, 2, 0, 'Romanian', '', 'pkg_ro-RO', 'package', '', 0, '3.7.0.1', '', 'https://update.joomla.org/language/details3/ro-RO_details.xml', '', ''),
(4, 2, 0, 'Flemish', '', 'pkg_nl-BE', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/nl-BE_details.xml', '', ''),
(5, 2, 0, 'Chinese Traditional', '', 'pkg_zh-TW', 'package', '', 0, '3.7.0.2', '', 'https://update.joomla.org/language/details3/zh-TW_details.xml', '', ''),
(6, 2, 0, 'French', '', 'pkg_fr-FR', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/fr-FR_details.xml', '', ''),
(7, 2, 0, 'Galician', '', 'pkg_gl-ES', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/gl-ES_details.xml', '', ''),
(8, 2, 0, 'Georgian', '', 'pkg_ka-GE', 'package', '', 0, '3.7.2.2', '', 'https://update.joomla.org/language/details3/ka-GE_details.xml', '', ''),
(9, 2, 0, 'Greek', '', 'pkg_el-GR', 'package', '', 0, '3.6.3.2', '', 'https://update.joomla.org/language/details3/el-GR_details.xml', '', ''),
(10, 2, 0, 'Japanese', '', 'pkg_ja-JP', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/ja-JP_details.xml', '', ''),
(11, 2, 0, 'Hebrew', '', 'pkg_he-IL', 'package', '', 0, '3.1.1.1', '', 'https://update.joomla.org/language/details3/he-IL_details.xml', '', ''),
(12, 2, 0, 'Hungarian', '', 'pkg_hu-HU', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/hu-HU_details.xml', '', ''),
(13, 2, 0, 'Afrikaans', '', 'pkg_af-ZA', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/af-ZA_details.xml', '', ''),
(14, 2, 0, 'Arabic Unitag', '', 'pkg_ar-AA', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/ar-AA_details.xml', '', ''),
(15, 2, 0, 'Belarusian', '', 'pkg_be-BY', 'package', '', 0, '3.2.1.2', '', 'https://update.joomla.org/language/details3/be-BY_details.xml', '', ''),
(16, 2, 0, 'Bulgarian', '', 'pkg_bg-BG', 'package', '', 0, '3.6.5.2', '', 'https://update.joomla.org/language/details3/bg-BG_details.xml', '', ''),
(17, 2, 0, 'Catalan', '', 'pkg_ca-ES', 'package', '', 0, '3.7.0.1', '', 'https://update.joomla.org/language/details3/ca-ES_details.xml', '', ''),
(18, 2, 0, 'Chinese Simplified', '', 'pkg_zh-CN', 'package', '', 0, '3.4.1.2', '', 'https://update.joomla.org/language/details3/zh-CN_details.xml', '', ''),
(19, 2, 0, 'Croatian', '', 'pkg_hr-HR', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/hr-HR_details.xml', '', ''),
(20, 2, 0, 'Czech', '', 'pkg_cs-CZ', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/cs-CZ_details.xml', '', ''),
(21, 2, 0, 'Danish', '', 'pkg_da-DK', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/da-DK_details.xml', '', ''),
(22, 2, 0, 'Dutch', '', 'pkg_nl-NL', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/nl-NL_details.xml', '', ''),
(23, 2, 0, 'Estonian', '', 'pkg_et-EE', 'package', '', 0, '3.7.0.1', '', 'https://update.joomla.org/language/details3/et-EE_details.xml', '', ''),
(24, 2, 0, 'Italian', '', 'pkg_it-IT', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/it-IT_details.xml', '', ''),
(25, 2, 0, 'Khmer', '', 'pkg_km-KH', 'package', '', 0, '3.4.5.1', '', 'https://update.joomla.org/language/details3/km-KH_details.xml', '', ''),
(26, 2, 0, 'Korean', '', 'pkg_ko-KR', 'package', '', 0, '3.7.2.3', '', 'https://update.joomla.org/language/details3/ko-KR_details.xml', '', ''),
(27, 2, 0, 'Latvian', '', 'pkg_lv-LV', 'package', '', 0, '3.7.2.2', '', 'https://update.joomla.org/language/details3/lv-LV_details.xml', '', ''),
(28, 2, 0, 'Macedonian', '', 'pkg_mk-MK', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/mk-MK_details.xml', '', ''),
(29, 2, 0, 'Norwegian Bokmal', '', 'pkg_nb-NO', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/nb-NO_details.xml', '', ''),
(30, 2, 0, 'Norwegian Nynorsk', '', 'pkg_nn-NO', 'package', '', 0, '3.4.2.1', '', 'https://update.joomla.org/language/details3/nn-NO_details.xml', '', ''),
(31, 2, 0, 'Persian', '', 'pkg_fa-IR', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/fa-IR_details.xml', '', ''),
(32, 2, 0, 'Polish', '', 'pkg_pl-PL', 'package', '', 0, '3.7.0.3', '', 'https://update.joomla.org/language/details3/pl-PL_details.xml', '', ''),
(33, 2, 0, 'Portuguese', '', 'pkg_pt-PT', 'package', '', 0, '3.7.0.1', '', 'https://update.joomla.org/language/details3/pt-PT_details.xml', '', ''),
(34, 2, 0, 'English AU', '', 'pkg_en-AU', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-AU_details.xml', '', ''),
(35, 2, 0, 'Slovak', '', 'pkg_sk-SK', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/sk-SK_details.xml', '', ''),
(36, 2, 0, 'English US', '', 'pkg_en-US', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-US_details.xml', '', ''),
(37, 2, 0, 'Swedish', '', 'pkg_sv-SE', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/sv-SE_details.xml', '', ''),
(38, 2, 0, 'Syriac', '', 'pkg_sy-IQ', 'package', '', 0, '3.4.5.1', '', 'https://update.joomla.org/language/details3/sy-IQ_details.xml', '', ''),
(39, 2, 0, 'Tamil', '', 'pkg_ta-IN', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/ta-IN_details.xml', '', ''),
(40, 2, 0, 'Thai', '', 'pkg_th-TH', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/th-TH_details.xml', '', ''),
(41, 2, 0, 'Turkish', '', 'pkg_tr-TR', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/tr-TR_details.xml', '', ''),
(42, 2, 0, 'Ukrainian', '', 'pkg_uk-UA', 'package', '', 0, '3.7.1.1', '', 'https://update.joomla.org/language/details3/uk-UA_details.xml', '', ''),
(43, 2, 0, 'Uyghur', '', 'pkg_ug-CN', 'package', '', 0, '3.3.0.2', '', 'https://update.joomla.org/language/details3/ug-CN_details.xml', '', ''),
(44, 2, 0, 'Albanian', '', 'pkg_sq-AL', 'package', '', 0, '3.1.1.2', '', 'https://update.joomla.org/language/details3/sq-AL_details.xml', '', ''),
(45, 2, 0, 'Basque', '', 'pkg_eu-ES', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/eu-ES_details.xml', '', ''),
(46, 2, 0, 'Hindi', '', 'pkg_hi-IN', 'package', '', 0, '3.3.6.2', '', 'https://update.joomla.org/language/details3/hi-IN_details.xml', '', ''),
(47, 2, 0, 'Portuguese Brazil', '', 'pkg_pt-BR', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/pt-BR_details.xml', '', ''),
(48, 2, 0, 'Serbian Latin', '', 'pkg_sr-YU', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/sr-YU_details.xml', '', ''),
(49, 2, 0, 'Spanish', '', 'pkg_es-ES', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/es-ES_details.xml', '', ''),
(50, 2, 0, 'Bosnian', '', 'pkg_bs-BA', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/bs-BA_details.xml', '', ''),
(51, 2, 0, 'Serbian Cyrillic', '', 'pkg_sr-RS', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/sr-RS_details.xml', '', ''),
(52, 2, 0, 'Vietnamese', '', 'pkg_vi-VN', 'package', '', 0, '3.2.1.2', '', 'https://update.joomla.org/language/details3/vi-VN_details.xml', '', ''),
(53, 2, 0, 'Bahasa Indonesia', '', 'pkg_id-ID', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/id-ID_details.xml', '', ''),
(54, 2, 0, 'Finnish', '', 'pkg_fi-FI', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/fi-FI_details.xml', '', ''),
(55, 2, 0, 'Swahili', '', 'pkg_sw-KE', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/sw-KE_details.xml', '', ''),
(56, 2, 0, 'Montenegrin', '', 'pkg_srp-ME', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/srp-ME_details.xml', '', ''),
(57, 2, 0, 'English CA', '', 'pkg_en-CA', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-CA_details.xml', '', ''),
(58, 2, 0, 'French CA', '', 'pkg_fr-CA', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/fr-CA_details.xml', '', ''),
(59, 2, 0, 'Welsh', '', 'pkg_cy-GB', 'package', '', 0, '3.7.0.1', '', 'https://update.joomla.org/language/details3/cy-GB_details.xml', '', ''),
(60, 2, 0, 'Sinhala', '', 'pkg_si-LK', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/si-LK_details.xml', '', ''),
(61, 2, 0, 'Dari Persian', '', 'pkg_prs-AF', 'package', '', 0, '3.4.4.2', '', 'https://update.joomla.org/language/details3/prs-AF_details.xml', '', ''),
(62, 2, 0, 'Turkmen', '', 'pkg_tk-TM', 'package', '', 0, '3.5.0.2', '', 'https://update.joomla.org/language/details3/tk-TM_details.xml', '', ''),
(63, 2, 0, 'Irish', '', 'pkg_ga-IE', 'package', '', 0, '3.7.0.1', '', 'https://update.joomla.org/language/details3/ga-IE_details.xml', '', ''),
(64, 2, 0, 'Dzongkha', '', 'pkg_dz-BT', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/dz-BT_details.xml', '', ''),
(65, 2, 0, 'Slovenian', '', 'pkg_sl-SI', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/sl-SI_details.xml', '', ''),
(66, 2, 0, 'Spanish CO', '', 'pkg_es-CO', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/es-CO_details.xml', '', ''),
(67, 2, 0, 'German CH', '', 'pkg_de-CH', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/de-CH_details.xml', '', ''),
(68, 2, 0, 'German AT', '', 'pkg_de-AT', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/de-AT_details.xml', '', ''),
(69, 2, 0, 'German LI', '', 'pkg_de-LI', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/de-LI_details.xml', '', ''),
(70, 2, 0, 'German LU', '', 'pkg_de-LU', 'package', '', 0, '3.7.2.1', '', 'https://update.joomla.org/language/details3/de-LU_details.xml', '', ''),
(71, 2, 0, 'English NZ', '', 'pkg_en-NZ', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/en-NZ_details.xml', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_update_sites`
--

CREATE TABLE `lend_update_sites` (
  `update_site_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Update Sites';

--
-- Дамп данных таблицы `lend_update_sites`
--

INSERT INTO `lend_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla! Core', 'collection', 'https://update.joomla.org/core/list.xml', 1, 1497890578, ''),
(2, 'Accredited Joomla! Translations', 'collection', 'https://update.joomla.org/language/translationlist_3.xml', 1, 1497890581, ''),
(3, 'Joomla! Update Component Update Site', 'extension', 'https://update.joomla.org/core/extensions/com_joomlaupdate.xml', 1, 1497890582, ''),
(4, 'Flexheader3 Updates', 'extension', 'https://flexheader.andrehotzler.de/Downloads/flexheader3_updates.xml', 1, 1497890582, ''),
(5, 'Slideshow CK Update', 'extension', 'http://update.joomlack.fr/mod_slideshowck_update.xml', 1, 1497890583, '');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_update_sites_extensions`
--

CREATE TABLE `lend_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Links extensions to update sites';

--
-- Дамп данных таблицы `lend_update_sites_extensions`
--

INSERT INTO `lend_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 802),
(2, 10002),
(2, 10005),
(3, 28),
(4, 10007),
(5, 10008),
(5, 10009);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_usergroups`
--

CREATE TABLE `lend_usergroups` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_usergroups`
--

INSERT INTO `lend_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 18, 'Public'),
(2, 1, 8, 15, 'Registered'),
(3, 2, 9, 14, 'Author'),
(4, 3, 10, 13, 'Editor'),
(5, 4, 11, 12, 'Publisher'),
(6, 1, 4, 7, 'Manager'),
(7, 6, 5, 6, 'Administrator'),
(8, 1, 16, 17, 'Super Users'),
(9, 1, 2, 3, 'Guest');

-- --------------------------------------------------------

--
-- Структура таблицы `lend_users`
--

CREATE TABLE `lend_users` (
  `id` int(11) NOT NULL,
  `name` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_users`
--

INSERT INTO `lend_users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES
(535, 'Super User', 'admin', 'addresok@gmail.com', '$2y$10$SZqnLzqu4mT5drN01oBpPep0eRULGhqjbTUDRbTQ21LgdxANDYh3K', 0, 1, '2017-06-14 17:54:32', '2017-06-19 16:42:54', '0', '', '0000-00-00 00:00:00', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_user_keys`
--

CREATE TABLE `lend_user_keys` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `series` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uastring` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_user_notes`
--

CREATE TABLE `lend_user_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `lend_user_profiles`
--

CREATE TABLE `lend_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Структура таблицы `lend_user_usergroup_map`
--

CREATE TABLE `lend_user_usergroup_map` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_user_usergroup_map`
--

INSERT INTO `lend_user_usergroup_map` (`user_id`, `group_id`) VALUES
(535, 8);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_utf8_conversion`
--

CREATE TABLE `lend_utf8_conversion` (
  `converted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_utf8_conversion`
--

INSERT INTO `lend_utf8_conversion` (`converted`) VALUES
(2);

-- --------------------------------------------------------

--
-- Структура таблицы `lend_viewlevels`
--

CREATE TABLE `lend_viewlevels` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `lend_viewlevels`
--

INSERT INTO `lend_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 2, '[6,2,8]'),
(3, 'Special', 3, '[6,3,8]'),
(5, 'Guest', 1, '[9]'),
(6, 'Super Users', 4, '[8]');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `lend_assets`
--
ALTER TABLE `lend_assets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_asset_name` (`name`),
  ADD KEY `idx_lft_rgt` (`lft`,`rgt`),
  ADD KEY `idx_parent_id` (`parent_id`);

--
-- Индексы таблицы `lend_associations`
--
ALTER TABLE `lend_associations`
  ADD PRIMARY KEY (`context`,`id`),
  ADD KEY `idx_key` (`key`);

--
-- Индексы таблицы `lend_banners`
--
ALTER TABLE `lend_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100)),
  ADD KEY `idx_banner_catid` (`catid`),
  ADD KEY `idx_language` (`language`);

--
-- Индексы таблицы `lend_banner_clients`
--
ALTER TABLE `lend_banner_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100));

--
-- Индексы таблицы `lend_banner_tracks`
--
ALTER TABLE `lend_banner_tracks`
  ADD PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  ADD KEY `idx_track_date` (`track_date`),
  ADD KEY `idx_track_type` (`track_type`),
  ADD KEY `idx_banner_id` (`banner_id`);

--
-- Индексы таблицы `lend_categories`
--
ALTER TABLE `lend_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_idx` (`extension`,`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Индексы таблицы `lend_contact_details`
--
ALTER TABLE `lend_contact_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Индексы таблицы `lend_content`
--
ALTER TABLE `lend_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Индексы таблицы `lend_contentitem_tag_map`
--
ALTER TABLE `lend_contentitem_tag_map`
  ADD UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  ADD KEY `idx_tag_type` (`tag_id`,`type_id`),
  ADD KEY `idx_date_id` (`tag_date`,`tag_id`),
  ADD KEY `idx_core_content_id` (`core_content_id`);

--
-- Индексы таблицы `lend_content_frontpage`
--
ALTER TABLE `lend_content_frontpage`
  ADD PRIMARY KEY (`content_id`);

--
-- Индексы таблицы `lend_content_rating`
--
ALTER TABLE `lend_content_rating`
  ADD PRIMARY KEY (`content_id`);

--
-- Индексы таблицы `lend_content_types`
--
ALTER TABLE `lend_content_types`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `idx_alias` (`type_alias`(100));

--
-- Индексы таблицы `lend_extensions`
--
ALTER TABLE `lend_extensions`
  ADD PRIMARY KEY (`extension_id`),
  ADD KEY `element_clientid` (`element`,`client_id`),
  ADD KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  ADD KEY `extension` (`type`,`element`,`folder`,`client_id`);

--
-- Индексы таблицы `lend_fields`
--
ALTER TABLE `lend_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_user_id` (`created_user_id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Индексы таблицы `lend_fields_categories`
--
ALTER TABLE `lend_fields_categories`
  ADD PRIMARY KEY (`field_id`,`category_id`);

--
-- Индексы таблицы `lend_fields_groups`
--
ALTER TABLE `lend_fields_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_by` (`created_by`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Индексы таблицы `lend_fields_values`
--
ALTER TABLE `lend_fields_values`
  ADD KEY `idx_field_id` (`field_id`),
  ADD KEY `idx_item_id` (`item_id`(191));

--
-- Индексы таблицы `lend_finder_filters`
--
ALTER TABLE `lend_finder_filters`
  ADD PRIMARY KEY (`filter_id`);

--
-- Индексы таблицы `lend_finder_links`
--
ALTER TABLE `lend_finder_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `idx_type` (`type_id`),
  ADD KEY `idx_title` (`title`(100)),
  ADD KEY `idx_md5` (`md5sum`),
  ADD KEY `idx_url` (`url`(75)),
  ADD KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  ADD KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`);

--
-- Индексы таблицы `lend_finder_links_terms0`
--
ALTER TABLE `lend_finder_links_terms0`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_terms1`
--
ALTER TABLE `lend_finder_links_terms1`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_terms2`
--
ALTER TABLE `lend_finder_links_terms2`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_terms3`
--
ALTER TABLE `lend_finder_links_terms3`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_terms4`
--
ALTER TABLE `lend_finder_links_terms4`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_terms5`
--
ALTER TABLE `lend_finder_links_terms5`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_terms6`
--
ALTER TABLE `lend_finder_links_terms6`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_terms7`
--
ALTER TABLE `lend_finder_links_terms7`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_terms8`
--
ALTER TABLE `lend_finder_links_terms8`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_terms9`
--
ALTER TABLE `lend_finder_links_terms9`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_termsa`
--
ALTER TABLE `lend_finder_links_termsa`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_termsb`
--
ALTER TABLE `lend_finder_links_termsb`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_termsc`
--
ALTER TABLE `lend_finder_links_termsc`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_termsd`
--
ALTER TABLE `lend_finder_links_termsd`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_termse`
--
ALTER TABLE `lend_finder_links_termse`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_links_termsf`
--
ALTER TABLE `lend_finder_links_termsf`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Индексы таблицы `lend_finder_taxonomy`
--
ALTER TABLE `lend_finder_taxonomy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `state` (`state`),
  ADD KEY `ordering` (`ordering`),
  ADD KEY `access` (`access`),
  ADD KEY `idx_parent_published` (`parent_id`,`state`,`access`);

--
-- Индексы таблицы `lend_finder_taxonomy_map`
--
ALTER TABLE `lend_finder_taxonomy_map`
  ADD PRIMARY KEY (`link_id`,`node_id`),
  ADD KEY `link_id` (`link_id`),
  ADD KEY `node_id` (`node_id`);

--
-- Индексы таблицы `lend_finder_terms`
--
ALTER TABLE `lend_finder_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD UNIQUE KEY `idx_term` (`term`),
  ADD KEY `idx_term_phrase` (`term`,`phrase`),
  ADD KEY `idx_stem_phrase` (`stem`,`phrase`),
  ADD KEY `idx_soundex_phrase` (`soundex`,`phrase`);

--
-- Индексы таблицы `lend_finder_terms_common`
--
ALTER TABLE `lend_finder_terms_common`
  ADD KEY `idx_word_lang` (`term`,`language`),
  ADD KEY `idx_lang` (`language`);

--
-- Индексы таблицы `lend_finder_tokens`
--
ALTER TABLE `lend_finder_tokens`
  ADD KEY `idx_word` (`term`),
  ADD KEY `idx_context` (`context`);

--
-- Индексы таблицы `lend_finder_tokens_aggregate`
--
ALTER TABLE `lend_finder_tokens_aggregate`
  ADD KEY `token` (`term`),
  ADD KEY `keyword_id` (`term_id`);

--
-- Индексы таблицы `lend_finder_types`
--
ALTER TABLE `lend_finder_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Индексы таблицы `lend_languages`
--
ALTER TABLE `lend_languages`
  ADD PRIMARY KEY (`lang_id`),
  ADD UNIQUE KEY `idx_sef` (`sef`),
  ADD UNIQUE KEY `idx_langcode` (`lang_code`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Индексы таблицы `lend_menu`
--
ALTER TABLE `lend_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`(100),`language`),
  ADD KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  ADD KEY `idx_menutype` (`menutype`),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Индексы таблицы `lend_menu_types`
--
ALTER TABLE `lend_menu_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_menutype` (`menutype`);

--
-- Индексы таблицы `lend_messages`
--
ALTER TABLE `lend_messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `useridto_state` (`user_id_to`,`state`);

--
-- Индексы таблицы `lend_messages_cfg`
--
ALTER TABLE `lend_messages_cfg`
  ADD UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`);

--
-- Индексы таблицы `lend_modules`
--
ALTER TABLE `lend_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `published` (`published`,`access`),
  ADD KEY `newsfeeds` (`module`,`published`),
  ADD KEY `idx_language` (`language`);

--
-- Индексы таблицы `lend_modules_menu`
--
ALTER TABLE `lend_modules_menu`
  ADD PRIMARY KEY (`moduleid`,`menuid`);

--
-- Индексы таблицы `lend_newsfeeds`
--
ALTER TABLE `lend_newsfeeds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Индексы таблицы `lend_overrider`
--
ALTER TABLE `lend_overrider`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `lend_postinstall_messages`
--
ALTER TABLE `lend_postinstall_messages`
  ADD PRIMARY KEY (`postinstall_message_id`);

--
-- Индексы таблицы `lend_redirect_links`
--
ALTER TABLE `lend_redirect_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_old_url` (`old_url`(100)),
  ADD KEY `idx_link_modifed` (`modified_date`);

--
-- Индексы таблицы `lend_schemas`
--
ALTER TABLE `lend_schemas`
  ADD PRIMARY KEY (`extension_id`,`version_id`);

--
-- Индексы таблицы `lend_session`
--
ALTER TABLE `lend_session`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `time` (`time`);

--
-- Индексы таблицы `lend_tags`
--
ALTER TABLE `lend_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_idx` (`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Индексы таблицы `lend_template_styles`
--
ALTER TABLE `lend_template_styles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_template` (`template`),
  ADD KEY `idx_home` (`home`);

--
-- Индексы таблицы `lend_ucm_base`
--
ALTER TABLE `lend_ucm_base`
  ADD PRIMARY KEY (`ucm_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_item_id`),
  ADD KEY `idx_ucm_type_id` (`ucm_type_id`),
  ADD KEY `idx_ucm_language_id` (`ucm_language_id`);

--
-- Индексы таблицы `lend_ucm_content`
--
ALTER TABLE `lend_ucm_content`
  ADD PRIMARY KEY (`core_content_id`),
  ADD KEY `tag_idx` (`core_state`,`core_access`),
  ADD KEY `idx_access` (`core_access`),
  ADD KEY `idx_alias` (`core_alias`(100)),
  ADD KEY `idx_language` (`core_language`),
  ADD KEY `idx_title` (`core_title`(100)),
  ADD KEY `idx_modified_time` (`core_modified_time`),
  ADD KEY `idx_created_time` (`core_created_time`),
  ADD KEY `idx_content_type` (`core_type_alias`(100)),
  ADD KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  ADD KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  ADD KEY `idx_core_created_user_id` (`core_created_user_id`),
  ADD KEY `idx_core_type_id` (`core_type_id`);

--
-- Индексы таблицы `lend_ucm_history`
--
ALTER TABLE `lend_ucm_history`
  ADD PRIMARY KEY (`version_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  ADD KEY `idx_save_date` (`save_date`);

--
-- Индексы таблицы `lend_updates`
--
ALTER TABLE `lend_updates`
  ADD PRIMARY KEY (`update_id`);

--
-- Индексы таблицы `lend_update_sites`
--
ALTER TABLE `lend_update_sites`
  ADD PRIMARY KEY (`update_site_id`);

--
-- Индексы таблицы `lend_update_sites_extensions`
--
ALTER TABLE `lend_update_sites_extensions`
  ADD PRIMARY KEY (`update_site_id`,`extension_id`);

--
-- Индексы таблицы `lend_usergroups`
--
ALTER TABLE `lend_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  ADD KEY `idx_usergroup_title_lookup` (`title`),
  ADD KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  ADD KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE;

--
-- Индексы таблицы `lend_users`
--
ALTER TABLE `lend_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_name` (`name`(100)),
  ADD KEY `idx_block` (`block`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`);

--
-- Индексы таблицы `lend_user_keys`
--
ALTER TABLE `lend_user_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `series` (`series`),
  ADD UNIQUE KEY `series_2` (`series`),
  ADD UNIQUE KEY `series_3` (`series`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `lend_user_notes`
--
ALTER TABLE `lend_user_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`),
  ADD KEY `idx_category_id` (`catid`);

--
-- Индексы таблицы `lend_user_profiles`
--
ALTER TABLE `lend_user_profiles`
  ADD UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`);

--
-- Индексы таблицы `lend_user_usergroup_map`
--
ALTER TABLE `lend_user_usergroup_map`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Индексы таблицы `lend_viewlevels`
--
ALTER TABLE `lend_viewlevels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_assetgroup_title_lookup` (`title`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `lend_assets`
--
ALTER TABLE `lend_assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT для таблицы `lend_banners`
--
ALTER TABLE `lend_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_banner_clients`
--
ALTER TABLE `lend_banner_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_categories`
--
ALTER TABLE `lend_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `lend_contact_details`
--
ALTER TABLE `lend_contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_content`
--
ALTER TABLE `lend_content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_content_types`
--
ALTER TABLE `lend_content_types`
  MODIFY `type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `lend_extensions`
--
ALTER TABLE `lend_extensions`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10011;
--
-- AUTO_INCREMENT для таблицы `lend_fields`
--
ALTER TABLE `lend_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_fields_groups`
--
ALTER TABLE `lend_fields_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_finder_filters`
--
ALTER TABLE `lend_finder_filters`
  MODIFY `filter_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_finder_links`
--
ALTER TABLE `lend_finder_links`
  MODIFY `link_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_finder_taxonomy`
--
ALTER TABLE `lend_finder_taxonomy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `lend_finder_terms`
--
ALTER TABLE `lend_finder_terms`
  MODIFY `term_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_finder_types`
--
ALTER TABLE `lend_finder_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_languages`
--
ALTER TABLE `lend_languages`
  MODIFY `lang_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `lend_menu`
--
ALTER TABLE `lend_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT для таблицы `lend_menu_types`
--
ALTER TABLE `lend_menu_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `lend_messages`
--
ALTER TABLE `lend_messages`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_modules`
--
ALTER TABLE `lend_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT для таблицы `lend_newsfeeds`
--
ALTER TABLE `lend_newsfeeds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_overrider`
--
ALTER TABLE `lend_overrider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT для таблицы `lend_postinstall_messages`
--
ALTER TABLE `lend_postinstall_messages`
  MODIFY `postinstall_message_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `lend_redirect_links`
--
ALTER TABLE `lend_redirect_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_tags`
--
ALTER TABLE `lend_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `lend_template_styles`
--
ALTER TABLE `lend_template_styles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `lend_ucm_content`
--
ALTER TABLE `lend_ucm_content`
  MODIFY `core_content_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_ucm_history`
--
ALTER TABLE `lend_ucm_history`
  MODIFY `version_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_updates`
--
ALTER TABLE `lend_updates`
  MODIFY `update_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT для таблицы `lend_update_sites`
--
ALTER TABLE `lend_update_sites`
  MODIFY `update_site_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `lend_usergroups`
--
ALTER TABLE `lend_usergroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `lend_users`
--
ALTER TABLE `lend_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=536;
--
-- AUTO_INCREMENT для таблицы `lend_user_keys`
--
ALTER TABLE `lend_user_keys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_user_notes`
--
ALTER TABLE `lend_user_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `lend_viewlevels`
--
ALTER TABLE `lend_viewlevels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
